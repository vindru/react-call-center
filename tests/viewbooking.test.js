import React from 'react';
import { mount } from 'enzyme';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import Timeslot from '../src/components/pages/ViewBooking/timeslot';
import Booking from '../src/components/pages/ViewBooking/booking';


// Use array destructurig to create mock functions.
const [onCreateBooking, launchBroadcast, getNotes] = new Array(3).fill(jest.fn());


// Setup for Timeslot component
function mountSetup(_props,component) {
  // Sample props to pass to our shallow render
  // wrapper instance around rendered output
  let enzymeWrapper;
  try{
    switch (component) {
    case 'Timeslot': enzymeWrapper = mount(<Router><Timeslot {..._props} /></Router>);break;
    case 'Booking': enzymeWrapper = mount(<Router><Booking {..._props} /></Router>);break;
    default: throw new Error('No valid component name passed');
    }
  }
  catch(e){
    console.log(e);
    return '';
  }

  return enzymeWrapper;
}

describe('Checkup jest test setup',()=>{
  it('should run test', () => {
    expect(true).toBeTruthy();
  });
})

// Check ENABLE/DISABLING of Create booking button
// And Lock Color based upon lock_status whic would be comming from api in actual case
describe('Shallow rendered Timeslot Card', () => {
  const props = {
    refreshnotes:getNotes,
    notes:[{
      booking_id: "5d411bd0ae97e56783e79d5f",
      createdAt: "2019-08-09T18:59:01.876Z",
      date: new Date("2019/08/13"),
      executive_id: "user",
      note: "new note",
      updatedAt: "2019-08-09T18:59:01.876Z",
      __v: 0,
      _id: "5d4dc27557bfeb00293ad743",
    }],

    date:new Date("2019/08/13"),
    item:{
      bookings:[{},{}],
      checkin_time: {hours: 13, minutes: 0},
      deposit_price: 25,
      duration: 30,
      invoice_price: 135,
      lock_status: "yellow",
      number_of_spaces: 10,
      number_of_spaces_booked: 44,
      _id: "5d51022d2e0ccd002910fbee",
    },
    disableCreateBooking:true,
    onCreateBooking,
    LaunchBroadcast:launchBroadcast,
  }
  it('should render a card with the details of the Timeslot', () => {
    // Setup wrapper and assign props.
    let  enzymeWrapper  = mountSetup(props,'Timeslot');

    expect(enzymeWrapper.debug()).toMatchSnapshot();
    // check if Create button , Lock icon exists
    expect(enzymeWrapper.find('button.ui.basic.blue')).toHaveLength(1);
    expect(enzymeWrapper.find('i.icon.large.lock')).toHaveLength(1);
    // Check whether Create Booking button is disabled when - Past date is passed
    expect(enzymeWrapper.find('button.ui.basic.blue').hasClass('disabled')).toBeTruthy();

    // lock icon has class yellow
    expect(enzymeWrapper.find('i.icon.large.lock').hasClass('yellow')).toBeTruthy();


    // change Date of booking to futue date
    const date=new Date()
    date.setDate(date.getDate()+1)
    props.date=date
    enzymeWrapper  = mountSetup(props,'Timeslot');
    // check create booking button is not disabled for future date
    expect(enzymeWrapper.find('button.ui.basic.blue').hasClass('disabled')).toBeFalsy();


    // change lock status of booking to GREEN
    props.item.lock_status = 'green'
    enzymeWrapper  = mountSetup(props,'Timeslot');
    expect(enzymeWrapper.find('i.icon.large.lock').hasClass('green')).toBeTruthy();
    // change lock status of booking to RED
    props.item.lock_status = 'red'
    enzymeWrapper = mountSetup(props,'Timeslot');

    expect(enzymeWrapper.find('i.icon.large.lock').hasClass('red')).toBeTruthy();

    // check create booking button is not disabled when even when lock status is red
    expect(enzymeWrapper.find('button.ui.basic.blue').hasClass('disabled')).toBeFalsy();


  });
});

describe('ReactDOM rendered Booking Card', () => {
  const props ={
    refreshnotes:getNotes,
    notes:[{
      booking_id: "5d411bd0ae97e56783e79d5f",
      createdAt: "2019-08-09T18:59:01.876Z",
      date: new Date("2019/08/13"),
      executive_id: "user",
      note: "new note",
      updatedAt: "2019-08-09T18:59:01.876Z",
      __v: 0,
      _id: "5d4dc27557bfeb00293ad743",
    }],
    date:new Date(),
    booking:{
      // passing empty slots
      slots:null,
      // passing empty invoice details
      invoiceDetails:null,
      name:'Demo name',
      booking_id:'12345'
    }
  }
  it('should render a booking card even if the slots entered is empty AND/OR invoiceDetails is empty', () => {
    // Setup wrapper and assign props.
    const div = document.createElement('div');
    ReactDOM.render(<Router><Booking {...props} /></Router>, div);

  });
});
