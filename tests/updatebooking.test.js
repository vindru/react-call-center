import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';


import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import { createBrowserHistory } from 'history';
import configureStore from '../src/store/configureStore';

import UpdateBooking from '../src/components/pages/UpdateBooking';

const baseUrl = '/';
const history = createBrowserHistory({ basename: baseUrl });
const initialState = window.initialReduxState;
const store = configureStore(history, initialState);


describe('ReactDOM rendered UpdateBookingPage ', () => {
  const props ={
    match:{
      params:{
        id:'6c5e469d-3ea1-451e-9bb1-19e6c933995d'
      }
    }
  }
  it('should render a UpdateBookingPage even if the number-data passed is empty / null/ undefined', () => {
    // Setup wrapper and assign props.
    const div = document.createElement('div');
    ReactDOM.render(
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Router><UpdateBooking {...props} /></Router>
        </ConnectedRouter>
      </Provider>
      , div);

  });
});
