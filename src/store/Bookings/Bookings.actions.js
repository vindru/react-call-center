import axios from 'axios';

import * as Api from '../api';
import { BookingAPI } from '../api';
import { InvoiceAPI } from '../api';

Api.products.then(api => {
  console.log(api);
});

export function getChannelsByLocation(locationId) {
  return {
    type: 'GET_CHANNELS',
    payload: new Promise((resolve, reject) => {
      Api.channels.then(api => {
        api.channel
          .getChannelByLocationId({ location_id: locationId })
          .then(({ body }) => {
            if (body) {
              resolve(body.map(x => ({ ...x })));
            } else {
              reject();
            }
          })
          .catch(() => {
            reject();
          });
      });
    }),
  };
}

export function getSources() {
  return {
    type: 'GET_SOURCES',
    payload: new Promise((resolve, reject) => {
      Api.channels.then(api => {
        api.source
          .getAllSources()
          .then(({ body }) => {
            if (body) {
              resolve(body.map(x => ({ ...x })));
            } else {
              reject();
            }
          })
          .catch(() => {
            reject();
          });
      });
    }),
  };
}

export function getProducts(locationId) {
  return {
    type: 'GET_PRODUCTS',
    payload: new Promise((resolve, reject) => {
      Api.products.then(api => {
        api.product
          .getAllProducts({ locationId })
          .then(({ body }) => {
            if (body) {
              resolve(
                body.map(p => ({
                  id: p._id,
                  type: p.website
                    ? 'PRODUCT'
                    : p.other_products
                    ? 'OTHER_PRODUCT'
                    : p.promotion
                    ? 'PROMOTION'
                    : 'UNKNOWN',
                  name: p.title,
                  price: p.price,
                  deposit: p.deposit || p.price || 0,
                  max: 12,
                  qty: 1,
                  code: p._id,
                })),
              );
            }
          })
          .catch(() => {
            reject();
          });
      });
    }),
  };
}

export function getBookingFromBookingId(bookingId) {
  return new Promise((resolve, reject) => {
    axios
      .get(`${BookingAPI}/api/v1/booking/bookingId/${bookingId}`)
      .then(bookingData => {
        console.log(bookingData);
        // Complete
        resolve(bookingData);
      })
      .catch(err => {
        reject(err);
      });
  });
}

export function createNotesByBookingId(requestData) {
  return new Promise((resolve, reject) => {
    axios
      .post(
        `${BookingAPI}/api/v1/booking/note`,
        { data: requestData },
        { config: { headers: { 'Content-Type': 'multipart/form-data' } } },
      )
      .then(resp => {
        console.log(resp);
        // Complete
        resolve(resp);
      })
      .catch(err => {
        reject(err);
      });
  });
}

export function createBookingWithPayment(bookingInfo, paymentInfo) {
  return new Promise((resolve, reject) => {
    Api.invoice.then(invoiceApi => {
      console.log('Invoice', invoiceApi);
      invoiceApi.invoice
        .createInvoice({ body: bookingInfo })
        .then(invoiceResp => {
          if (invoiceResp.body) {
            console.log('Invoice response', invoiceResp.body);
            const invoiceData = invoiceResp.body;
            const paymentBody = {
              invoice_id: invoiceData.invoice_id,
              ...paymentInfo,
            };
            axios
              .post(`${process.env.REACT_APP_PAYMENT_SERVER_URL}/api/v1/payment`, paymentBody)
              .then(paymentResp => {
                console.log(paymentResp);
                resolve(paymentResp.data);
              })
              .catch(err => {
                reject(err);
              });
          }
        })
        .catch(err => {
          reject(err);
        });
    });
  });
}

export function createBookingWithoutPayment(bookingInfo) {
  return new Promise((resolve, reject) => {
    Api.invoice.then(invoiceApi => {
      invoiceApi.invoice
        .createInvoice({ body: bookingInfo })
        .then(invoiceResp => {
          if (invoiceResp.body) {
            const invoiceData = invoiceResp.body;
            Api.bookings
              .then(bookingAPI => {
                const requestBody = {
                  ...bookingInfo,
                  email: invoiceData.email,
                  phone: invoiceData.phone,
                  paid: invoiceData.paid,
                  slots: invoiceData.slots,
                  deposit_price: 0,
                  location_id: parseInt(invoiceData.location_id, 10),
                  invoice_id: invoiceData.invoice_id,
                  date: new Date(invoiceData.date).toISOString(),
                  checkin_time: invoiceData.checkin_time,
                };
                bookingAPI.booking
                  .confirmBooking({ body: requestBody })
                  .then(resp => {
                    if (resp.body) {
                      // Update Invoice
                      console.log(resp.body);
                      const bookingId = resp.body.booking_id;
                      Api.invoice
                        .then(invoiceAPIs => {
                          invoiceAPIs.invoice
                            .updateInvoiceByBookingId({ invoiceId: invoiceData.invoice_id, booking_id: bookingId })
                            .then(updatedResp => {
                              resolve(updatedResp.body);
                            })
                            .catch(err => {
                              reject(err);
                            });
                        })
                        .catch(err => {
                          reject(err);
                        });
                    }
                  })
                  .catch(err => {
                    reject(err);
                  });
              })
              .catch(err => {
                reject(err);
              });
          }
        })
        .catch(err => {
          reject(err);
        });
    });
  });
}

export function generateRefund(requestData) {
  return new Promise((resolve, reject) => {
    axios
      .post(`${InvoiceAPI}/refund`, { ...requestData })
      .then(resp => {
        console.log(resp);
        // Complete
        resolve(resp);
      })
      .catch(err => {
        reject(err);
      });
  });
}
export function suspendBoooking(requestData) {
  return new Promise((resolve, reject) => {
    axios
      .post(`${BookingAPI}/api/v1/changeStatus/${requestData.booking_id}`, requestData.body)
      .then(resp => {
        resolve(resp);
      })
      .catch(err => {
        reject(err);
      });
  });
}

export function getAvailablityByDate(d, locationId) {
  const date = d.format('YYYY-MM-DD');
  return new Promise((resolve, reject) => {
    Api.bookings.then(api => {
      api.booking
        .checkAvailabilityDate({ date, locationId })
        .then(({ body }) => {
          if (body) {
            resolve(body.map(x => ({ ...x, date: d, locationId })));
          } else {
            reject();
          }
        })
        .catch(() => {
          reject();
        });
    });
  });
}

export function updateBookingByInvoiceId(requestBody, invoiceId) {
  console.log('Update Booking', requestBody, invoiceId);
  return new Promise((resolve, reject) => {
    // resolve({ status: 'successful' });
    axios
      .post(`${InvoiceAPI}/invoiceId/${invoiceId}`, { ...requestBody })
      .then(resp => {
        console.log(resp);
        // Complete
        resolve(resp);
      })
      .catch(err => {
        reject(err);
      });
  });
}

export const t = 1;
