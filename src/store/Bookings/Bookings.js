import {
  droppedProduct,
  droppedProductUpdateScreen,
  depositAmtChange,
  droppedProductQtyChange,
  droppedProductDelete,
  droppedProductDeleteUpdateScreen,
  dateAvailiablityChange,
  setChannelPrices,
  droppedProductQtyChangeUpdateScreen,
  setTandemPrices,
  reCalculatePriceAndUpdateState,
  productAmtChange,
} from './Bookings.reducers';
import moment from 'moment';
const currentDate = moment();

const initialState = {
  newBooking: {
    firstname: '',
    surname: '',
    email: '',
    phonenumber: '',
    phonecode: '',
    consent18: false,
    consent_weight: false,
    products: [
      {
        id: 1,
        type: 'PRODUCT',
        name: 'Tandem',
        price: 200,
        deposit: 20,
        max: 12,
        qty: 1,
        fixed: true,
      },
      {
        id: 1,
        type: 'DEPOSIT',
        name: 'Tandem',
        price: 20,
        deposit: 20,
        max: 12,
        qty: 1,
        fixed: true,
      },
    ], // Object.assign([], defaultProduct), // make a shallow copy
    subtotal: 0,
    total: 0,
    totalDiscount: 0,
    experienceTotal: 0,
    deposit: 0,
    depositWithFee: 0,
    selectedChannel: { accept_credit_card: true },
    selectedSource: {},
    selectedSpot: '',
    selectedPickUpTime: '',
    status: '',
    errors: [],
    pickUpRequired: false,
  },
  updateBooking: {
    // for move functionality
    enableMoving: false,
    //= ============
    firstname: '',
    surname: '',
    email: '',
    phone: '',
    phonecode: '',
    consent18: false,
    consent_weight: false,
    numberOfSpace: 0,
    products: [],
    subtotal: 0,
    total: 0,
    totalDiscount: 0,
    experienceTotal: 0,
    pendingAmount: 0,
    alreadyPaid: 0,
    deposit: 0,
    depositWithFee: 0,
    selectedChannel: '',
    selectedSource: '',
    selectedSpot: 0,
    selectedPickUpTime: '',
    pickUpRequired: false,
    booking_id: '',
    checkin_time: { hours: 0, minutes: 0 },
    createdAt: currentDate,
    date: currentDate,
    invoiceDetails: [],
    invoice_id: '',
    location_id: 1,
    jumping_date: currentDate,
    paid: false,
    slots: [],
    updatedAt: currentDate,
    status: '',
    errors: [],
    previousPaymentMethods: [],
    // for any change to decide on ENABLE/DISABLE save Button
    hasChanged: {
      experienceTotal: false,
      checkin_time: false,
      jumping_date: false,
      products: false,
      basic_info: false,
    },
  },
  initialStateUpdateBooking: null,
  paymentInfo: {
    name_on_the_card: '',
    cardNumber: '',
    expireDate: '',
    cvv: '',
    country: '',
    zipCode: '',
  },
  bookings: [],
  allChannels: [],
  allSources: [],
  allPickUpSpots: [],
  place: 1,
  date: currentDate,
  rules: [],
  slotSelected: {
    checkinTime: {
      hours: 0,
      minutes: 0,
    },
  },
};

const actions = {
  ENABLE_MOVING: 'ENABLE_MOVING',
  DROPPED_PRODUCT: 'DROPPED_PRODUCT',
  BASICINFO_CHANGE: 'BASICINFO_CHANGE',
  BASICINFO_CHANGE_UPDATE_SCREEN: 'BASICINFO_CHANGE_UPDATE_SCREEN',
  PAYMENTINFO_CHANGE: 'PAYMENTINFO_CHANGE',
  DEPOSIT_AMT_CHANGE: 'DEPOSIT_AMT_CHANGE',
  PRODUCT_AMT_CHANGE: 'PRODUCT_AMT_CHANGE',
  PRODUCT_QTY_CHANGE: 'PRODUCT_QTY_CHANGE',
  DROPPED_PRODUCT_DELETE: 'DROPPED_PRODUCT_DELETE',
  PLACE_PICKED: 'PLACE_PICKED',
  DATE_PICKED: 'DATE_PICKED',
  DATE_PICKED_UPDATE_SCREEN: 'DATE_PICKED_UPDATE_SCREEN',
  GET_AVAILABILITY: 'GET_AVAILABILITY',
  SELECT_SLOT: 'SELECT_SLOT',
  SELECT_SLOT_UPDATE_SCREEN: 'SELECT_SLOT_UPDATE_SCREEN',
  GET_CHANNELS: 'GET_CHANNELS',
  GET_SOURCES: 'GET_SOURCES',
  RESET_BOOKINGINFO: 'RESET_BOOKINGINFO',
  RESET_BOOKINGINFO_UPDATE_SCREEN: 'RESET_BOOKINGINFO_UPDATE_SCREEN',
  CANCEL_UPDATE_SCREEN_EDIT_CUSTOMER_INFO: 'CANCEL_UPDATE_SCREEN_EDIT_CUSTOMER_INFO',
  RECALCULATE_PRICES_UPDATE_SCREEN: 'RECALCULATE_PRICES_UPDATE_SCREEN',
  CHANNEL_SELECTED: 'CHANNEL_SELECTED',
  SET_UPDATE_BOOKING: 'SET_UPDATE_BOOKING',
  SUSPEND_UPDATE_BOOKING: 'SUSPEND_UPDATE_BOOKING',
  SAVE_UPDATE_BOOKING: 'SAVE_UPDATE_BOOKING',
  DROPPED_PRODUCT_UPDATE_SCREEN: 'DROPPED_PRODUCT_UPDATE_SCREEN',
  DROPPED_PRODUCT_DELETE_UPDATE_SCREEN: 'DROPPED_PRODUCT_DELETE_UPDATE_SCREEN',
  DROPPED_PRODUCT_UPDATE_SCREEN_INITIAL: 'DROPPED_PRODUCT_UPDATE_SCREEN_INITIAL',
  PRODUCT_QTY_CHANGE_UPDATE_SCREEN: 'PRODUCT_QTY_CHANGE_UPDATE_SCREEN',
  PRODUCT_QTY_CHANGE_UPDATE_SCREEN_INITIAL: 'PRODUCT_QTY_CHANGE_UPDATE_SCREEN_INITIAL',
  DEPOSIT_AMT_CHANGE_UPDATE_SCREEN: 'DEPOSIT_AMT_CHANGE_UPDATE_SCREEN',
  PRODUCT_AMT_CHANGE_UPDATE_SCREEN: 'PRODUCT_AMT_CHANGE_UPDATE_SCREEN',
  CHANNEL_SELECTED_UPDATE_SCREEN: 'CHANNEL_SELECTED_UPDATE_SCREEN',
  SET_TANDEM_PRICES: 'SET_TANDEM_PRICES',
};

export default (pState, action) => {
  let state = pState || initialState;
  try {
    switch (action.type) {
      case actions.ENABLE_MOVING:
        const enableMoving = !state.updateBooking.enableMoving;
        state.updateBooking = { ...state.updateBooking, enableMoving };
        break;
      case actions.DROPPED_PRODUCT:
        state = droppedProduct(state, action);
        break;
      case actions.RECALCULATE_PRICES_UPDATE_SCREEN:
        state = reCalculatePriceAndUpdateState(state, action);
        break;
      case actions.DROPPED_PRODUCT_UPDATE_SCREEN:
        state = droppedProductUpdateScreen(state, action);
        // The following will check whether to ENABLE/DISABLE 'Save button' in update booking
        if (state.updateBooking.experienceTotal !== state.initialStateUpdateBooking.experienceTotal) {
          console.log('experienceTotal not equal for droppedProduct');
          state.updateBooking.hasChanged.experienceTotal = true;
        } else {
          state.updateBooking.hasChanged.experienceTotal = false;
        }
        // checking for the CASE when experineceTotal and original experineceTotal  are equal
        //  but the products quantity may be different
        if (state.updateBooking.products.length !== state.initialStateUpdateBooking.products.length) {
          console.log('Prdouct length not equal droppedProduct');
          state.updateBooking.hasChanged.products = true;
        } else {
          state.updateBooking.hasChanged.products = false;
        }
        break;
      case actions.DROPPED_PRODUCT_UPDATE_SCREEN_INITIAL:
        state = droppedProductUpdateScreen(state, action);
        // make deep copy
        state.initialStateUpdateBooking = JSON.parse(JSON.stringify(state.updateBooking));
        break;
      case actions.DROPPED_PRODUCT_DELETE_UPDATE_SCREEN:
        state = droppedProductDeleteUpdateScreen(state, action);
        break;
      case actions.DROPPED_PRODUCT_DELETE:
        state = droppedProductDelete(state, action);
        break;
      case actions.BASICINFO_CHANGE:
        state.newBooking = { ...state.newBooking, ...action.payload };
        break;
      case actions.BASICINFO_CHANGE_UPDATE_SCREEN:
        state.updateBooking = { ...state.updateBooking, ...action.payload };
        // for ENABLE/DISABLE 'save' button
        if (
          state.updateBooking.firstname !== state.initialStateUpdateBooking.firstname ||
          state.updateBooking.surname !== state.initialStateUpdateBooking.surname ||
          state.updateBooking.email !== state.initialStateUpdateBooking.email ||
          state.updateBooking.phone !== state.initialStateUpdateBooking.phone ||
          state.updateBooking.phonecode !== state.initialStateUpdateBooking.phonecode ||
          state.updateBooking.consent18 !== state.initialStateUpdateBooking.consent18 ||
          state.updateBooking.consent_weight !== state.initialStateUpdateBooking.consent_weight ||
          state.updateBooking.consent_weight !== state.initialStateUpdateBooking.consent_weight ||
          state.updateBooking.selectedSource !== state.initialStateUpdateBooking.selectedSource ||
          state.updateBooking.selectedChannel !== state.initialStateUpdateBooking.selectedChannel ||
          state.updateBooking.pickUpRequired !== state.initialStateUpdateBooking.pickUpRequired ||
          state.updateBooking.selectedPickUpTime !== state.initialStateUpdateBooking.selectedPickUpTime ||
          state.updateBooking.selectedSpot !== state.initialStateUpdateBooking.selectedSpot
        ) {
          state.updateBooking.hasChanged.basic_info = true;
        } else {
          state.updateBooking.hasChanged.basic_info = false;
        }
        break;
      case actions.RESET_BOOKINGINFO:
        state.newBooking = {
          ...initialState.newBooking,
          products: [
            {
              id: 1,
              type: 'PRODUCT',
              name: 'Tandem',
              price: 200,
              deposit: 20,
              max: 12,
              qty: 1,
              fixed: true,
            },
            {
              id: 1,
              type: 'DEPOSIT',
              name: 'Tandem',
              price: 20,
              deposit: 20,
              max: 12,
              qty: 1,
              fixed: true,
            },
          ], // Object.assign([], defaultProduct), // make a shallow copy
          errors: [],
        };
        state.paymentInfo = { ...initialState.paymentInfo };
        break;
      case actions.RESET_BOOKINGINFO_UPDATE_SCREEN:
        state.updateBooking = {
          ...initialState.updateBooking,
          products: [], // Object.assign([], defaultProduct), // make a shallow copy
          errors: [],
        };
        state.initialStateUpdateBooking = {
          ...initialState.updateBooking,
          products: [], // Object.assign([], defaultProduct), // make a shallow copy
          errors: [],
        };
        state.paymentInfo = { ...initialState.paymentInfo };
        break;
      case actions.PAYMENTINFO_CHANGE:
        state.paymentInfo = { ...state.paymentInfo, ...action.payload };
        break;
      case actions.DEPOSIT_AMT_CHANGE:
        state = depositAmtChange(state, action, 'new');
        break;
      case actions.PRODUCT_AMT_CHANGE:
        state = productAmtChange(state, action, 'new');
        break;
      case actions.PRODUCT_AMT_CHANGE_UPDATE_SCREEN:
        state = productAmtChange(state, action, 'update');
        // The following will check whether to ENABLE/DISABLE 'Save button' in update booking
        if (state.updateBooking.experienceTotal !== state.initialStateUpdateBooking.experienceTotal) {
          state.updateBooking.hasChanged.experienceTotal = true;
        } else {
          state.updateBooking.hasChanged.experienceTotal = false;
          // checking for the CASE when experineceTotal and original experineceTotal  are equal
          //  but the products quantity may be different
          for (let i = 0; i < state.updateBooking.products.length; i++) {
            if (state.updateBooking.products.price !== state.updateBooking.products.originalPrice) {
              state.updateBooking.hasChanged.products = true;
              break;
            } else {
              state.updateBooking.hasChanged.products = false;
            }
          }
        }
        break;
      case actions.DEPOSIT_AMT_CHANGE_UPDATE_SCREEN:
        state = depositAmtChange(state, action, 'update');
        // The following will check whether to ENABLE/DISABLE 'Save button' in update booking
        if (
          state.updateBooking.experienceTotal !== state.initialStateUpdateBooking.experienceTotal ||
          state.updateBooking.deposit !== state.initialStateUpdateBooking.deposit
        ) {
          state.updateBooking.hasChanged.experienceTotal = true;
        } else {
          state.updateBooking.hasChanged.experienceTotal = false;
          // checking for the CASE when experineceTotal and original experineceTotal  are equal
          //  but the products quantity may be different
          if (state.updateBooking.products.length === state.initialStateUpdateBooking.products.length) {
            for (let i = 0; i < state.updateBooking.products.length; i++) {
              if (state.updateBooking.products[i].deposit !== state.updateBooking.products[i].originalDeposit) {
                state.updateBooking.hasChanged.products = true;
                break;
              } else {
                state.updateBooking.hasChanged.products = false;
              }
            }
          } else {
            state.updateBooking.hasChanged.products = true;
            break;
          }
        }
        break;
      case actions.PRODUCT_QTY_CHANGE:
        state = droppedProductQtyChange(state, action);
        break;
      case actions.PRODUCT_QTY_CHANGE_UPDATE_SCREEN:
        state = droppedProductQtyChangeUpdateScreen(state, action);
        // The following will check whether to ENABLE/DISABLE 'Save button' in update booking
        if (state.updateBooking.experienceTotal !== state.initialStateUpdateBooking.experienceTotal) {
          state.updateBooking.hasChanged.experienceTotal = true;
        } else {
          state.updateBooking.hasChanged.experienceTotal = false;
        }
        // checking for the CASE when experineceTotal and original experineceTotal  are equal
        //  but the products quantity may be different
        if (state.updateBooking.products.length === state.initialStateUpdateBooking.products.length) {
          for (let i = 0; i < state.updateBooking.products.length; i++) {
            if (state.updateBooking.products[i].qty !== state.initialStateUpdateBooking.products[i].qty) {
              console.log('Prdouct qty not equal for Prdouct ', state.updateBooking.products[i]);
              state.updateBooking.hasChanged.products = true;
              break;
            } else {
              state.updateBooking.hasChanged.products = false;
            }
          }
        } else {
          console.log('Prdouct length not equal');
          state.updateBooking.hasChanged.products = true;
          break;
        }
        break;
      case actions.PRODUCT_QTY_CHANGE_UPDATE_SCREEN_INITIAL:
        state = droppedProductQtyChangeUpdateScreen(state, action);
        // make deep copy
        state.initialStateUpdateBooking = JSON.parse(JSON.stringify(state.updateBooking));
        break;
      case actions.PLACE_PICKED:
        state = { ...state, place: action.payload };
        break;
      case actions.DATE_PICKED:
        console.log('date pick PAYLOAD', action.payload);
        state = { ...state, rulesError: false, date: action.payload };
        break;
      case actions.DATE_PICKED_UPDATE_SCREEN:
        state = { ...state, date: action.payload };
        // for making update in update booking state
        state.updateBooking = { ...state.updateBooking, date: action.payload, jumping_date: action.payload };

        // for enabling /disabling SAVE button
        if (!action.payload.isSame(state.initialStateUpdateBooking.jumping_date, 'days')) {
          state.updateBooking.hasChanged.jumping_date = true;
        } else {
          state.updateBooking.hasChanged.jumping_date = false;
        }
        break;
      case actions.CHANNEL_SELECTED:
        console.log('Channel', action.payload);
        state.newBooking.selectedChannel = action.payload;
        state = setChannelPrices(state, 'new');
        break;
      case actions.CHANNEL_SELECTED_UPDATE_SCREEN:
        state.newBooking.selectedChannel = action.payload;
        state = setChannelPrices(state, 'update');
        break;
      case actions.GET_AVAILABILITY:
        state = dateAvailiablityChange(state, action);
        break;
      case actions.SET_TANDEM_PRICES:
        state = setTandemPrices(state, action.payload.bookingType);
        break;
      case actions.SELECT_SLOT:
        state = { ...state, slotSelected: action.payload };
        state = setTandemPrices(state, 'new');
        break;
      case actions.SELECT_SLOT_UPDATE_SCREEN:
        // for update booking
        state.updateBooking = { ...state.updateBooking, checkin_time: action.payload.checkinTime };
        state = setTandemPrices(state, 'update');
        // for enabling /disabling SAVE button
        const checkinTimeOriginal = state.initialStateUpdateBooking.checkin_time;
        // for enabling /disabling SAVE button
        if (
          action.payload.checkinTime.hours !== checkinTimeOriginal.hours ||
          action.payload.checkinTime.minutes !== checkinTimeOriginal.minutes
        ) {
          state.updateBooking.hasChanged.checkin_time = true;
        } else {
          state.updateBooking.hasChanged.checkin_time = false;
        }
        break;
      case actions.GET_CHANNELS:
        state = { ...state, allChannels: action.payload };
        state.newBooking.selectedChannel = action.payload.length > 0 ? action.payload[0] : { accept_credit_card: true };
        // state.updateBooking.selectedChannel = action.payload.length > 0 ? action.payload[0] : {};
        // state = updatePrices(state, 'new');
        // state = updatePrices(state, 'update');
        break;
      case actions.GET_SOURCES:
        state = { ...state, allSources: action.payload };
        state.newBooking.selectedSource = action.payload.length > 0 ? action.payload[0] : {};
        break;
      case actions.SET_UPDATE_BOOKING:
        state.updateBooking = { ...state.updateBooking, ...action.payload };
        // make deep copy
        state.initialStateUpdateBooking = JSON.parse(JSON.stringify(state.updateBooking));
        break;
      case actions.SAVE_UPDATE_BOOKING:
        // make deep copy
        state.initialStateUpdateBooking = JSON.parse(JSON.stringify(state.updateBooking));
        // Changed hasChanged to False
        state.updateBooking.hasChanged.checkin_time = false;
        state.updateBooking.hasChanged.basic_info = false;
        state.updateBooking.hasChanged.experienceTotal = false;
        state.updateBooking.hasChanged.jumping_date = false;
        state.updateBooking.hasChanged.products = false;
        break;
      case actions.SUSPEND_UPDATE_BOOKING:
        state.updateBooking.status = 'SUSPEND';
        break;
      case actions.CANCEL_UPDATE_SCREEN_EDIT_CUSTOMER_INFO:
        // make deep copy
        state.updateBooking.firstname = state.initialStateUpdateBooking.firstname;
        state.updateBooking.surname = state.initialStateUpdateBooking.surname;
        state.updateBooking.email = state.initialStateUpdateBooking.email;
        state.updateBooking.phone = state.initialStateUpdateBooking.phone;
        state.updateBooking.phonecode = state.initialStateUpdateBooking.phonecode;
        state.updateBooking.consent18 = state.initialStateUpdateBooking.consent18;
        state.updateBooking.consent_weight = state.initialStateUpdateBooking.consent_weight;
        state.updateBooking.consent_weight = state.initialStateUpdateBooking.consent_weight;
        state.updateBooking.hasChanged.basic_info = false;
        break;
      default:
    }
  } catch (e) {
    console.log('ERROR', e);
  }
  return Object.assign({}, state);
};
