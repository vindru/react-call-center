export function calculatePrices(booking) {
  const { products, selectedChannel } = booking;
  let total = 0;
  let experienceTotal = 0;
  let deposit = 0;
  let promotion = 0;
  let subtotal = 0;
  let depositWithFee = 0;
  let alreadyPaid = 0;
  let channelDiscount = 0;
  const fee = 3 / 100;
  products.forEach(p => {
    if (['PRODUCT', 'OTHER_PRODUCT'].indexOf(p.type) > -1) {
      subtotal += p.price * p.qty;
    } else if (['PROMOTION'].indexOf(p.type) > -1) {
      promotion += p.price;
    } else if (['DEPOSIT'].indexOf(p.type) > -1) {
      deposit += p.price * p.qty;
    } else if (['CREDIT'].indexOf(p.type) > -1) {
      alreadyPaid += p.price * p.qty;
    }
  });

  experienceTotal = subtotal - promotion;
  const pendingAmount = experienceTotal - alreadyPaid;
  if (selectedChannel) {
    channelDiscount = (parseInt(selectedChannel.percentage_discount || 0, 10) * experienceTotal) / 100;
  }
  const totalDiscount = channelDiscount + promotion;
  total = experienceTotal - deposit - channelDiscount - alreadyPaid;
  depositWithFee = deposit + deposit * fee;

  // const channelPromotion = products.find(x => x.name === 'CHANNEL' && x.type === 'PROMOTION');
  // if (channelPromotion) {
  //   channelPromotion.price = channelDiscount;
  // }

  return {
    total,
    subtotal,
    deposit,
    depositWithFee,
    experienceTotal,
    totalDiscount,
    pendingAmount,
    alreadyPaid,
  };
}
export function reCalculatePriceAndUpdateState(pstate, { payload }) {
  const state = pstate;
  state.updateBooking = { ...state.updateBooking, ...calculatePrices(payload) };
  return state;
}

export function droppedProduct(pstate, { payload }) {
  const state = pstate;
  state.newBooking.products.push({ ...payload, qty: 1 });
  if (payload.deposit && payload.type !== 'PROMOTION') {
    state.newBooking.products.push({
      ...payload,
      type: 'DEPOSIT',
      price: payload.deposit,
      qty: 1,
    });
  }
  state.newBooking = { ...state.newBooking, ...calculatePrices(state.newBooking) };
  return state;
}

export function droppedProductUpdateScreen(pstate, { payload }) {
  const state = pstate;
  if (payload.qty !== 0) {
    state.updateBooking.products.push({ ...payload, qty: payload.qty });
    if (payload.createDeposit && payload.type !== 'PROMOTION') {
      state.updateBooking.products.push({
        ...payload,
        type: 'DEPOSIT',
        qty: 1,
      });
    }
    state.updateBooking = { ...state.updateBooking, ...calculatePrices(state.updateBooking) };
  }
  return state;
}

export function droppedProductDelete(pstate, { payload }) {
  const state = pstate;
  state.newBooking.products = state.newBooking.products.filter(p => p.id !== payload.id);
  state.newBooking = { ...state.newBooking, ...calculatePrices(state.newBooking) };
  return state;
}

export function droppedProductDeleteUpdateScreen(pstate, { payload }) {
  const state = pstate;
  state.updateBooking.products = state.updateBooking.products.filter(p => p.id !== payload.id);
  state.updateBooking = { ...state.updateBooking, ...calculatePrices(state.updateBooking) };
  return state;
}

export function droppedProductQtyChange(pstate, { payload }) {
  const state = pstate;
  const { id, qty } = payload;
  const product = state.newBooking.products.find(p => p.id === id && p.type !== 'DEPOSIT');
  const deposit = state.newBooking.products.find(p => p.id === id && p.type === 'DEPOSIT');
  if (product) {
    product.qty = qty;
  }
  if (deposit) {
    deposit.qty = qty;
  }
  state.newBooking = { ...state.newBooking, ...calculatePrices(state.newBooking) };
  return Object.assign({}, state);
}

export function droppedProductQtyChangeUpdateScreen(pstate, { payload }) {
  const state = pstate;
  const { id, qty } = payload;
  if (qty < 0) {
    return state;
  }
  const product = state.updateBooking.products.find(
    p => p.id === id && ['PRODUCT', 'OTHER_PRODUCT'].indexOf(p.type) > -1,
  );
  const deposit = state.updateBooking.products.find(p => p.id === id && p.type === 'DEPOSIT');
  const debit = state.updateBooking.products.find(p => p.id === id && p.type === 'DEBIT');
  const credit = state.updateBooking.products.find(p => p.id === id && p.type === 'CREDIT');

  console.log('In Dropped Producty Qty Change Update Screen - ', product, deposit, debit, credit);

  const change = qty - product.qty;
  console.log('Change ', change);
  product.qty += change;
  if (change < 0) {
    let remaining = -1 * change;
    if (deposit && deposit.qty > 0 && remaining > 0) {
      const q = deposit.qty;
      deposit.qty = deposit.qty - remaining || 0;
      remaining = q > remaining ? 0 : remaining - q;
    }
    if (debit && debit.qty > 0 && remaining > 0) {
      const q = debit.qty;
      debit.qty = debit.qty - remaining || 0;
      remaining = q > remaining ? 0 : remaining - q;
    }
    if (remaining !== 0) {
      console.log("Something's wrong", remaining);
    }
  } else if (deposit) {
    deposit.qty += change;
  } else if (credit && credit.qty >= product.qty) {
    // Add Debit until debit is equal to credit available
    if (debit) {
      debit.qty = credit.qty;
    }
  } else if (product.qty - (credit ? credit.qty : 0) > 0) {
    // After adding Debit equal to credit, whatever is left should be added as Deposit
    const depositQty = product.qty - (credit ? credit.qty : 0);
    state.updateBooking.products.push({
      ...product,
      type: 'DEPOSIT',
      qty: depositQty,
      price: product.deposit,
    });
  }
  if (deposit && deposit.qty === 0) {
    // Drop Deposit
    state.updateBooking.products = state.updateBooking.products.filter(
      p => p.id !== deposit.id || p.type !== 'DEPOSIT',
    );
  }
  if (debit && debit.qty === 0) {
    // Drop Debit
    state.updateBooking.products = state.updateBooking.products.filter(p => p.id !== debit.id || p.type !== 'DEBIT');
  }
  if (product && product.qty === 0) {
    // Drop Product
    state.updateBooking.products = state.updateBooking.products.filter(
      p => p.id !== product.id || ['PRODUCT', 'OTHER_PRODUCT'].indexOf(p.type) === -1,
    );
  }

  state.updateBooking = { ...state.updateBooking, ...calculatePrices(state.updateBooking) };
  return Object.assign({}, state);
}

export function depositAmtChange(pstate, { payload }, bookingType) {
  const { id, value } = payload;
  const state = pstate;
  let booking;
  if (bookingType === 'new') {
    booking = state.newBooking;
  } else if (bookingType === 'update') {
    booking = state.updateBooking;
  }
  const deposit = booking.products.find(p => p.type === payload.type && p.id === id);
  const product = booking.products.find(p => ['PRODUCT', 'OTHER_PRODUCT'].indexOf(p.type) !== -1 && p.id === id);
  product.deposit = parseInt(value, 10);
  deposit.price = value;
  booking = { ...booking, ...calculatePrices(booking) };
  if (bookingType === 'new') {
    state.newBooking = booking;
  } else if (bookingType === 'update') {
    state.updateBooking = booking;
  }
  return state;
}

export function productAmtChange(pstate, { payload }, bookingType) {
  const { id, value } = payload;
  const state = pstate;
  let booking;
  if (bookingType === 'new') {
    booking = state.newBooking;
  } else if (bookingType === 'update') {
    booking = state.updateBooking;
  }
  const product = booking.products.find(p => ['PRODUCT', 'OTHER_PRODUCT'].indexOf(p.type) !== -1 && p.id === id);
  product.price = parseInt(value, 10);
  booking = { ...booking, ...calculatePrices(booking) };
  if (bookingType === 'new') {
    state.newBooking = booking;
  } else if (bookingType === 'update') {
    state.updateBooking = booking;
  }
  return state;
}

export function dateAvailiablityChange(pstate, { payload, error }) {
  const state = pstate;
  console.log('line-state', state, payload, error);
  if (!error) {
    state.rules = new Array();
    payload.forEach(r => {
      // let rule = state.rules.find(x => r._id === x._id);
      // let rule = state.rules.find(x => new Date(r.date) === new Date(x.date));
      // if (rule) {
      //   rule = { ...rule, r };
      // } else {
      //   state.rules.push(r);
      // }
      state.rules.push(r);
    });
  } else {
    state.rulesError = true;
  }
  console.log('line-state', state);
  return Object.assign({}, state);
}

export function setChannelPrices(pstate, bookingType) {
  let state = pstate;
  if (bookingType === 'new') {
    const tandem = state.newBooking.products.find(x => x.name === 'Tandem' && x.type === 'PRODUCT');
    tandem.price = state.newBooking.selectedChannel ? state.newBooking.selectedChannel.fixed_price : tandem.price;
    state = {
      ...state,
      newBooking: { ...state.newBooking, ...calculatePrices(state.newBooking) },
    };
  } else if (bookingType === 'update') {
    state = {
      ...state,
      updateBooking: { ...state.updateBooking, ...calculatePrices(state.updateBooking) },
    };
  }

  return state;
}

export function setTandemPrices(pstate, bookingType) {
  let state = pstate;
  if (bookingType === 'update') {
    const tandem = state.updateBooking.products.find(x => x.id === 'tandem' && x.type === 'PRODUCT');
    const selected = state.rules.find(
      x =>
        parseInt(x.checkin_time.hours, 10) === state.updateBooking.checkin_time.hours &&
        parseInt(x.checkin_time.minutes, 10) === state.updateBooking.checkin_time.minutes,
    );
    if (selected) {
      tandem.max = selected.number_of_spaces - selected.number_of_spaces_booked;
    }
  } else {
    const selected = state.rules.find(
      x =>
        parseInt(x.checkin_time.hours, 10) === state.slotSelected.checkinTime.hours &&
        parseInt(x.checkin_time.minutes, 10) === state.slotSelected.checkinTime.minutes,
    );
    if (selected) {
      const tandem = state.newBooking.products.find(x => x.name === 'Tandem' && x.type === 'PRODUCT');
      const tandemDeposit = state.newBooking.products.find(x => x.name === 'Tandem' && x.type === 'DEPOSIT');
      tandem.price = selected ? selected.invoice_price : tandem.price;
      tandem.max = selected.number_of_spaces - selected.number_of_spaces_booked;
      tandem.deposit = selected.deposit_price;
      tandemDeposit.price = selected ? selected.deposit_price * tandem.qty : tandemDeposit.price;
      if (tandem.max < tandem.qty) {
        tandem.qty = tandem.max;
        tandemDeposit.price = tandem.qty * tandem.deposit;
      }
      state = {
        ...state,
        rules: [...state.rules],
        newBooking: { ...state.newBooking, ...calculatePrices(state.newBooking) },
      };
    }
  }
  return state;
}
