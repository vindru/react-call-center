﻿const TOGGLE_MENU = 'TOGGLE_MENU';
const ACTIVE_ITEM = 'ACTIVE_ITEM';

const initialState = {
  smallMenu: true,
  activeItem: 'dashboard',
};

export const actionCreators = {
  toggleSideMenu: () => ({
    type: TOGGLE_MENU,
  }),
  setActiveItem: payload => ({
    type: ACTIVE_ITEM,
    payload,
  }),
};

export const reducer = (pState, action) => {
  const state = pState || initialState;
  switch (action.type) {
    case TOGGLE_MENU:
      return {
        ...state,
        smallMenu: !state.smallMenu,
      };
    case ACTIVE_ITEM:
      return {
        ...state,
        activeItem: action.payload,
      };
    default:
  }
  return state;
};
