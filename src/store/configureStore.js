﻿import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import logger from 'redux-logger';
import promiseMiddleware from 'redux-promise';
import * as Card from './Card';
import * as SideMenu from './SideMenu';
import * as UserManagement from './UserManagement';
import * as SearchStore from './SearchStore';
import Bookings from './Bookings';
import Products from './Products';

export default function configureStore(history, initialState) {
  const reducers = {
    SideMenu: SideMenu.reducer,
    card: Card.reducer,
    userManagement: UserManagement.reducer,
    searchStore: SearchStore.reducer,
    Bookings,
    Products,
  };

  const middleware = [promiseMiddleware, thunk, routerMiddleware(history), logger];

  // In development, use the browser's Redux dev tools extension if installed
  const enhancers = [];
  const isDevelopment = process.env.NODE_ENV === 'development';
  if (isDevelopment && typeof window !== 'undefined' && window.devToolsExtension) {
    enhancers.push(window.devToolsExtension());
  }

  const rootReducer = combineReducers({
    ...reducers,
    routing: routerReducer,
  });

  return createStore(
    rootReducer,
    initialState,
    compose(
      applyMiddleware(...middleware),
      ...enhancers,
    ),
  );
}
