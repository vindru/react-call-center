import Swagger from 'swagger-client';

export const bookings = new Promise(resolve =>
  Swagger(`${process.env.REACT_APP_BOOKING_SERVER_URL}/api-docs`).then(client => {
    console.log(client.apis);
    resolve(client.apis);
  }),
);
export const products = new Promise(resolve =>
  Swagger(`${process.env.REACT_APP_PRODUCT_SERVER_URL}/api-docs`).then(client => {
    console.log(client.apis);
    resolve(client.apis);
  }),
);

export const channels = new Promise(resolve =>
  Swagger(`${process.env.REACT_APP_CHANNEL_SERVER_URL}/api-docs`).then(client => {
    console.log(client.apis);
    resolve(client.apis);
  }),
);

export const invoice = new Promise(resolve =>
  Swagger(`${process.env.REACT_APP_INVOICE_SERVER_URL}/api-docs`).then(client => {
    console.log(client.apis);
    resolve(client.apis);
  }),
);

export const BookingAPI = `${process.env.REACT_APP_BOOKING_SERVER_URL}`;
export const InvoiceAPI = `${process.env.REACT_APP_INVOICE_SERVER_URL}/api/v1/invoice`;
