const actions = {
  GET_PRODUCTS: 'GET_PRODUCTS',
};
export default (pState, action) => {
  let state = pState;
  switch (action.type) {
    case actions.GET_PRODUCTS:
      state = [...action.payload];
      return Object.assign([], state);
    default:
      return Object.assign([], state);
  }
};
