import React, { useState } from 'react';
import { Input, Button } from 'semantic-ui-react';

import './Counter.scss';

export default ({ qty, min, max, onChange }) => {
  const initialCount = qty === parseInt(qty, 10) ? qty : 1;
  const [count, setCount] = useState(initialCount);
  const maxCount = max || 20;
  const minCount = min === parseInt(min, 10) ? min : 1;
  if (count > maxCount) {
    setCount(maxCount);
  }
  const handleChange = value => {
    setCount(value);
    onChange(value);
  };
  return (
    <Button.Group className="counter">
      <Button icon basic onClick={() => handleChange(count > minCount ? count - 1 : minCount)}>
        -
      </Button>
      <Input value={count} type="number" />
      <Button icon basic color="blue" onClick={() => handleChange(count < maxCount ? count + 1 : maxCount)}>
        +
      </Button>
    </Button.Group>
  );
};
