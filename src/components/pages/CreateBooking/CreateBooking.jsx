import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Divider, Grid, Form, Header, Modal, Button } from 'semantic-ui-react';
import { DndProvider } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import moment from 'moment';

import './CreateBooking.scss';
import axios from 'axios';
import Notes from '../Notes';
import {
  BookingTimePlacePickerBar,
  CustomerInfo,
  BookingInfo,
  ProductsAndPromotions,
  ChannelInfo,
  SourceInfo,
  PickUpInfo,
  SaveForLater,
  ErrorsMessage,
} from './sections';
import {
  createBookingWithPayment,
  getProducts,
  createBookingWithoutPayment,
  getAvailablityByDate,
} from '../../../store/Bookings/Bookings.actions';

class CreateBooking extends Component {
  constructor(props) {
    super(props);

    this.state = {
      notes: [],
      openConfirmModal: false,
      redirect: false,
      redirectUrl: '',
      message: '',
      isCreatingBooking: false,
      isBookingSuccessful: false,
      isSavingForLater: false,
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.updatePayment = this.updatePayment.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.storeNotes = this.storeNotes.bind(this);
    this.CreateNotes = this.CreateNotes.bind(this);
  }

  componentDidMount() {
    // Tandem should already be added on load.
    this.props.resetBookingInfo();
    const { Bookings } = this.props;
    const { date } = Bookings;

    this.props.setPlace(1);
    console.log('line_id', date);
    getAvailablityByDate(date, 1).then(rules => {
      this.props.setAvailability(rules);
      this.props.setTandemPrices({ bookingType: 'new' });
    });
    this.props.fetchProducts(1);
  }

  // on payment form submit handler
  onSubmit() {
    // Validate the data and create the booking.

    const { Bookings } = { ...this.props };
    const { newBooking, paymentInfo } = { ...Bookings };

    const errors = [];
    if (newBooking.firstname === '') {
      errors.push({ id: 'firstName', message: 'First Name cannot be empty.' });
    }
    if (newBooking.surname === '') {
      errors.push({ id: 'surname', message: 'Surname cannot be empty.' });
    }
    if (newBooking.email === '') {
      errors.push({ id: 'email', message: 'Email cannot be empty.' });
    }
    if (newBooking.phonecode === '') {
      errors.push({ id: 'phonecode', message: 'Phone code cannot be empty.' });
    }
    if (newBooking.phonenumber === '') {
      errors.push({ id: 'phoneNumber', message: 'Phone number cannot be empty.' });
    }
    if (newBooking.products.length === 0) {
      errors.push({ id: 'products', message: 'Total Products cannot be zero.' });
    }
    if (!newBooking.consent18) {
      errors.push({ id: 'consent18', message: 'Person should be above 18.' });
    }
    if (!newBooking.consent_weight) {
      errors.push({ id: 'consent_weight', message: 'Person should be in given weight limit.' });
    }
    if (newBooking.depositWithFee > 0 && newBooking.selectedChannel.accept_credit_card) {
      if (paymentInfo.cvv.length !== 3) {
        errors.push({ id: 'cvv', message: 'CVV is Invalid. Should be 3 digits' });
      }
      if (paymentInfo.cardNumber.length !== 16) {
        errors.push({ id: 'cardNumber', message: 'Card Number is Invalid. Should be 16 digits.' });
      }
      const regexExpireDate = new RegExp('^(0[1-9]|1[0-2])\\/([0-9]{2}|[0-9]{2})$');
      if (!regexExpireDate.test(paymentInfo.expireDate)) {
        errors.push({ id: 'expireDate', message: 'Expire Date is Invalid. Should be of the form MM/YY.' });
      }
      if (paymentInfo.country === '') {
        errors.push({ id: 'country', message: 'Country name is required.' });
      }
      if (paymentInfo.zipCode.length !== 5) {
        errors.push({ id: 'zipCode', message: 'Zip Code should be of 5 digits.' });
      }
    }

    // Find Selected Slot
    const selectedSlot = Bookings.rules.find(
      x =>
        x.checkin_time.hours === Bookings.slotSelected.checkinTime.hours &&
        x.checkin_time.minutes === Bookings.slotSelected.checkinTime.minutes,
    );
    if (!selectedSlot) {
      errors.push({ id: 'slot', message: 'Slot is not chosen.' });
      // selectedSlot = { checkin_time: { hours: '9', minutes: '30' } };
    }

    const payload = {};
    payload.errors = errors;
    this.props.updateBookingInfo(payload);

    console.log('Errors', errors);

    if (errors.length === 0) {
      // Create Booking
      this.setState({ isCreatingBooking: true });

      // Set Timeout to avoid freezing of loader.
      this.bookingTimeout = setTimeout(() => {
        this.setState({
          openConfirmModal: true,
          redirect: false,
          message: 'Timed out',
          isCreatingBooking: false,
          isBookingSuccessful: false,
        });
        clearTimeout(this.bookingTimeout);
      }, 30000);
      // Calculate addOns
      let addOnProducts = newBooking.products.filter(
        p => ['PRODUCT', 'OTHER_PRODUCT', 'PROMOTIONS'].indexOf(p.type) !== -1 && p.name !== 'Tandem',
      );
      addOnProducts = addOnProducts.map(product => ({
        code: product.id,
        label: product.name,
        amount: product.price,
        quantity: product.qty,
        forUser: [],
        type: product.type,
      }));
      // Calculate Total Tandem
      const tandemProduct = newBooking.products.find(x => x.name === 'Tandem' && x.type === 'PRODUCT');
      const tandemDeposit = newBooking.products.find(x => x.name === 'Tandem' && x.type === 'DEPOSIT');

      const totalDeposit = tandemProduct ? tandemDeposit.price : 0;
      const totalQty = tandemProduct ? tandemProduct.qty : 0;

      const depositPerTandem = totalQty ? totalDeposit / totalQty : 0;
      const pricePerTandem = tandemProduct ? tandemProduct.price : 0;

      const requestBody = {
        location_id: Bookings.place,
        name: newBooking.firstname,
        surname: newBooking.surname,
        email: newBooking.email,
        phone: `${newBooking.phonecode}${newBooking.phonenumber}`,
        number_of_spaces: totalQty,
        space_price: pricePerTandem, // price of one tandem
        space_deposit_price: depositPerTandem * totalQty, // deposit price of all tandems
        addons: addOnProducts,
        checkin_time: { hours: selectedSlot.checkin_time.hours, minutes: selectedSlot.checkin_time.minutes },
        date: Bookings.date.toDate(),
        consent18: newBooking.consent18,
        consent_weight: newBooking.consent_weight,
        channel: newBooking.selectedChannel.channel_name || '',
        source: newBooking.selectedSource.source_name || '',
      };

      if (newBooking.pickUpRequired) {
        requestBody.pickup_time = newBooking.selectedPickUpTime;
        requestBody.pickup_spot = newBooking.selectedSpot;
      }

      console.log('Request Body', requestBody);
      let request;
      if (newBooking.depositWithFee > 0 && newBooking.selectedChannel.accept_credit_card) {
        request = createBookingWithPayment(requestBody, paymentInfo);
      } else {
        request = createBookingWithoutPayment(requestBody);
      }
      request
        .then(async resp => {
          console.log('Response Create Booking', resp);
          clearTimeout(this.bookingTimeout);
          if (resp.status === 'successful' || resp.booking_id) {
            // createBookingWithoutPayment does not send status so booking_id is used.
            this.CreateNotes(resp.booking_id);
            // Show alert and redirect to Home.
            // await this.props.resetBookingInfo();
            console.log(resp.data);
            this.setState({
              openConfirmModal: true,
              redirect: true,
              redirectUrl: `/UpdateBooking/${resp.booking_id}`,
              message: 'Booking Created.',
              isCreatingBooking: false,
              isBookingSuccessful: true,
            });
          } else {
            // Show alert and redirect to Home.
            this.setState({
              openConfirmModal: true,
              redirect: false,
              message: 'Error while creating booking',
              isCreatingBooking: false,
            });
          }
        })
        .catch(err => {
          console.log('Error in Create booking', err);
          clearTimeout(this.bookingTimeout);
          this.setState({
            openConfirmModal: true,
            redirect: false,
            message: 'Error while creating booking',
            isCreatingBooking: false,
          });
        });
    }
  }

  storeNotes(note) {
    const { notes } = this.state;
    notes.push(note);
    this.setState({ notes });
    console.log(this.state.notes);
  }

  closeModal() {
    this.setState({ openConfirmModal: false });
    if (this.state.redirect) {
      this.props.history.push('/');
    }
  }

  updatePayment(e, d) {
    const payload = {};
    payload[d.name] = d.value;
    console.log('Payment Info payload', payload);
    this.props.updatePaymentInfo(payload);
  }

  CreateNotes(bookingId) {
    this.state.notes.forEach(item => {
      const Payload = {
        booking_id: bookingId,
        executive_id: 'user',
        note: item.note,
        date: item.date,
      };
      axios({
        method: 'post',
        url: `${process.env.REACT_APP_BOOKING_SERVER_URL}/api/v1/note`, // ---|Broadcast api link
        config: { headers: { 'Content-Type': 'multipart/form-data' } },
        data: Payload,
      })
        .then(() => {
          this.getNotes();
          console.log('note added');
          this.setState({
            noteInTheMaking: '',
          });
        })
        .catch(err => {
          console.log('catch 2 error', err.message);
          this.setState({ BroadcastError: 'Error in adding note' });
        });
    });
  }

  render() {
    const { newBooking } = { ...this.props.Bookings };
    const { history } = { ...this.props };
    const { openConfirmModal, redirect, message, isCreatingBooking, isBookingSuccessful } = { ...this.state };
    return (
      <DndProvider backend={HTML5Backend}>
        <Grid columns="16" celled="internally" id="CreateBookingPage">
          <Modal
            open={openConfirmModal}
            onClose={() => this.closeModal()}
            closeOnEscape={() => this.closeModal()}
            closeOnDimmerClick={() => this.closeModal()}
            size="mini">
            <Modal.Content>
              <Header>{message}</Header>
            </Modal.Content>
            <Modal.Actions>
              {isBookingSuccessful && (
                <Button
                  color="green"
                  onClick={() => {
                    if (this.state.redirectUrl !== '') {
                      this.props.history.push(this.state.redirectUrl);
                    } else {
                      this.props.history.push('/');
                    }
                  }}
                  inverted>
                  OK
                </Button>
              )}
              {!isBookingSuccessful && (
                <Button color="red" onClick={() => this.closeModal()} inverted>
                  Close
                </Button>
              )}
            </Modal.Actions>
          </Modal>
          <Grid.Row className="header-bar">
            <Grid.Column width={16}>
              <Header as="h2" className="mt-5 main-title">
                Create Booking
              </Header>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row className="date-picker-bar">
            <Grid.Column width={13}>
              <BookingTimePlacePickerBar />
            </Grid.Column>
            <Grid.Column width={3}>
              <SaveForLater />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row stretched className="booking-section">
            <Grid.Column width={3}>
              <CustomerInfo />
              <Divider />
              <ChannelInfo />
              <Divider />
              <SourceInfo />
              <Divider />
              <PickUpInfo />
            </Grid.Column>
            <Grid.Column width={5} id="BookingInfo">
              <BookingInfo />
            </Grid.Column>
            <Grid.Column width={8}>
              <Grid.Row>
                <ProductsAndPromotions />
              </Grid.Row>
              <Divider />
              <Grid.Row>
                <Form onSubmit={this.onSubmit}>
                  {newBooking.depositWithFee > 0 && newBooking.selectedChannel.accept_credit_card && (
                    <React.Fragment>
                      <Header as="h3">Payment</Header>
                      <Form.Group widths="equal">
                        <Form.Input
                          fluid
                          placeholder="Enter Card Number"
                          name="cardNumber"
                          onChange={this.updatePayment}
                        />
                        <Form.Input
                          fluid
                          placeholder="Enter Name on Card"
                          name="name_on_the_card"
                          onChange={this.updatePayment}
                        />
                        <Form.Input fluid placeholder="MM/YY" name="expireDate" onChange={this.updatePayment} />
                        <Form.Input fluid placeholder="CVV" name="cvv" onChange={this.updatePayment} />
                      </Form.Group>
                      <Form.Group>
                        <Form.Input
                          width={8}
                          fluid
                          placeholder="Enter Address"
                          name="country"
                          onChange={this.updatePayment}
                        />
                        <Form.Input
                          width={4}
                          fluid
                          placeholder="Enter Zip Code"
                          name="zipCode"
                          onChange={this.updatePayment}
                        />
                      </Form.Group>
                    </React.Fragment>
                  )}
                  {!newBooking.selectedChannel.accept_credit_card && (
                    <Header as="h5">*Only cash accepted. No credit card or change the channel provided.</Header>
                  )}
                  <ErrorsMessage />
                  <Form.Button disabled={isCreatingBooking} loading={isCreatingBooking} primary>
                    {newBooking.depositWithFee > 0 && newBooking.selectedChannel.accept_credit_card
                      ? 'Pay Now'
                      : 'Create booking'}
                  </Form.Button>
                </Form>
              </Grid.Row>
              <Divider />
              <Grid.Row>
                <Notes
                  Data=""
                  type="panel"
                  date={this.props.Bookings.date.toDate()}
                  id="newBooking"
                  MakeNotes={this.storeNotes}
                />
              </Grid.Row>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </DndProvider>
    );
  }
}

const mapStateToProps = state => ({
  ...state,
});

const mapDispatchToProps = dispatch => ({
  updateBookingInfo: payload => dispatch({ type: 'BASICINFO_CHANGE', payload }),
  setPlace: placeId => dispatch({ type: 'PLACE_PICKED', payload: placeId }),
  fetchProducts: placeId => dispatch(getProducts(placeId)),
  setAvailability: payload => dispatch({ type: 'GET_AVAILABILITY', payload }),
  updatePaymentInfo: payload => dispatch({ type: 'PAYMENTINFO_CHANGE', payload }),
  resetBookingInfo: () => dispatch({ type: 'RESET_BOOKINGINFO' }),
  setTandemPrices: payload => dispatch({ type: 'SET_TANDEM_PRICES', payload }),
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(CreateBooking),
);
