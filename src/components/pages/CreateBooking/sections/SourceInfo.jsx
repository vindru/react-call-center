import React from 'react';
import { connect } from 'react-redux';
import { Dropdown, Form } from 'semantic-ui-react';
import { getSources } from '../../../../store/Bookings/Bookings.actions';

class SourceInfo extends React.Component {
  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
  }

  componentWillMount() {
    this.props.fetchSources();
  }

  onChange(e, d) {
    const payload = {};
    payload[d.name] = d.value;
    this.props.updateBasicInfo(payload);
  }

  render() {
    const { selectedSource, allSources } = this.props;
    let sourceOptions;
    try {
      sourceOptions = allSources.map(source => ({
        key: source['__v'],
        value: source,
        text: source.source_name,
      }));
    } catch (e) {
      console.log('ERROR:', e);
    }

    return (
      <React.Fragment>
        <Form>
          <Form.Field>
            <label htmlFor="channel">SELECT SOURCE</label>
            <Dropdown
              name="selectedSource"
              placeholder="Select Channel"
              fluid
              selection
              options={sourceOptions}
              value={selectedSource}
              onChange={this.onChange}
            />
          </Form.Field>
        </Form>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  selectedSource: state.Bookings.newBooking.selectedSource,
  allSources: state.Bookings.allSources,
});

const mapDispatchToProps = dispatch => ({
  fetchSources: () => dispatch(getSources()),
  updateBasicInfo: payload => dispatch({ type: 'BASICINFO_CHANGE', payload }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SourceInfo);
