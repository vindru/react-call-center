import React from 'react';
import { connect } from 'react-redux';
import { Message } from 'semantic-ui-react';
import { withRouter } from 'react-router';

const ErrorMessages = ({ Bookings }) => {
  if (Bookings.newBooking.errors.length > 0) {
    const { errors } = { ...Bookings.newBooking };
    let errorsList;
    try {
      errorsList = errors.map(error => <Message.Item>{error.message}</Message.Item>);
    } catch (e) {
      console.log('ERRROR:', e);
    }
    return (
      <Message negative>
        <Message.Header>Errors</Message.Header>
        <Message.List>{errorsList}</Message.List>
      </Message>
    );
  }
  return '';
};

export default withRouter(connect(s => ({ Bookings: s.Bookings }))(ErrorMessages));
