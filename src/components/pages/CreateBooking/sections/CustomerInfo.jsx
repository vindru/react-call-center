import React from 'react';
import { connect } from 'react-redux';
import { Checkbox, Form, Input, Header, Dropdown } from 'semantic-ui-react';
import './CustomerInfo.scss';

const phonecode = [
  { key: '+972 Israel', text: '+972', value: '+972' },
  { key: '+93 Afghanistan', text: '+93', value: '+93' },
  { key: '+355 Albania', text: '+355', value: '+355' },
  { key: '+213 Algeria', text: '+213', value: '+213' },
  { key: '+1 684 AmericanSamoa', text: '+1 684', value: '+1 684' },
  { key: '+376 Andorra', text: '+376', value: '+376' },
  { key: '+244 Angola', text: '+244', value: '+244' },
  { key: '+1 264 Anguilla', text: '+1 264', value: '+1 264' },
  { key: '+1268 Antigua and Barbuda', text: '+1268', value: '+1268' },
  { key: '+54 Argentina', text: '+54', value: '+54' },
  { key: '+374 Armenia', text: '+374', value: '+374' },
  { key: '+297 Aruba', text: '+297', value: '+297' },
  { key: '+61 Australia', text: '+61', value: '+61' },
  { key: '+43 Austria', text: '+43', value: '+43' },
  { key: '+994 Azerbaijan', text: '+994', value: '+994' },
  { key: '+1 242 Bahamas', text: '+1 242', value: '+1 242' },
  { key: '+973 Bahrain', text: '+973', value: '+973' },
  { key: '+880 Bangladesh', text: '+880', value: '+880' },
  { key: '+1 246 Barbados', text: '+1 246', value: '+1 246' },
  { key: '+375 Belarus', text: '+375', value: '+375' },
  { key: '+32 Belgium', text: '+32', value: '+32' },
  { key: '+501 Belize', text: '+501', value: '+501' },
  { key: '+229 Benin', text: '+229', value: '+229' },
  { key: '+1 441 Bermuda', text: '+1 441', value: '+1 441' },
  { key: '+975 Bhutan', text: '+975', value: '+975' },
  { key: '+387 Bosnia and Herzegovina', text: '+387', value: '+387' },
  { key: '+267 Botswana', text: '+267', value: '+267' },
  { key: '+55 Brazil', text: '+55', value: '+55' },
  { key: '+246 British Indian Ocean Territory', text: '+246', value: '+246' },
  { key: '+359 Bulgaria', text: '+359', value: '+359' },
  { key: '+226 Burkina Faso', text: '+226', value: '+226' },
  { key: '+257 Burundi', text: '+257', value: '+257' },
  { key: '+855 Cambodia', text: '+855', value: '+855' },
  { key: '+237 Cameroon', text: '+237', value: '+237' },
  { key: '+1 Canada', text: '+1', value: '+1' },
  { key: '+238 Cape Verde', text: '+238', value: '+238' },
  { key: '+ 345 Cayman Islands', text: '+ 345', value: '+ 345' },
  { key: '+236 Central African Republic', text: '+236', value: '+236' },
  { key: '+235 Chad', text: '+235', value: '+235' },
  { key: '+56 Chile', text: '+56', value: '+56' },
  { key: '+86 China', text: '+86', value: '+86' },
  { key: '+61 Christmas Island', text: '+61', value: '+61' },
  { key: '+57 Colombia', text: '+57', value: '+57' },
  { key: '+269 Comoros', text: '+269', value: '+269' },
  { key: '+242 Congo', text: '+242', value: '+242' },
  { key: '+682 Cook Islands', text: '+682', value: '+682' },
  { key: '+506 Costa Rica', text: '+506', value: '+506' },
  { key: '+385 Croatia', text: '+385', value: '+385' },
  { key: '+53 Cuba', text: '+53', value: '+53' },
  { key: '+537 Cyprus', text: '+537', value: '+537' },
  { key: '+420 Czech Republic', text: '+420', value: '+420' },
  { key: '+45 Denmark', text: '+45', value: '+45' },
  { key: '+253 Djibouti', text: '+253', value: '+253' },
  { key: '+1 767 Dominica', text: '+1 767', value: '+1 767' },
  { key: '+1 849 Dominican Republic', text: '+1 849', value: '+1 849' },
  { key: '+593 Ecuador', text: '+593', value: '+593' },
  { key: '+20 Egypt', text: '+20', value: '+20' },
  { key: '+503 El Salvador', text: '+503', value: '+503' },
  { key: '+240 Equatorial Guinea', text: '+240', value: '+240' },
  { key: '+291 Eritrea', text: '+291', value: '+291' },
  { key: '+372 Estonia', text: '+372', value: '+372' },
  { key: '+251 Ethiopia', text: '+251', value: '+251' },
  { key: '+298 Faroe Islands', text: '+298', value: '+298' },
  { key: '+679 Fiji', text: '+679', value: '+679' },
  { key: '+358 Finland', text: '+358', value: '+358' },
  { key: '+33 France', text: '+33', value: '+33' },
  { key: '+594 French Guiana', text: '+594', value: '+594' },
  { key: '+689 French Polynesia', text: '+689', value: '+689' },
  { key: '+241 Gabon', text: '+241', value: '+241' },
  { key: '+220 Gambia', text: '+220', value: '+220' },
  { key: '+995 Georgia', text: '+995', value: '+995' },
  { key: '+49 Germany', text: '+49', value: '+49' },
  { key: '+233 Ghana', text: '+233', value: '+233' },
  { key: '+350 Gibraltar', text: '+350', value: '+350' },
  { key: '+30 Greece', text: '+30', value: '+30' },
  { key: '+299 Greenland', text: '+299', value: '+299' },
  { key: '+1 473 Grenada', text: '+1 473', value: '+1 473' },
  { key: '+590 Guadeloupe', text: '+590', value: '+590' },
  { key: '+1 671 Guam', text: '+1 671', value: '+1 671' },
  { key: '+502 Guatemala', text: '+502', value: '+502' },
  { key: '+224 Guinea', text: '+224', value: '+224' },
  { key: '+245 Guinea-Bissau', text: '+245', value: '+245' },
  { key: '+595 Guyana', text: '+595', value: '+595' },
  { key: '+509 Haiti', text: '+509', value: '+509' },
  { key: '+504 Honduras', text: '+504', value: '+504' },
  { key: '+36 Hungary', text: '+36', value: '+36' },
  { key: '+354 Iceland', text: '+354', value: '+354' },
  { key: '+91 India', text: '+91', value: '+91' },
  { key: '+62 Indonesia', text: '+62', value: '+62' },
  { key: '+964 Iraq', text: '+964', value: '+964' },
  { key: '+353 Ireland', text: '+353', value: '+353' },

  { key: '+39 Italy', text: '+39', value: '+39' },
  { key: '+1 876 Jamaica', text: '+1 876', value: '+1 876' },
  { key: '+81 Japan', text: '+81', value: '+81' },
  { key: '+962 Jordan', text: '+962', value: '+962' },
  { key: '+7 7 Kazakhstan', text: '+7 7', value: '+7 7' },
  { key: '+254 Kenya', text: '+254', value: '+254' },
  { key: '+686 Kiribati', text: '+686', value: '+686' },
  { key: '+965 Kuwait', text: '+965', value: '+965' },
  { key: '+996 Kyrgyzstan', text: '+996', value: '+996' },
  { key: '+371 Latvia', text: '+371', value: '+371' },
  { key: '+961 Lebanon', text: '+961', value: '+961' },
  { key: '+266 Lesotho', text: '+266', value: '+266' },
  { key: '+231 Liberia', text: '+231', value: '+231' },
  { key: '+423 Liechtenstein', text: '+423', value: '+423' },
  { key: '+370 Lithuania', text: '+370', value: '+370' },
  { key: '+352 Luxembourg', text: '+352', value: '+352' },
  { key: '+261 Madagascar', text: '+261', value: '+261' },
  { key: '+265 Malawi', text: '+265', value: '+265' },
  { key: '+60 Malaysia', text: '+60', value: '+60' },
  { key: '+960 Maldives', text: '+960', value: '+960' },
  { key: '+223 Mali', text: '+223', value: '+223' },
  { key: '+356 Malta', text: '+356', value: '+356' },
  { key: '+692 Marshall Islands', text: '+692', value: '+692' },
  { key: '+596 Martinique', text: '+596', value: '+596' },
  { key: '+222 Mauritania', text: '+222', value: '+222' },
  { key: '+230 Mauritius', text: '+230', value: '+230' },
  { key: '+262 Mayotte', text: '+262', value: '+262' },
  { key: '+52 Mexico', text: '+52', value: '+52' },
  { key: '+377 Monaco', text: '+377', value: '+377' },
  { key: '+976 Mongolia', text: '+976', value: '+976' },
  { key: '+382 Montenegro', text: '+382', value: '+382' },
  { key: '+1664 Montserrat', text: '+1664', value: '+1664' },
  { key: '+212 Morocco', text: '+212', value: '+212' },
  { key: '+95 Myanmar', text: '+95', value: '+95' },
  { key: '+264 Namibia', text: '+264', value: '+264' },
  { key: '+674 Nauru', text: '+674', value: '+674' },
  { key: '+977 Nepal', text: '+977', value: '+977' },
  { key: '+31 Netherlands', text: '+31', value: '+31' },
  { key: '+599 Netherlands Antilles', text: '+599', value: '+599' },
  { key: '+687 New Caledonia', text: '+687', value: '+687' },
  { key: '+64 New Zealand', text: '+64', value: '+64' },
  { key: '+505 Nicaragua', text: '+505', value: '+505' },
  { key: '+227 Niger', text: '+227', value: '+227' },
  { key: '+234 Nigeria', text: '+234', value: '+234' },
  { key: '+683 Niue', text: '+683', value: '+683' },
  { key: '+672 Norfolk Island', text: '+672', value: '+672' },
  { key: '+1 670 Northern Mariana Islands', text: '+1 670', value: '+1 670' },
  { key: '+47 Norway', text: '+47', value: '+47' },
  { key: '+968 Oman', text: '+968', value: '+968' },
  { key: '+92 Pakistan', text: '+92', value: '+92' },
  { key: '+680 Palau', text: '+680', value: '+680' },
  { key: '+507 Panama', text: '+507', value: '+507' },
  { key: '+675 Papua New Guinea', text: '+675', value: '+675' },
  { key: '+595 Paraguay', text: '+595', value: '+595' },
  { key: '+51 Peru', text: '+51', value: '+51' },
  { key: '+63 Philippines', text: '+63', value: '+63' },
  { key: '+48 Poland', text: '+48', value: '+48' },
  { key: '+351 Portugal', text: '+351', value: '+351' },
  { key: '+1 939 Puerto Rico', text: '+1 939', value: '+1 939' },
  { key: '+974 Qatar', text: '+974', value: '+974' },
  { key: '+40 Romania', text: '+40', value: '+40' },
  { key: '+250 Rwanda', text: '+250', value: '+250' },
  { key: '+685 Samoa', text: '+685', value: '+685' },
  { key: '+378 San Marino', text: '+378', value: '+378' },
  { key: '+966 Saudi Arabia', text: '+966', value: '+966' },
  { key: '+221 Senegal', text: '+221', value: '+221' },
  { key: '+381 Serbia', text: '+381', value: '+381' },
  { key: '+248 Seychelles', text: '+248', value: '+248' },
  { key: '+232 Sierra Leone', text: '+232', value: '+232' },
  { key: '+65 Singapore', text: '+65', value: '+65' },
  { key: '+421 Slovakia', text: '+421', value: '+421' },
  { key: '+386 Slovenia', text: '+386', value: '+386' },
  { key: '+677 Solomon Islands', text: '+677', value: '+677' },
  { key: '+27 South Africa', text: '+27', value: '+27' },
  { key: '+500 South Georgia and the South Sandwich Islands', text: '+500', value: '+500' },
  { key: '+34 Spain', text: '+34', value: '+34' },
  { key: '+94 Sri Lanka', text: '+94', value: '+94' },
  { key: '+249 Sudan', text: '+249', value: '+249' },
  { key: '+597 Suriname', text: '+597', value: '+597' },
  { key: '+268 Swaziland', text: '+268', value: '+268' },
  { key: '+46 Sweden', text: '+46', value: '+46' },
  { key: '+41 Switzerland', text: '+41', value: '+41' },
  { key: '+992 Tajikistan', text: '+992', value: '+992' },
  { key: '+66 Thailand', text: '+66', value: '+66' },
  { key: '+228 Togo', text: '+228', value: '+228' },
  { key: '+690 Tokelau', text: '+690', value: '+690' },
  { key: '+676 Tonga', text: '+676', value: '+676' },
  { key: '+1 868 Trinidad and Tobago', text: '+1 868', value: '+1 868' },
  { key: '+216 Tunisia', text: '+216', value: '+216' },
  { key: '+90 Turkey', text: '+90', value: '+90' },
  { key: '+993 Turkmenistan', text: '+993', value: '+993' },
  { key: '+1 649 Turks and Caicos Islands', text: '+1 649', value: '+1 649' },
  { key: '+688 Tuvalu', text: '+688', value: '+688' },
  { key: '+256 Uganda', text: '+256', value: '+256' },
  { key: '+380 Ukraine', text: '+380', value: '+380' },
  { key: '+971 United Arab Emirates', text: '+971', value: '+971' },
  { key: '+44 United Kingdom', text: '+44', value: '+44' },
  { key: '+1 United States', text: '+1', value: '+1' },
  { key: '+598 Uruguay', text: '+598', value: '+598' },
  { key: '+998 Uzbekistan', text: '+998', value: '+998' },
  { key: '+678 Vanuatu', text: '+678', value: '+678' },
  { key: '+681 Wallis and Futuna', text: '+681', value: '+681' },
  { key: '+967 Yemen', text: '+967', value: '+967' },
  { key: '+260 Zambia', text: '+260', value: '+260' },
  { key: '+263 Zimbabwe', text: '+263', value: '+263' },
  { key: ' land Islands', text: '', value: '' },
  { key: 'null Antarctica', text: null, value: null },
  { key: '+591 Bolivia, Plurinational State of', text: '+591', value: '+591' },
  { key: '+673 Brunei Darussalam', text: '+673', value: '+673' },
  { key: '+61 Cocos (Keeling) Islands', text: '+61', value: '+61' },
  { key: '+243 Congo, The Democratic Republic of the', text: '+243', value: '+243' },
  { key: "+225 Cote d'Ivoire", text: '+225', value: '+225' },
  { key: '+500 Falkland Islands (Malvinas)', text: '+500', value: '+500' },
  { key: '+44 Guernsey', text: '+44', value: '+44' },
  { key: '+379 Holy See (Vatican City State)', text: '+379', value: '+379' },
  { key: '+852 Hong Kong', text: '+852', value: '+852' },
  { key: '+98 Iran, Islamic Republic of', text: '+98', value: '+98' },
  { key: '+44 Isle of Man', text: '+44', value: '+44' },
  { key: '+44 Jersey', text: '+44', value: '+44' },
  { key: "+850 Korea, Democratic People's Republic of", text: '+850', value: '+850' },
  { key: '+82 Korea, Republic of', text: '+82', value: '+82' },
  { key: "+856 Lao People's Democratic Republic", text: '+856', value: '+856' },
  { key: '+218 Libyan Arab Jamahiriya', text: '+218', value: '+218' },
  { key: '+853 Macao', text: '+853', value: '+853' },
  { key: '+389 Macedonia, The Former Yugoslav Republic of', text: '+389', value: '+389' },
  { key: '+691 Micronesia, Federated States of', text: '+691', value: '+691' },
  { key: '+373 Moldova, Republic of', text: '+373', value: '+373' },
  { key: '+258 Mozambique', text: '+258', value: '+258' },
  { key: '+970 Palestinian Territory, Occupied', text: '+970', value: '+970' },
  { key: '+872 Pitcairn', text: '+872', value: '+872' },
  { key: '+262 Réunion', text: '+262', value: '+262' },
  { key: '+7 Russia', text: '+7', value: '+7' },
  { key: '+590 Saint Barthélemy', text: '+590', value: '+590' },
  { key: '+290 Saint Helena, Ascension and Tristan Da Cunha', text: '+290', value: '+290' },
  { key: '+1 869 Saint Kitts and Nevis', text: '+1 869', value: '+1 869' },
  { key: '+1 758 Saint Lucia', text: '+1 758', value: '+1 758' },
  { key: '+590 Saint Martin', text: '+590', value: '+590' },
  { key: '+508 Saint Pierre and Miquelon', text: '+508', value: '+508' },
  { key: '+1 784 Saint Vincent and the Grenadines', text: '+1 784', value: '+1 784' },
  { key: '+239 Sao Tome and Principe', text: '+239', value: '+239' },
  { key: '+252 Somalia', text: '+252', value: '+252' },
  { key: '+47 Svalbard and Jan Mayen', text: '+47', value: '+47' },
  { key: '+963 Syrian Arab Republic', text: '+963', value: '+963' },
  { key: '+886 Taiwan, Province of China', text: '+886', value: '+886' },
  { key: '+255 Tanzania, United Republic of', text: '+255', value: '+255' },
  { key: '+670 Timor-Leste', text: '+670', value: '+670' },
  { key: '+58 Venezuela, Bolivarian Republic of', text: '+58', value: '+58' },
  { key: '+84 Viet Nam', text: '+84', value: '+84' },
  { key: '+1 284 Virgin Islands, British', text: '+1 284', value: '+1 284' },
  { key: '+1 340 Virgin Islands, U.S.', text: '+1 340', value: '+1 340' },
];

export default connect(s => ({ ...s.Bookings.newBooking }))(
  ({ firstname, surname, email, phonenumber, consent18, consent_weight, dispatch }) => {
    const handleChange = (e, d) => {
      const payload = {};
      payload[d.name] = d.checked === undefined ? d.value : d.checked;
      dispatch({ type: 'BASICINFO_CHANGE', payload });
    };
    return (
      <React.Fragment>
        <Header as="h3">Customer Info</Header>
        <Form>
          <Form.Field>
            <label htmlFor="first">FIRST NAME</label>
            <Input type="text" maxLength="35" onChange={handleChange} id="first" name="firstname" value={firstname} />
          </Form.Field>
          <Form.Field>
            <label htmlFor="surname">SURNAME</label>
            <Input type="text" maxLength="35" onChange={handleChange} id="surname" name="surname" value={surname} />
          </Form.Field>
          <label htmlFor="phonenumber">PHONE NUMBER</label>
          <Form.Field id="phonefield">
            <Dropdown
              placeholder="Code"
              id="phonecode"
              onChange={handleChange}
              name="phonecode"
              compact
              search
              selection
              options={phonecode}
            />
            <Input
              type="tel"
              maxLength="35"
              onChange={handleChange}
              id="phonenumber"
              name="phonenumber"
              value={phonenumber}
            />
          </Form.Field>
          <Form.Field>
            <label htmlFor="email">EMAIL</label>
            <Input type="email" maxLength="105" onChange={handleChange} id="email" name="email" value={email} />
          </Form.Field>
          <Form.Field>
            <Checkbox onChange={handleChange} checked={consent18} name="consent18" label="Confirm above 18" />
          </Form.Field>
          <Form.Field>
            <Checkbox onChange={handleChange} checked={consent_weight} name="consent_weight" label="230lbs or less" />
          </Form.Field>
        </Form>
      </React.Fragment>
    );
  },
);
