import React from 'react';
import { connect } from 'react-redux';
import { Dropdown, Form, Input, Grid, Checkbox } from 'semantic-ui-react';
import './PickUpInfo.css';
import { getSources } from '../../../../store/Bookings/Bookings.actions';

class PickUpInfo extends React.Component {
  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
  }

  // componentWillMount() {
  //   this.props.fetchPickUpSpots();
  // }

  onChange(e, d) {
    const payload = {};
    payload[d.name] = d.checked === undefined ? d.value : d.checked;
    this.props.updateBasicInfo(payload);
  }

  render() {
    const { selectedSpot, selectedPickUpTime, allPickUpSpots, slotSelected, rules, pickUpRequired } = this.props;
    let spotOptions, selected;
    let pickUpTimeOptions = [];
    try {
      spotOptions = (allPickUpSpots || []).map(spot => ({
        key: spot.id,
        value: spot.id,
        text: spot.name,
      }));

      selected = rules.find(
        x =>
          x.checkin_time.hours === slotSelected.checkinTime.hours &&
          x.checkin_time.minutes === slotSelected.checkinTime.minutes,
      );

      if (selected) {
        if (selected.checkin_time.hours < 5) {
          pickUpTimeOptions.push(
            {
              key: 21,
              value: '22:00',
              text: '22:00',
            },
            {
              key: 22,
              value: '23:00',
              text: '23:00',
            },
            {
              key: 23,
              value: '00:00',
              text: '00:00',
            },
          );
        }
        for (let i = 1; i < selected.checkin_time.hours; i++) {
          pickUpTimeOptions.push({
            key: i - 1,
            value: `${i}:00`,
            text: `${i}:00`,
          });
        }
      }

      if (!pickUpTimeOptions.length) {
        pickUpTimeOptions = [
          {
            key: 0,
            value: '12:30',
            text: '12:30PM',
          },
          {
            key: 1,
            value: '1:30',
            text: '1:30PM',
          },
          {
            key: 2,
            value: '2:30',
            text: '2:30PM',
          },
        ];
      }
    } catch (e) {
      console.log('ERROR:', e);
    }

    const defaultPickUpSpots = [
      {
        key: 0,
        value: 'MGM Grande',
        text: 'MGM Grande',
      },
      {
        key: 1,
        value: 'Summerlin South',
        text: 'Summerlin South',
      },
      {
        key: 2,
        value: 'Spring Valley',
        text: 'Spring Valley',
      },
      {
        key: 3,
        value: 'Donut Bar',
        text: 'Donut Bar',
      },
      {
        key: 4,
        value: 'Applebee Grill',
        text: 'Applebee Grill',
      },
    ];
    return (
      <React.Fragment>
        <Grid>
          <Grid.Row>
            <Grid.Column>
              <Form>
                <Form.Field>
                  <label htmlFor="channel">PICKUP INFO</label>
                  <Checkbox value={pickUpRequired} onChange={this.onChange} name="pickUpRequired" label="Required" />
                  {pickUpRequired && (
                    <Input type="text" placeholder="Search..." action>
                      <Dropdown
                        className="leftDropDown"
                        name="selectedSpot"
                        placeholder="Choose Spot"
                        fluid
                        selection
                        options={defaultPickUpSpots}
                        value={selectedSpot}
                        onChange={this.onChange}
                      />
                      <Dropdown
                        className="rightDropDown"
                        name="selectedPickUpTime"
                        placeholder="Time"
                        fluid
                        selection
                        options={pickUpTimeOptions}
                        value={selectedPickUpTime}
                        onChange={this.onChange}
                      />
                    </Input>
                  )}
                </Form.Field>
              </Form>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  selectedSpot: state.Bookings.newBooking.selectedSpot,
  selectedPickUpTime: state.Bookings.newBooking.selectedPickUpTime,
  allPickUpSpots: state.Bookings.allPickUpSpots,
  slotSelected: state.Bookings.slotSelected,
  rules: state.Bookings.rules,
  pickUpRequired: state.Bookings.newBooking.pickUpRequired,
});

const mapDispatchToProps = dispatch => ({
  fetchPickUpSpots: () => dispatch(getSources()),
  updateBasicInfo: payload => dispatch({ type: 'BASICINFO_CHANGE', payload }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PickUpInfo);
