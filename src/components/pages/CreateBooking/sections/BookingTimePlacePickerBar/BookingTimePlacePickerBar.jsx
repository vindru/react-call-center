import React from 'react';
import { Grid } from 'semantic-ui-react';
import PlacePicker from './PlacePicker';
import DatePicker from './DatePicker';
import SlotPicker from './SlotPicker';

export default () => (
  <Grid columns="equal">
    <Grid.Row className="date-picker-bar">
      <Grid.Column width={4}>
        <PlacePicker />
      </Grid.Column>
      <Grid.Column width={4}>
        <DatePicker />
      </Grid.Column>
      <Grid.Column width={8}>
        <SlotPicker />
      </Grid.Column>
    </Grid.Row>
  </Grid>
);
