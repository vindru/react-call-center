import React from 'react';
import { connect } from 'react-redux';
import DatePicker from 'react-date-picker';
import moment from 'moment';
// date picker
import SemanticDatepicker from 'react-semantic-ui-datepickers';
import 'react-semantic-ui-datepickers/dist/react-semantic-ui-datepickers.css';
import { Icon } from 'semantic-ui-react';
import './DatePicker.scss';

import { getAvailablityByDate } from '../../../../../store/Bookings/Bookings.actions';

const PlacePicker = ({ Bookings, dispatch }) => {
  const { date, place } = Bookings;
  const handleDateChange = d => {
    const datePicked = moment(d);

    if (!date.isSame(datePicked, 'day')) {
      dispatch({
        type: 'DATE_PICKED',
        payload: datePicked,
      });
      getAvailablityByDate(datePicked, place)
        .then(async rules => {
          await dispatch({ type: 'GET_AVAILABILITY', payload: rules });
          await dispatch({ type: 'SET_TANDEM_PRICES', payload: { bookingType: 'new' } });
        })
        .catch(e => {
          console.log('ERROR:', e);
        });
    }
  };
  const minDate = moment();

  // minDate.setDate(minDate.getDate() - 1);
  return (
    <React.Fragment>
      {/* <DatePicker
      calendarIcon={<Icon name="calendar outline" />}
      iconPosition="left"
      clearIcon={null}
      className="ui fluid selection dropdown DatePicker"
      format="d MMMM, yyyy"
      value={date}
      minDate={minDate}
      onChange={handleDateChange}
    /> */}
      <SemanticDatepicker
        selected={date.toDate()}
        minDate={minDate.toDate()}
        clearOnSameDateClick={false}
        clearable={false}
        onDateChange={handleDateChange}
      />
    </React.Fragment>
  );
};

export default connect(s => ({ Bookings: s.Bookings }))(PlacePicker);
