import React from 'react';
import { connect } from 'react-redux';
import { Dropdown, Header, Loader } from 'semantic-ui-react';
import './SlotPicker.scss';
import moment from 'moment';
const SlotPicker = ({ rules, rulesError, place, slotSelected, dispatch }) => {
  const onChange = (e, checkinTime) => {
    dispatch({
      type: 'SELECT_SLOT',
      payload: { checkinTime },
    });
  };

  const currentDate = moment();

  // cannot allow rules of past time
  let currentRules, selected, renderSelection;
  try {
    currentRules = rules.filter(rule => {
      if (rule.locationId === place) {
        if (currentDate.isBefore(rule.date, 'day')) {
          return true;
        }

        if (currentDate.isSame(rule.date, 'day')) {
          if (rule.checkin_time.hours > currentDate.hours()) {
            return true;
          }
          if (rule.checkin_time.hours === currentDate.hours() && rule.checkin_time.minutes >= currentDate.minutes()) {
            return true;
          }
          return false;
        }
        return false;
      }
      return false;
    });

    selected = currentRules.find(
      x =>
        x.checkin_time.hours === slotSelected.checkinTime.hours &&
        x.checkin_time.minutes === slotSelected.checkinTime.minutes,
    );

    renderSelection = () =>
      selected ? (
        <React.Fragment>
          <Header>
            {selected.checkin_time.hours < 10 ? `0${selected.checkin_time.hours}` : selected.checkin_time.hours}
            {':'}
            {selected.checkin_time.minutes < 10 ? `0${selected.checkin_time.minutes}` : selected.checkin_time.minutes}
            {' Hrs'}
          </Header>
          <Header className="price">{selected.invoice_price}</Header>
          <Header className="status">{`${selected.bookings.length}/${selected.number_of_spaces}`}</Header>
        </React.Fragment>
      ) : (
        'Select a slot'
      );
  } catch (e) {
    currentRules = [];
    console.log('ERROR:', e);
  }
  return (
    <React.Fragment>
      {currentRules.length === 0 && (
        <React.Fragment>
          <Dropdown fluid selection disabled text="No slots available" className="slotPicker" />
        </React.Fragment>
      )}

      {currentRules.length > 0 && (
        <React.Fragment>
          <Dropdown
            // placeholder="Select Location"
            fluid
            selection
            disabled={!currentRules.length}
            text={renderSelection()}
            onChange={onChange}
            className="slotPicker">
            <Dropdown.Menu>
              {currentRules.map(r => (
                <Dropdown.Item onClick={e => onChange(e, r.checkin_time)} active={r.selected} key={r._id} value={r._id}>
                  <Header>
                    {r.checkin_time.hours < 10 ? `0${r.checkin_time.hours}` : r.checkin_time.hours}
                    {':'}
                    {r.checkin_time.minutes < 10 ? `0${r.checkin_time.minutes}` : r.checkin_time.minutes}
                    {' Hrs'}
                  </Header>
                  <Header className="price">{r.invoice_price}</Header>
                  <Header className="status">{`${r.bookings.length}/${r.number_of_spaces}`}</Header>
                </Dropdown.Item>
              ))}
            </Dropdown.Menu>
          </Dropdown>
          {selected && <Header>{`${selected.number_of_spaces - selected.bookings.length} Spaces available`}</Header>}
          {rulesError && <Header>No slots available</Header>}
        </React.Fragment>
      )}
    </React.Fragment>
  );
};

const mapStateToProps = state => ({
  slotSelected: state.Bookings.slotSelected,
  place: state.Bookings.place,
  rulesError: state.Bookings.rulesError,
  rules: state.Bookings.rules,
});

export default connect(mapStateToProps)(SlotPicker);
