import React from 'react';
import { connect } from 'react-redux';
import { Dropdown } from 'semantic-ui-react';
import {
  getAvailablityByDate,
  getChannelsByLocation,
  getProducts,
} from '../../../../../store/Bookings/Bookings.actions';

const locationOptions = [
  {
    key: 'Oceanside',
    text: 'Oceanside',
    value: 0,
  },
  {
    key: 'Las Vegas',
    text: 'Las Vegas',
    value: 1,
  },
  {
    key: 'Santa Cruz',
    text: 'Santa Cruz',
    value: 2,
  },
];

const PlacePicker = ({ Bookings, dispatch }) => {
  const { place, date } = Bookings;
  const onChange = (e, { value }) => {
    dispatch({ type: 'PLACE_PICKED', payload: value });
    getAvailablityByDate(date, value)
      .then(async rules => {
        await dispatch({ type: 'GET_AVAILABILITY', payload: rules });
        await dispatch({ type: 'SET_TANDEM_PRICES', payload: { bookingType: 'new' } });
      })
      .catch(e => {
        console.log('ERROR:', e);
      });
    dispatch(getChannelsByLocation(value));
    dispatch(getProducts(value));
  };
  return (
    <Dropdown
      placeholder="Select Location"
      fluid
      selection
      options={locationOptions}
      value={place}
      onChange={onChange}
    />
  );
};

export default connect(s => ({ Bookings: s.Bookings }))(PlacePicker);
