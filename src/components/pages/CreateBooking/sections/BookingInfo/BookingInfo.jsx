import React from 'react';
import { connect } from 'react-redux';
import { Segment, Header, Grid, Divider } from 'semantic-ui-react';
import DebitItem from './DebitCreditItem';
import DepositItem from './DepositItem';
import Drop from './Drop';
import DropDelete from './DropDelete';

export default connect(s => ({ ...s.Bookings.newBooking }))(
  ({ products, subtotal, total, deposit, experienceTotal, depositWithFee, totalDiscount }) => {
    let debitItems, creditItems, depositItems;
    try {
      debitItems = products.filter(p => ['PRODUCT', 'OTHER_PRODUCT'].indexOf(p.type) > -1);
      creditItems = products.filter(p => ['PROMOTION'].indexOf(p.type) > -1);
      depositItems = products.filter(p => ['DEPOSIT'].indexOf(p.type) > -1);
    } catch (e) {
      console.log('ERROR:', e);
    }
    return (
      <React.Fragment>
        <Header as="h3">Booking</Header>
        {debitItems.map(di => (
          <DebitItem debit counter key={di.id} {...di}>
            {' '}
          </DebitItem>
        ))}
        <Drop accept="PRODUCT" />
        <Drop accept="OTHER_PRODUCT" />
        <Segment basic>
          <Grid verticalAlign="middle" columns="equal">
            <Grid.Row>
              <Grid.Column width={12}>Subtotal</Grid.Column>
              <Grid.Column>
                <Header size="small" textAlign="right">{`$${subtotal}`}</Header>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
        {depositItems.map(di => (
          <DepositItem auto key={di.id} {...di}>
            {' '}
          </DepositItem>
        ))}
        <Divider />
        {creditItems.map(di => (
          <DebitItem credit key={di.id} {...di}>
            {' '}
          </DebitItem>
        ))}
        <Drop accept="PROMOTION" green />
        <Segment basic>
          <Grid verticalAlign="middle" columns="equal">
            <Grid.Row>
              <Grid.Column width={12}>Experience Total</Grid.Column>
              <Grid.Column>
                <Header size="small" textAlign="right">{`$${experienceTotal}`}</Header>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
        <Segment basic>
          <Grid verticalAlign="middle" columns="equal">
            <Grid.Row>
              <Grid.Column width={12}>Total Discount</Grid.Column>
              <Grid.Column>
                <Header size="small" textAlign="right">{`$${totalDiscount}`}</Header>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
        <Segment basic>
          <Grid verticalAlign="middle" columns="equal">
            <Grid.Row>
              <Grid.Column width={12}>Total After Deposit</Grid.Column>
              <Grid.Column>
                <Header size="small" textAlign="right">{`$${total}`}</Header>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
        <Segment basic>
          <Grid verticalAlign="middle" columns="equal">
            <Grid.Row>
              <Grid.Column width={12}>Deposit</Grid.Column>
              <Grid.Column>
                <Header size="small" textAlign="right">{`$${deposit}`}</Header>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
        <Divider />
        <Segment basic>
          <Grid verticalAlign="middle" columns="equal">
            <Grid.Row>
              <Grid.Column width={12}>Deposit with Fees</Grid.Column>
              <Grid.Column>
                <Header size="small" textAlign="right">{`$${depositWithFee}`}</Header>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
        <DropDelete />
      </React.Fragment>
    );
  },
);
