import React, { useState } from 'react';
import { Segment, Header, Grid, Input, Button, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import classNames from 'classnames';

import './DebitCreditItem.scss';

export default connect(s => ({ products: s.Bookings.newBooking.products }))(
  ({ name, credit, debit, auto, id, dispatch, products, type }) => {
    const handleChange = (e, maxPrice) => {
      let { value } = e.target;
      value = value || 0;

      const re = /^[0-9\b]+$/;
      if (re.test(value) && parseInt(value, 10) <= maxPrice) {
        dispatch({
          type: 'DEPOSIT_AMT_CHANGE',
          payload: {
            id,
            value,
            type,
          },
        });
      }
    };

    const [edit, toggleEditable] = useState(false);
    let deposit, product;
    try {
      deposit = products.find(p => p.type === 'DEPOSIT' && p.id === id);
      product = products.find(p => p.id === id && p.type !== 'DEPOSIT');
    } catch (e) {
      console.log('ERROR:', e);
    }
    const { qty, price: depositPerItem } = deposit;
    const totalDeposit = depositPerItem * qty;
    const pricePerItem = product.price;
    return (
      <Segment color={classNames('left', 'productSegment', { orange: debit, green: credit, red: auto })}>
        <Grid verticalAlign="middle">
          <Grid.Row columns={3}>
            <Grid.Column width={11} stretched className="productName">
              <Header as="h6">{name}</Header>
              <div className="subInfo">
                <p>{`$${depositPerItem}`}</p>
                {!edit && (
                  <a as="button" className="pointer-cursor" onClick={() => toggleEditable(!edit)}>
                    Edit
                  </a>
                )}
              </div>
            </Grid.Column>
            <Grid.Column width={1} className="productPrice">
              <Header size="small" textAlign="right">{`x${qty}`}</Header>
            </Grid.Column>
            <Grid.Column width={3} className="productPriceInput">
              {!edit && <Header size="small" textAlign="right">{`$${totalDeposit}`}</Header>}
              {edit && (
                <Input textAlign="right" value={depositPerItem} onChange={e => handleChange(e, pricePerItem)} action>
                  <input />
                  <Button onClick={() => toggleEditable(!edit)} icon>
                    <Icon name="check" />
                  </Button>
                </Input>
              )}
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
    );
  },
);
