import React, { Component } from 'react';
import axios from 'axios';
import { Icon, Divider, Button, Grid, Segment, Form, Header, Modal, List, Transition } from 'semantic-ui-react';

export default class Notes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      NotesDisplayType: this.props.type,
      BookingId: this.props.id,
      noteInTheMaking: '',
      notes: [],
      Mesvisible: false,
      date: this.props.date,
      parentNotes: this.props.notes,
    };
  }

  componentDidMount() {
    if (typeof this.state.parentNotes !== 'undefined') {
      const newnotes = this.state.parentNotes.filter(item => {
        if (item.booking_id === this.state.BookingId) {
          return item;
        }
      });
      this.setState({ notes: newnotes.reverse() });
      if (this.props.onChange) {
        this.props.onChange(newnotes.length);
      }
    }
    if (this.state.NotesDisplayType === 'panel') {
      this.getNotes();
    }
  }

  getNotes = () => {
    axios({
      method: 'get',
      url: `${process.env.REACT_APP_BOOKING_SERVER_URL}/api/v1/notes/booking_id/${this.state.BookingId}`, // ---|Broadcast api link
      config: { headers: { 'Content-Type': 'multipart/form-data' } },
    })
      .then(data => {
        this.setState({ notes: data.data.reverse() });
        // this would pass it to parent component for display of number of notes
        if (this.props.onChange) {
          this.props.onChange(data.data.length);
        }
      })
      .catch(err => {
        console.log('catch 2 error', err.message);
        this.setState({ BroadcastError: 'Error in BrodCasting', BroadcastModalSend: true });
      });
  };

  handleFormSubmit = () => {
    const notes = this.state.notes;

    if (this.state.noteInTheMaking !== '') {
      //  let newDate = new Date()
      //  let date = newDate.getDate();
      //  let month = newDate.getMonth() + 1;
      //  let year = newDate.getFullYear();
      //  let fDate=`${year}${'-'}${month<10?`0${month}`:`${month}`}${'-'}${date}`
      let date = this.state.date;
      if (typeof this.state.date === 'object') {
        date = this.state.date.toISOString().split('T')[0];
      }
      const Payload = {
        booking_id: this.state.BookingId,
        executive_id: 'user',
        note: this.state.noteInTheMaking,
        date,
      };
      console.log(Payload, typeof Payload.date);
      if (this.props.id !== 'newBooking') {
        axios({
          method: 'post',
          url: `${process.env.REACT_APP_BOOKING_SERVER_URL}/api/v1/note`, // ---|Broadcast api link
          config: { headers: { 'Content-Type': 'multipart/form-data' } },
          data: Payload,
        })
          .then(data => {
            this.getNotes();
            // this.props.refreshnotes(this.state.date)
            console.log('note added');
            this.setState({
              noteInTheMaking: '',
            });
          })
          .catch(err => {
            console.log('catch 2 error', err.message);
            this.setState({ BroadcastError: 'Error in adding note' });
          });
        // api call here  to store in database
      } else {
        console.log('notes', this.state.notes, Payload, '+++++');
        this.props.MakeNotes(Payload);
        notes.push(Payload);
        this.setState({ notes: notes.reverse() });
        console.log('notes', this.state.notes);
      }
    }
  };

  handleInputChange = e => {
    this.setState({
      noteInTheMaking: e.target.value,
    });
  };

  handleInputClick = () => {};

  gettime = time => {
    const hrs = parseInt(time.slice(time.indexOf(':') - 2, time.indexOf(':'))) * 60;
    const min = parseInt(time.slice(time.indexOf(':') + 1, time.indexOf(':') + 3));
    const offset = new Date().getTimezoneOffset();
    const newTime = hrs + min - offset;
    const nhrs = Math.floor(newTime / 60);
    const nmin = newTime % 60;
    return (
      <span>
        {nhrs < 10 ? `0${nhrs}:` : `${nhrs}:`}
        {nmin < 10 ? `0${nmin}` : nmin}
      </span>
    );
  };

  render() {
    return (
      <div>
        {this.state.NotesDisplayType === 'panel' ? (
          <div>
            <Form onSubmit={this.handleFormSubmit}>
              <Header as="h3">Notes</Header>
              <Form.Group widths="equal">
                <Form.Input
                  fluid
                  placeholder="Send a note"
                  action={<Button button="true" basic floating="true" content="Send" />}
                  value={this.state.noteInTheMaking}
                  onChange={this.handleInputChange}
                />
              </Form.Group>
            </Form>
            {this.state.notes.map((item, i) => (
              <p>
                {console.log(item)}
                <Icon name="envelope outline" style={{ color: '#9EA0A5' }} />
                {item.note}
                <br />
                {this.props.id !== 'newBooking' ? (
                  <span>
                    <small style={{ color: '#9EA0A5' }}>{item.createdAt.slice(0, item.createdAt.indexOf('T'))}</small>
                    <small style={{ color: '#9EA0A5' }} className="float-right">
                      {this.gettime(item.createdAt)}
                    </small>
                  </span>
                ) : null}
              </p>
            ))}
          </div>
        ) : (
          <div>
            <Form onSubmit={this.handleFormSubmit}>
              <Form.Input
                placeholder="Write a Note"
                value={this.state.noteInTheMaking}
                onFocus={() => {
                  this.setState({ Mesvisible: true });
                }}
                onBlur={() => {
                  this.setState({ Mesvisible: false });
                }}
                onChange={this.handleInputChange}
                onClick={() => this.handleInputClick()}
              />
            </Form>
            <Transition visible={this.state.Mesvisible} animation="scale" duration={500}>
              <div>press Enter to store</div>
            </Transition>
            <Transition.Group as={List} duration={200} divided size="small" verticalAlign="middle">
              {this.state.notes.map((item, i) => (
                <List.Item key={i}>
                  <List.Header>{item.note}</List.Header>
                  <List.Description>
                    &nbsp;&nbsp;- {item.executive_id}&nbsp;,&nbsp; {this.gettime(item.createdAt)}
                  </List.Description>
                </List.Item>
              ))}
            </Transition.Group>
          </div>
        )}
      </div>
    );
  }
}
