import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Header } from 'semantic-ui-react';

import DatePicker from './DatePicker';
import SlotPicker from './SlotPicker';
import './BookingTimePlacePickerBar.scss';

function printTimeslot(timeslot) {
  try {
    if (timeslot.minutes < 10) {
      return timeslot.hours < 10 ? `0${timeslot.hours}:0${timeslot.minutes}` : `${timeslot.hours}:0${timeslot.minutes}`;
    }
    return timeslot.hours < 10 ? `0${timeslot.hours}:${timeslot.minutes}` : `${timeslot.hours}:${timeslot.minutes}`;
  } catch (e) {
    console.log('ERROR:', e);
    return '';
  }
}

function printDateTime(date) {
  const d = new Date(date);
  return d.toLocaleDateString('en-us', { year: 'numeric', month: 'long', day: '2-digit' });
}
function printLocation(id) {
  switch (id) {
    case 1:
      return 'Las Vegas';
    case 2:
      return 'Ocean Side';
    case 3:
      return 'Santa Cruz';
    default:
      return '';
  }
}

class BookingTimePlacePickerBar extends Component {
  render() {
    const { Bookings } = this.props;

    const { updateBooking } = { ...Bookings };
    const { enableMoving } = { ...updateBooking };

    return (
      <Grid columns="equal">
        <Grid.Row className="date-picker-bar">
          <Grid.Column width={3}>
            <Header size="medium">{printLocation(updateBooking.location_id)}</Header>
          </Grid.Column>
          <Grid.Column width={3}>
            {enableMoving ? <DatePicker /> : <Header size="medium">{printDateTime(updateBooking.jumping_date)}</Header>}
          </Grid.Column>
          <Grid.Column width={10}>
            {enableMoving ? (
              <SlotPicker />
            ) : (
              <Header size="medium">
                {printTimeslot(updateBooking.checkin_time)}
                {' Hrs'}
              </Header>
            )}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

const mapStateToProps = state => ({
  Bookings: state.Bookings,
});

export default connect(mapStateToProps)(BookingTimePlacePickerBar);
