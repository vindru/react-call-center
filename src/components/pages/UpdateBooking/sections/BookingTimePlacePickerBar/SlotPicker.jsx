import React from 'react';
import { connect } from 'react-redux';
import { Dropdown, Header, Loader } from 'semantic-ui-react';
import moment from 'moment';
import './SlotPicker.scss';

const SlotPicker = ({ Bookings, dispatch }) => {
  const { rules, rulesError, updateBooking } = Bookings;
  const { jumping_date, location_id, checkin_time } = Bookings.updateBooking;

  let currentDate = moment();
  if (currentDate.isBefore(jumping_date, 'day')) {
    currentDate = jumping_date;
    currentDate.hours(0);
    currentDate.minutes(0);
    currentDate.seconds(0);
  }

  // cannot allow rules of past time
  let onChange, currentRules, selected, renderSelection;
  try {
    currentRules = rules.filter(rule => {
      if (rule.locationId === location_id) {
        if (currentDate.isBefore(jumping_date, 'day')) {
          return true;
        }

        if (currentDate.isSame(rule.date, 'day')) {
          if (rule.checkin_time.hours > currentDate.hours()) {
            return true;
          }
          if (rule.checkin_time.hours === currentDate.hours() && rule.checkin_time.minutes >= currentDate.minutes()) {
            return true;
          }
          return false;
        }
        return false;
      }
      return false;
    });
  } catch (e) {
    console.log('UPDATESCREEN SLOT PICKER ERR:', e);
  }
  try {
    selected = currentRules.find(
      x => x.checkin_time.hours === checkin_time.hours && x.checkin_time.minutes === checkin_time.minutes,
    );
  } catch (e) {
    console.log('UPDATESCREEN SLOT PICKER ERR:', e);
  }
  try {
    onChange = (e, checkinTime, selectedSlotPrice, selectedSlotDepositPrice) => {
      dispatch({
        type: 'SELECT_SLOT_UPDATE_SCREEN',
        payload: { checkinTime },
      });
      if (selectedSlotPrice && selectedSlotDepositPrice) {
        const oldSlot = updateBooking.products.find(p => p.name === 'Tandem' && p.type === 'PRODUCT');

        if (selectedSlotPrice !== oldSlot.price || selectedSlotDepositPrice !== oldSlot.deposit) {
          if (selectedSlotPrice > oldSlot.price) {
            updateBooking.products.forEach(p => {
              if (p.name === 'Tandem' && p.type === 'DEPOSIT') {
                p.price = selectedSlotDepositPrice;
              }
              if (p.name === 'Tandem' && p.type === 'PRODUCT') {
                p.price = selectedSlotPrice;
                p.deposit = selectedSlotDepositPrice;
              }
            });
          } else {
            const oldCredit = updateBooking.products.find(p => p.name === 'Tandem' && p.type === 'CREDIT');
            updateBooking.products.forEach(p => {
              if (p.name === 'Tandem' && p.type === 'DEPOSIT') {
                p.price = selectedSlotDepositPrice;
              }
              if (p.name === 'Tandem' && p.type === 'DEBIT') {
                p.price = selectedSlotPrice - oldCredit.price;
              }
              if (p.name === 'Tandem' && p.type === 'PRODUCT') {
                p.price = selectedSlotPrice;
                p.deposit = selectedSlotDepositPrice;
              }
            });
          }
          dispatch({
            type: 'RECALCULATE_PRICES_UPDATE_SCREEN',
            payload: updateBooking,
          });
        }
      }
    };
  } catch (e) {
    console.log('UPDATESCREEN SLOT PICKER ERR:', e);
  }
  try {
    renderSelection = () =>
      selected ? (
        <React.Fragment>
          <Header>
            {selected.checkin_time.hours < 10 ? `0${selected.checkin_time.hours}` : selected.checkin_time.hours}
            {':'}
            {selected.checkin_time.minutes < 10 ? `0${selected.checkin_time.minutes}` : selected.checkin_time.minutes}
            {' Hrs'}
          </Header>
          <Header className="price">{selected.invoice_price}</Header>
          <Header className="status">{`${selected.bookings.length}/${selected.number_of_spaces}`}</Header>
        </React.Fragment>
      ) : (
        'Select a slot'
      );
  } catch (e) {
    console.log('UPDATESCREEN SLOT PICKER ERR:', e);
  }
  return (
    <React.Fragment>
      {currentRules.length === 0 && (
        <React.Fragment>
          <Dropdown
            // placeholder="Select Location"
            fluid
            selection
            disabled
            text="Select a slot"
            className="slotPicker"
          />
          <Loader inline className="slotPickerLoader" />
        </React.Fragment>
      )}
      {currentRules.length > 0 && (
        <React.Fragment>
          <Dropdown
            // placeholder="Select Location"
            fluid
            selection
            disabled={!currentRules.length}
            text={renderSelection()}
            onChange={onChange}
            className="slotPicker">
            <Dropdown.Menu>
              {currentRules.map(r => (
                <Dropdown.Item
                  onClick={e => onChange(e, r.checkin_time, r.invoice_price, r.deposit_price)}
                  active={r.selected}
                  key={r._id}
                  value={r._id}>
                  <Header>
                    {r.checkin_time.hours < 10 ? `0${r.checkin_time.hours}` : r.checkin_time.hours}
                    {':'}
                    {r.checkin_time.minutes < 10 ? `0${r.checkin_time.minutes}` : r.checkin_time.minutes}
                    {' Hrs'}
                  </Header>
                  <Header className="price">{r.invoice_price}</Header>
                  <Header className="status">{`${r.bookings.length}/${r.number_of_spaces}`}</Header>
                </Dropdown.Item>
              ))}
            </Dropdown.Menu>
          </Dropdown>
          {selected && <Header>{`${selected.number_of_spaces - selected.bookings.length} Spaces available`}</Header>}
          {rulesError && <Header>No slots available</Header>}
        </React.Fragment>
      )}
    </React.Fragment>
  );
};

export default connect(s => ({ Bookings: s.Bookings }))(SlotPicker);
