import React from 'react';
import { connect } from 'react-redux';
import DatePicker from 'react-date-picker';
import moment from 'moment';
// date picker
import SemanticDatepicker from 'react-semantic-ui-datepickers';
import 'react-semantic-ui-datepickers/dist/react-semantic-ui-datepickers.css';

import { Icon } from 'semantic-ui-react';
import './DatePicker.scss';

import { getAvailablityByDate } from '../../../../../store/Bookings/Bookings.actions';

const PlacePicker = ({ Bookings, dispatch }) => {
  const { jumping_date, location_id } = Bookings.updateBooking;

  const handleDateChange = d => {
    try {
      const datePicked = moment(d);
      if (!jumping_date.isSame(datePicked, 'day')) {
        dispatch({
          type: 'DATE_PICKED_UPDATE_SCREEN',
          payload: datePicked,
        });
        getAvailablityByDate(datePicked, location_id).then(rules => {
          if (rules) {
            dispatch({ type: 'GET_AVAILABILITY', payload: rules });
            dispatch({ type: 'SET_TANDEM_PRICES', payload: { bookingType: 'update' } });
          }
        });
      }
    } catch (e) {
      console.log('ERROR:', e);
    }
  };

  const minDate = moment();

  return (
    <React.Fragment>
      {/* <DatePicker
        calendarIcon={<Icon name="calendar outline" />}
        iconPosition="left"
        clearIcon={null}
        className="ui fluid selection dropdown DatePicker"
        format="d MMMM, yyyy"
        value={date}
        minDate={minDate}
        onChange={handleDateChange}
      /> */}
      <SemanticDatepicker
        selected={jumping_date.toDate()}
        minDate={minDate.toDate()}
        clearable={false}
        clearOnSameDateClick={false}
        onDateChange={handleDateChange}
      />
    </React.Fragment>
  );
};

export default connect(s => ({ Bookings: s.Bookings }))(PlacePicker);
