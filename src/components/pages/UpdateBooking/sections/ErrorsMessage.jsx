import React from 'react';
import { connect } from 'react-redux';
import { Message } from 'semantic-ui-react';
import { withRouter } from 'react-router';

const ErrorMessages = ({ Bookings }) => {
  const { errors } = { ...Bookings.updateBooking };
  if (errors.length > 0) {
    const errorsList = errors.map(error => <Message.Item>{error.message}</Message.Item>);
    return (
      <Message negative>
        <Message.Header>Errors</Message.Header>
        <Message.List>{errorsList}</Message.List>
      </Message>
    );
  }
  return '';
};

export default withRouter(connect(s => ({ Bookings: s.Bookings }))(ErrorMessages));
