import React from 'react';
import { connect } from 'react-redux';
import { Dropdown, Form, Input, Grid, Checkbox } from 'semantic-ui-react';
import './PickUpInfo.css';
import { getSources } from '../../../../store/Bookings/Bookings.actions';

class PickUpInfo extends React.Component {
  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
  }

  // componentWillMount() {
  //   this.props.fetchPickUpSpots();
  // }

  onChange(e, d) {
    const payload = {};
    payload[d.name] = d.checked === undefined ? d.value : d.checked;
    this.props.updateBasicInfo(payload);
  }

  render() {
    const {
      selectedSpot,
      selectedPickUpTime,
      allPickUpSpots,
      slotSelected,
      rules,

      pickUpRequired,
    } = this.props;

    const spotOptions = (allPickUpSpots || []).map(spot => ({
      key: spot.id,
      value: spot.name,
      text: spot.name,
    }));
    let selected;
    try {
      selected = rules.find(
        x =>
          x.checkin_time.hours === slotSelected.checkinTime.hours &&
          x.checkin_time.minutes === slotSelected.checkinTime.minutes,
      );
    } catch (e) {
      console.log('ERROR:', e);
    }

    console.log(selected);

    const defaultPickUpSpots = [
      {
        key: 0,
        value: 'MGM Grande',
        text: 'MGM Grande',
      },
      {
        key: 1,
        value: 'Summerlin South',
        text: 'Summerlin South',
      },
      {
        key: 2,
        value: 'Spring Valley',
        text: 'Spring Valley',
      },
      {
        key: 3,
        value: 'Donut Bar',
        text: 'Donut Bar',
      },
      {
        key: 4,
        value: 'Applebee Grill',
        text: 'Applebee Grill',
      },
    ];

    let pickUpTimeOptions = [];

    if (selected && selected.checkin_time) {
      if (selected.checkin_time.hours < 5) {
        pickUpTimeOptions.push(
          {
            key: 21,
            value: 22,
            text: '22:00',
          },
          {
            key: 22,
            value: 23,
            text: '23:00',
          },
          {
            key: 23,
            value: 24,
            text: '00:00',
          },
        );
      }
      for (let i = 1; i < selected.checkin_time.hours; i++) {
        pickUpTimeOptions.push({
          key: i - 1,
          value: `${i}:00`,
          text: `${i}:00`,
        });
      }
    }

    if (!pickUpTimeOptions.length) {
      pickUpTimeOptions = [
        {
          key: 0,
          value: '12:30PM',
          text: '12:30PM',
        },
        {
          key: 1,
          value: '1:30PM',
          text: '1:30PM',
        },
        {
          key: 2,
          value: '2:30PM',
          text: '2:30PM',
        },
      ];
    }

    return (
      <React.Fragment>
        <Grid>
          <Grid.Row>
            <Grid.Column>
              <Form>
                <Form.Field>
                  <label htmlFor="channel">PICKUP INFO</label>
                  <Input type="text" placeholder="Search..." action>
                    <Checkbox
                      checked={pickUpRequired}
                      value={pickUpRequired}
                      onChange={this.onChange}
                      name="pickUpRequired"
                      label={pickUpRequired ? '' : 'Required'}
                    />
                    {pickUpRequired && (
                      <React.Fragment>
                        <Dropdown
                          className="leftDropDown"
                          name="selectedSpot"
                          placeholder="Choose Spot"
                          fluid
                          selection
                          options={defaultPickUpSpots}
                          value={selectedSpot}
                          onChange={this.onChange}
                        />
                        <Dropdown
                          className="rightDropDown"
                          name="selectedPickUpTime"
                          placeholder="Time"
                          fluid
                          selection
                          options={pickUpTimeOptions}
                          value={selectedPickUpTime}
                          onChange={this.onChange}
                        />
                      </React.Fragment>
                    )}
                  </Input>
                </Form.Field>
              </Form>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  selectedSpot: state.Bookings.updateBooking.selectedSpot,
  selectedPickUpTime: state.Bookings.updateBooking.selectedPickUpTime,
  allPickUpSpots: state.Bookings.allPickUpSpots,
  slotSelected: state.Bookings.slotSelected,
  rules: state.Bookings.rules,
  pickUpRequired: state.Bookings.updateBooking.pickUpRequired,
});

const mapDispatchToProps = dispatch => ({
  fetchPickUpSpots: () => dispatch(getSources()),
  updateBasicInfo: payload => dispatch({ type: 'BASICINFO_CHANGE_UPDATE_SCREEN', payload }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PickUpInfo);
