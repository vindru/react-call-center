import React from 'react';
import { connect } from 'react-redux';
import { Dropdown, Form } from 'semantic-ui-react';
import { getChannelsByLocation } from '../../../../store/Bookings/Bookings.actions';

class ChannelInfo extends React.Component {
  // constructor(props) {
  //   super(props);

  // this.onChange = this.onChange.bind(this);
  // this.getChannels = this.getChannels.bind(this);
  // }

  // componentWillMount() {
  //   this.getChannels();
  // }

  // onChange(e, d) {
  //   this.props.updateChannelSelected(d.value);
  // }

  // getChannels() {
  //   const { selectedLocation } = this.props;
  //   this.props.getChannelsByLocation(selectedLocation);
  // }

  render() {
    const { selectedChannel } = this.props;

    return (
      <React.Fragment>
        <Form>
          <Form.Field>
            <label htmlFor="channel">CHANNEL</label>
            {/* <Dropdown */}
            {/*  name="selectedChannel" */}
            {/*  placeholder="Select Channel" */}
            {/*  fluid */}
            {/*  selection */}
            {/*  options={channelOptions} */}
            {/*  value={selectedChannel} */}
            {/*  onChange={this.onChange} */}
            {/* /> */}
            <p>{selectedChannel || 'Call Centre'}</p>
          </Form.Field>
        </Form>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  selectedChannel: state.Bookings.updateBooking.selectedChannel,
  selectedLocation: state.Bookings.place,
});

const mapDispatchToProps = dispatch => ({
  getChannelsByLocation: locationId => dispatch(getChannelsByLocation(locationId)),
  updateChannelSelected: value => dispatch({ type: 'CHANNEL_SELECTED', payload: value }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ChannelInfo);
