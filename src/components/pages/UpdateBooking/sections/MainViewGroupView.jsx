import React from 'react';
import { connect } from 'react-redux';
import { Button } from 'semantic-ui-react';
import { withRouter } from 'react-router';
import { createBookingWithoutPayment } from '../../../../store/Bookings/Bookings.actions';

const MainViewGroupView = ({ Bookings, dispatch, history }) => {
  let isSavingForLater = false;
  const onClick = () => {
    const errors = [];
    const { newBooking } = { ...Bookings };
    if (newBooking.firstname === '') {
      errors.push({ id: 'firstName', message: 'First Name cannot be empty.' });
    }
    if (newBooking.surname === '') {
      errors.push({ id: 'surname', message: 'Surname cannot be empty.' });
    }
    if (newBooking.email === '') {
      errors.push({ id: 'email', message: 'Email cannot be empty.' });
    }
    if (newBooking.phonenumber === '') {
      errors.push({ id: 'phoneNumber', message: 'Phone number cannot be empty.' });
    }
    // Find Selected Slot
    const selectedSlot = Bookings.rules.find(
      x =>
        x.checkin_time.hours === Bookings.slotSelected.checkinTime.hours &&
        x.checkin_time.minutes === Bookings.slotSelected.checkinTime.minutes,
    );
    if (!selectedSlot) {
      errors.push({ id: 'slot', message: 'Slot is not chosen.' });
      // selectedSlot = { checkin_time: { hours: '9', minutes: '30' } };
    }

    const payload = {};
    payload.errors = errors;
    dispatch({ type: 'BASICINFO_CHANGE', payload });

    if (errors.length === 0) {
      isSavingForLater = true;
      // Calculate addOns
      let addOnProducts = newBooking.products.filter(
        p => ['OTHER_PRODUCT', 'PRODUCT'].indexOf(p.type) > -1 && p.name !== 'Tandem',
      );
      addOnProducts = addOnProducts.map(product => ({
        code: product.id,
        label: product.name,
        amount: product.price,
        quantity: product.qty,
        forUser: ['1'],
      }));
      // Calculate Total Tandem
      const tandemProduct = newBooking.products.find(x => x.name === 'Tandem' && x.type === 'PRODUCT');
      const tandemDeposit = newBooking.products.find(x => x.name === 'Tandem' && x.type === 'DEPOSIT');

      const totalDeposit = tandemProduct ? tandemDeposit.price : 0;
      const totalQty = tandemProduct ? tandemProduct.qty : 0;

      const depositPerTandem = totalQty ? totalDeposit / totalQty : 0;
      const pricePerTandem = tandemProduct ? tandemProduct.price : 0;

      const requestBody = {
        location_id: Bookings.place,
        name: newBooking.firstname,
        surname: newBooking.surname,
        email: newBooking.email,
        phone: newBooking.phonenumber,
        number_of_spaces: totalQty,
        space_price: pricePerTandem, // optional override
        space_deposit_price: depositPerTandem, // optional override
        addons: addOnProducts,
        checkin_time: { hours: selectedSlot.checkin_time.hours, minutes: selectedSlot.checkin_time.minutes },
        date: Bookings.date,
      };
      // Save Booking
      console.log('Request Body - Save for later', requestBody);
      createBookingWithoutPayment(requestBody)
        .then(async resp => {
          isSavingForLater = false;
          console.log('Response Saving Booking', resp);
          if (resp) {
            // Show alert and redirect to Home.
            await dispatch({ type: 'RESET_BOOKINGINFO' });
            await alert('Booking Saved.');
            history.push('/');
          } else {
            // Show alert and redirect to Home.
            alert('Error while saving booking');
          }
        })
        .catch(err => {
          console.log('Error in Create booking', err);
        });
    }
  };
  return (
    <Button.Group>
      <Button primary>Main View</Button>
      <Button basic color="grey">
        Group View
      </Button>
    </Button.Group>
  );
};

export default withRouter(connect(s => ({ Bookings: s.Bookings }))(MainViewGroupView));
