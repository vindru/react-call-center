import React from 'react';
import { connect } from 'react-redux';
import { Tab, Grid, Header } from 'semantic-ui-react';
import ProductItem from './ProductItem';

const RenderList = connect(s => ({ Products: s.Products, Bookings: s.Bookings }))(({ Products, type, Bookings }) => {
  try {
    const bookedProductsIds = Bookings.updateBooking.products.map(bp => {
      if (['PRODUCT', 'OTHER_PRODUCT', 'PROMOTION'].indexOf(bp.type) !== -1) {
        return bp.id;
      }
    });
    const productsAllowed = [];
    const data = Products.filter(p => {
      if (p.type === type && bookedProductsIds.indexOf(p.id) === -1) {
        if (productsAllowed && productsAllowed.length > 0 && productsAllowed.indexOf(p.id) === -1) {
          return false;
        }
        return true;
      }
      return false;
    });

    return (
      <Grid columns="two">
        <Grid.Row>
          {data.map(p => (
            <Grid.Column key={p.id}>
              <ProductItem {...p} />
            </Grid.Column>
          ))}
          {data.length === 0 && (
            <Grid.Column width={16}>
              <Header as="h2" textAlign="center" disabled>
                No Items
              </Header>
            </Grid.Column>
          )}
        </Grid.Row>
      </Grid>
    );
  } catch (e) {
    return (
      <Grid columns="two">
        <Grid.Row>
          <Grid.Column width={16}>
            <Header as="h2" textAlign="center" disabled>
              No Items
            </Header>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
});

const panes = [
  {
    menuItem: 'Products',
    render: () => (
      <Tab.Pane attached={false}>
        <RenderList type="PRODUCT" />
      </Tab.Pane>
    ),
  },
  {
    menuItem: 'Other Charges',
    render: () => (
      <Tab.Pane attached={false}>
        <RenderList type="OTHER_PRODUCT" />
      </Tab.Pane>
    ),
  },
  {
    menuItem: 'Promotions',
    render: () => (
      <Tab.Pane attached={false}>
        <RenderList type="PROMOTION" />
      </Tab.Pane>
    ),
  },
];

const ProductsAndPromotions = () => (
  <Tab id="ProductsAndPromitions" menu={{ secondary: true, pointing: true }} panes={panes} />
);

export default ProductsAndPromotions;
