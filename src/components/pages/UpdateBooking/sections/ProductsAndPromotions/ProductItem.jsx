import React from 'react';
import { useDrag } from 'react-dnd';
import { Header, Segment, Grid } from 'semantic-ui-react';
import './ProductsAndPromotions.scss';

const style = {
  cursor: 'move',
};
export default ({ id, name, price, type, ...rest }) => {
  const [options, drag] = useDrag({
    item: {
      id,
      name,
      price,
      type,
      ...rest,
    },
    end: dropResult => {
      if (dropResult) {
        // console.log(dropResult);
        // alert(`You dropped ${name} into ${dropResult.name}!`);
      }
    },
    collect: monitor => ({
      isDragging: monitor.isDragging(),
    }),
  });
  return (
    <div ref={drag} style={{ ...style }}>
      <Segment className="productSegment">
        <Grid verticalAlign="middle">
          <Grid.Row>
            <Grid.Column stretched className="productName">
              <Header as="h5">{name}</Header>
            </Grid.Column>
            <Grid.Column className="productPrice">
              <Header textAlign="right" size="small">
                {`$${price}`}
              </Header>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
    </div>
  );
};
