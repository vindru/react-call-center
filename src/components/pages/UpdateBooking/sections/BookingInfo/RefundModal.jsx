import { Button, Form, Modal, Message, Radio } from 'semantic-ui-react';
import React, { useState } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { generateRefund } from '../../../../../store/Bookings/Bookings.actions';

const allowedStatus = {
  STARTED: 'STARTED',
  NOT_STARTED: 'NOT_STARTED',
  SUCCESS: 'SUCCESS',
  FAILED: 'FAILED',
};

export default withRouter(
  connect(state => ({ ...state.Bookings.updateBooking }))(
    ({ id, products, invoice_id, close, getData, booking_id }) => {
      let product;
      try {
        product = products.find(p => p.id === id && p.type === 'CREDIT');
      } catch (e) {
        console.log('ERROR:', e);
      }
      const [refundQty, setRefundQty] = useState(1);
      const [status, setStatus] = useState(allowedStatus.NOT_STARTED);

      const processRefund = () => {
        setStatus(allowedStatus.STARTED);
        const creditLines = [];
        try {
          for (let i = 0; i < refundQty; i++) {
            creditLines.push({ slot_id: product.slotId, line_id: product.lineItemsArray[i] });
          }
        } catch (e) {
          console.log('ERROR:', e);
        }
        const requestBody = {
          invoice_id,
          credit_line: creditLines,
          credit_lines: creditLines,
        };
        generateRefund(requestBody)
          .then(resp => {
            console.log(resp.data);
            if (resp.data.successRefunds.length > 0) {
              console.log('After generateRefund');
              setStatus(allowedStatus.SUCCESS);
            } else {
              setStatus(allowedStatus.FAILED);
            }
          })
          .catch(err => {
            console.log('Refund failed ', err);
            setStatus(allowedStatus.FAILED);
          });
      };

      const handleChange = value => {
        setRefundQty(value);
      };

      const radioButtons = [];
      try {
        for (let i = 1; i <= product.qty; i++) {
          radioButtons.push(
            <Form.Field>
              <Radio
                label={`${i} x deposit ($${product.price * i})`}
                name="radioGroup"
                value={i}
                checked={refundQty === i}
                onChange={() => handleChange(i)}
              />
            </Form.Field>,
          );
        }
      } catch (e) {
        console.log('ERROR:', e);
      }

      return (
        <Modal size="mini" open>
          <Modal.Header>Refund Credits</Modal.Header>
          {(status === allowedStatus.NOT_STARTED || status === allowedStatus.STARTED) && (
            <React.Fragment>
              <Modal.Content>
                <Form>
                  {radioButtons}
                  <p>Warning: This cannot be undone once refund.</p>
                </Form>
              </Modal.Content>
              <Modal.Actions>
                <Button disabled={status === allowedStatus.STARTED} basic onClick={() => close()}>
                  Cancel
                </Button>
                <Button
                  disabled={status === allowedStatus.STARTED}
                  loading={status === allowedStatus.STARTED}
                  primary
                  onClick={() => processRefund(id)}>
                  Confirm
                </Button>
              </Modal.Actions>
            </React.Fragment>
          )}

          {status === allowedStatus.SUCCESS && (
            <React.Fragment>
              <Modal.Content>
                <Message success header="Refund successful" />
              </Modal.Content>
              <Modal.Actions>
                <Button
                  primary
                  onClick={() => {
                    close();
                    getData(booking_id);
                  }}>
                  Close
                </Button>
              </Modal.Actions>
            </React.Fragment>
          )}
          {status === allowedStatus.FAILED && (
            <React.Fragment>
              <Modal.Content>
                <Message negative header="Refund Failed" />
              </Modal.Content>
              <Modal.Actions>
                <Button primary onClick={() => close()}>
                  Close
                </Button>
              </Modal.Actions>
            </React.Fragment>
          )}
        </Modal>
      );
    },
  ),
);
