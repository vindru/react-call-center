import React, { useState } from 'react';
import { connect } from 'react-redux';
import { useDrag } from 'react-dnd';
import { Segment, Header, Grid, Input, Button, Icon } from 'semantic-ui-react';
import classNames from 'classnames';
import { Menu, Item, MenuProvider } from 'react-contexify';
import 'react-contexify/dist/ReactContexify.min.css';

import { Counter } from '../../common';
import './DebitCreditItem.scss';

export default connect()(
  ({ id, name, price, min, max, fixed, credit, debit, auto, dispatch, qty, paid, counter, openRefundModal }) => {
    const [options, drag] = useDrag({
      item: {
        id,
        type: 'ADDED_PRODUCT',
        fixed,
      },
      end: dropResult => {
        if (dropResult) {
          // console.log(dropResult);
          // alert(`You dropped ${name} into ${dropResult.name}!`);
        }
      },
      collect: monitor => ({
        isDragging: monitor.isDragging(),
      }),
    });

    const MyMenu = () => (
      <Menu id={id}>
        <Item onClick={() => openRefundModal(id)}>Give Refund</Item>
      </Menu>
    );

    const handelChange = newQuantity => {
      dispatch({
        type: 'PRODUCT_QTY_CHANGE_UPDATE_SCREEN',
        payload: {
          id,
          qty: newQuantity,
        },
      });

      // here we need check whthere we wnat to add as Green / Red
      // and then dispacth DROPPED_PRODUCT
    };

    const handlePriceChange = e => {
      let { value } = e.target;
      value = value || 0;
      const re = /^[0-9\b]+$/;
      if (re.test(value)) {
        dispatch({
          type: 'PRODUCT_AMT_CHANGE_UPDATE_SCREEN',
          payload: {
            id,
            value,
          },
        });
      }
    };

    const [edit, toggleEditable] = useState(false);

    const renderComponent = (
      <Segment
        color={classNames('left', 'productSegment', {
          orange: debit,
          green: credit || paid,
          red: auto,
        })}>
        <Grid verticalAlign="middle">
          <Grid.Row columns={3}>
            <Grid.Column width={counter ? 7 : 9} className="productName">
              <Header as="h5">{name}</Header>
              <div className="subInfo">
                {!edit && (
                  <a as="button" className="pointer-cursor" onClick={() => toggleEditable(!edit)}>
                    Edit
                  </a>
                )}
              </div>
            </Grid.Column>
            {counter && (
              <Grid.Column width={5} className="productPrice">
                <Counter onChange={handelChange} qty={qty} max={max} min={min} />
              </Grid.Column>
            )}
            {!counter && (
              <Grid.Column width={1} className="productPrice">
                <Header size="small" textAlign="right">{`x${qty}`}</Header>
              </Grid.Column>
            )}
            {debit && (
              <Grid.Column width={counter ? 4 : 5} className="productPrice">
                {!edit && (
                  <Header textAlign="right" size="small">
                    {`$${price}`}
                  </Header>
                )}
                {edit && (
                  <Input
                    id="editPriceInput"
                    textAlign="right"
                    value={price}
                    onChange={e => handlePriceChange(e)}
                    action>
                    <input />
                    <Button onClick={() => toggleEditable(!edit)} icon>
                      <Icon name="check" />
                    </Button>
                  </Input>
                )}
              </Grid.Column>
            )}

            {!debit && (
              <Grid.Column width={counter ? 4 : 5} className="productPrice">
                <Header textAlign="right" size="small">
                  {`$${price * qty}`}
                </Header>
              </Grid.Column>
            )}
          </Grid.Row>
        </Grid>
      </Segment>
    );
    return (
      <div ref={drag}>
        {paid && <MyMenu />}
        <MenuProvider id={id}>{renderComponent}</MenuProvider>
      </div>
    );
  },
);
