import React, { useState } from 'react';
import { Segment, Header, Grid, Input, Button, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import classNames from 'classnames';

import './DebitCreditItem.scss';

export default connect(s => ({ products: s.Bookings.updateBooking.products }))(
  ({ name, credit, debit, auto, id, dispatch, price, qty, type, products }) => {
    const handleChange = (e, maxPrice) => {
      let { value } = e.target;
      value = value || 0;
      console.log(parseInt(value, 10), maxPrice);
      const re = /^[0-9\b]+$/;
      if (re.test(value) && parseInt(value, 10) <= maxPrice) {
        dispatch({
          type: 'DEPOSIT_AMT_CHANGE_UPDATE_SCREEN',
          payload: {
            id,
            value,
            type,
          },
        });
      }
    };

    const [edit, toggleEditable] = useState(false);

    // const deposit = products.find(p => ['DEPOSIT', 'DEBIT'].indexOf(p.type) > -1 && p.id === id);
    // const { qty } = deposit;
    let product;
    try {
      product = products.find(p => p.id === id && ['PRODUCT', 'OTHER_PRODUCT'].indexOf(p.type) > -1);
    } catch (e) {
      console.log('ERROR:', e);
    }
    const depositPerItem = price;
    const totalPrice = price * qty;
    return (
      <Segment color={classNames('left', 'productSegment', { orange: debit, green: credit, red: auto })}>
        <Grid verticalAlign="middle">
          <Grid.Row columns={3}>
            <Grid.Column width={9} stretched className="productName">
              <Header as="h6">{name}</Header>
              <div className="subInfo">
                <p>{`$${depositPerItem}`}</p>
                {!edit && (
                  <a className="pointer-cursor" onClick={() => toggleEditable(!edit)}>
                    Edit
                  </a>
                )}
              </div>
            </Grid.Column>
            <Grid.Column width={1} className="productPrice">
              <Header size="small" textAlign="right">{`x${qty}`}</Header>
            </Grid.Column>
            <Grid.Column width={5} className="productPriceInput">
              {!edit && <Header size="small" textAlign="right">{`$${totalPrice}`}</Header>}
              {edit && (
                <Input
                  textAlign="right"
                  value={depositPerItem}
                  onChange={e => {
                    if (product) {
                      handleChange(e, product.price);
                    }
                  }}
                  action>
                  <input />
                  <Button onClick={() => toggleEditable(!edit)} icon>
                    <Icon name="check" />
                  </Button>
                </Input>
              )}
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
    );
  },
);
