import React from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { useDrop } from 'react-dnd';
import { Segment, Header } from 'semantic-ui-react';

import './Drop.scss';

const Dustbin = ({ accept, dispatch, green }) => {
  const [{ canDrop, isOver }, drop] = useDrop({
    accept,
    drop: item => {
      dispatch({ type: 'DROPPED_PRODUCT_UPDATE_SCREEN', payload: { ...item, createDeposit: true } });

      return { name: 'Dustbin' };
    },
    collect: monitor => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  });
  return (
    <div ref={drop} className={classNames('dropBox', { canDrop, isOver })}>
      <Segment className={classNames('left', { green, orange: !green })} textAlign="center">
        <Header as="h5">Drop to add</Header>
      </Segment>
    </div>
  );
};
export default connect()(Dustbin);
