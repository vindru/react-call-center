import React, { Component, useState } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Segment, Header, Grid, Divider, Button, Modal, Form } from 'semantic-ui-react';
import DebitItem from './DebitCreditItem';
import DepositItem from './DepositItem';
import Drop from './Drop';
// import DropDelete from './DropDelete';
import RefundModal from './RefundModal';

import { getProducts } from '../../../../../store/Bookings/Bookings.actions';

class BookingInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenRefundModal: false,
      creditId: null,
      openPaymentModal: false,
    };
  }

  closeModal = () => {
    this.setState({ openPaymentModal: false });
  };

  toggleRefundModal = bool => {
    this.setState({
      isOpenRefundModal: bool,
    });
  };

  setCreditId = id => {
    this.setState({
      creditId: id,
    });
  };

  openRefundModal = id => {
    this.toggleRefundModal(true);
    this.setCreditId(id);
  };

  render() {
    const {
      products,
      subtotal,
      deposit,
      experienceTotal,
      depositWithFee,
      totalDiscount,
      pendingAmount,
      alreadyPaid,
      previousPaymentMethods,
    } = this.props.Bookings.updateBooking;

    const { getData } = this.props;
    let items, creditItems, depositItems, debitItems, paidItems;
    try {
      items = products.filter(p => ['PRODUCT', 'OTHER_PRODUCT'].indexOf(p.type) > -1);
      creditItems = products.filter(p => ['PROMOTION'].indexOf(p.type) > -1);
      depositItems = products.filter(p => ['DEPOSIT'].indexOf(p.type) > -1);
      debitItems = products.filter(p => ['DEBIT'].indexOf(p.type) > -1);
      paidItems = products.filter(p => ['CREDIT'].indexOf(p.type) > -1);
    } catch (e) {
      console.log('ERROR:', e);
    }

    return (
      <React.Fragment>
        {this.state.isOpenRefundModal && (
          <RefundModal close={() => this.toggleRefundModal(false)} getData={getData} id={this.state.creditId} />
        )}
        <Header as="h3">Booking</Header>
        {items &&
          items.map(di => (
            <DebitItem debit counter key={di.id} {...di}>
              {' '}
            </DebitItem>
          ))}
        <Drop accept="PRODUCT" />
        <Drop accept="OTHER_PRODUCT" />
        <Segment basic>
          <Grid verticalAlign="middle" columns="equal">
            <Grid.Row>
              <Grid.Column width={12}>Subtotal</Grid.Column>
              <Grid.Column>
                <Header size="small" textAlign="right">{`$${subtotal}`}</Header>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
        {debitItems &&
          debitItems.map(di => (
            <DepositItem auto key={di.id} {...di}>
              {' '}
            </DepositItem>
          ))}
        {depositItems &&
          depositItems.map(di => (
            <DepositItem auto key={di.id} {...di}>
              {' '}
            </DepositItem>
          ))}
        <Divider />
        {paidItems &&
          paidItems.map(item => (
            <DebitItem paid key={item.id} openRefundModal={this.openRefundModal} {...item}>
              {' '}
            </DebitItem>
          ))}
        {creditItems &&
          creditItems.map(di => (
            <DebitItem credit key={di.id} {...di}>
              {' '}
            </DebitItem>
          ))}
        <Drop accept="PROMOTION" green />
        <Segment basic>
          <Grid verticalAlign="middle" columns="equal">
            <Grid.Row>
              <Grid.Column width={12}>Total Discount</Grid.Column>
              <Grid.Column>
                <Header size="small" textAlign="right">{`$${totalDiscount}`}</Header>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
        <Segment basic>
          <Grid verticalAlign="middle" columns="equal">
            <Grid.Row>
              <Grid.Column width={12}>Experience Total</Grid.Column>
              <Grid.Column>
                <Header size="small" textAlign="right">{`$${experienceTotal}`}</Header>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
        <Segment basic>
          <Grid verticalAlign="middle" columns="equal">
            <Grid.Row>
              <Grid.Column width={12}>Pending Amount</Grid.Column>
              <Grid.Column>
                <Header size="small" textAlign="right">{`$${pendingAmount}`}</Header>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
        <Segment basic>
          <Grid verticalAlign="middle" columns="equal">
            <Grid.Row>
              <Grid.Column width={12}>Already Paid</Grid.Column>
              <Grid.Column>
                <Header size="small" textAlign="right">{`$${alreadyPaid}`}</Header>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
        {/* <Segment basic> */}
        {/*  <Grid verticalAlign="middle" columns="equal"> */}
        {/*    <Grid.Row> */}
        {/*      <Grid.Column width={12}> */}
        {/*        Total After Deposit */}
        {/*      </Grid.Column> */}
        {/*      <Grid.Column> */}
        {/*        <Header size="small" textAlign="right">{`$${total}`}</Header> */}
        {/*      </Grid.Column> */}
        {/*    </Grid.Row> */}
        {/*  </Grid> */}
        {/* </Segment> */}
        <Segment basic>
          <Grid verticalAlign="middle" columns="equal">
            <Grid.Row>
              <Grid.Column width={12}>Deposit</Grid.Column>
              <Grid.Column>
                <Header size="small" textAlign="right">{`$${deposit}`}</Header>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
        <Divider />
        <Segment basic>
          <Grid verticalAlign="middle" columns="equal">
            <Grid.Row>
              <Grid.Column width={12}>Deposit with Fees</Grid.Column>
              <Grid.Column>
                <Header size="small" textAlign="right">{`$${depositWithFee}`}</Header>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
        {depositWithFee > 0 ? (
          <React.Fragment>
            <Button
              primary
              onClick={e => {
                this.setState({ openPaymentModal: true });
              }}
              className="PayNowModalTriggerButton"
              fluid
              content={`Pay Now  $${depositWithFee}`}
            />
            <Modal size="small" open={this.state.openPaymentModal} onClose={this.closeModal} closeIcon>
              <Modal.Header>Choose your payment method</Modal.Header>
              <Modal.Content>
                <Form>
                  <Modal.Description>
                    {previousPaymentMethods && previousPaymentMethods.length > 0 ? (
                      <React.Fragment>
                        <label className="container">
                          Last payment method
                          <input type="radio" name="radio" />
                          <span className="checkmark" />
                        </label>
                        {previousPaymentMethods.map(method => {
                          return (
                            <Segment compact>
                              <p>{method.account}</p>
                              <p>{method.nameOnTheCard ? method.nameOnTheCard : ''}</p>
                            </Segment>
                          );
                        })}
                      </React.Fragment>
                    ) : (
                      ''
                    )}
                  </Modal.Description>
                  <Divider />
                  <Modal.Description>
                    <label className="container">
                      {' '}
                      Other credit card
                      <input type="radio" name="radio" />
                      <span className="checkmark" />
                    </label>
                    <Form.Group widths="equal">
                      <Form.Input label="CARD NUMBER" placeholder="xxxx xxxx xxxx xxxx" />
                      <Form.Dropdown
                        compact
                        selection
                        label="EXPIRY MONTH"
                        options={[{ key: 1, text: 'Choice 1', value: 1 }]}
                        placeholder="10"
                      />
                      <Form.Dropdown
                        compact
                        selection
                        label="EXPIRY YEAR"
                        options={[{ key: 1, text: 'Choice 1', value: 1 }]}
                        placeholder="2016"
                      />
                      <Form.Input label="CVC CODE" placeholder="000" />
                    </Form.Group>
                    <Form.Group widths="equal">
                      <Form.Input placeholder="Enter Name on Card" />
                      <Form.Input placeholder="Enter Address" />
                    </Form.Group>
                    <Form.Group>
                      <Form.Input width={4} placeholder="Enter Zip Code" />
                    </Form.Group>
                    <Divider />
                    <Button floated="left" basic onClick={this.closeModal}>
                      Cancel
                    </Button>
                    <Button floated="right" color="green">
                      Pay {`$${depositWithFee}`}
                    </Button>
                  </Modal.Description>
                </Form>
              </Modal.Content>
            </Modal>
          </React.Fragment>
        ) : (
          ''
        )}
        {/* <br /> */}
        {/* <DropDelete /> */}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  ...state,
});

const mapDispatchToProps = dispatch => ({
  updateBookingInfo: payload => dispatch({ type: 'BASICINFO_CHANGE', payload }),
  setPlace: placeId => dispatch({ type: 'PLACE_PICKED', payload: placeId }),
  fetchProducts: placeId => dispatch(getProducts(placeId)),
  setAvailability: payload => dispatch({ type: 'GET_AVAILABILITY', payload }),
  updatePaymentInfo: payload => dispatch({ type: 'PAYMENTINFO_CHANGE', payload }),
  resetBookingInfo: () => dispatch({ type: 'RESET_BOOKINGINFO' }),
  setTandemPrices: payload => dispatch({ type: 'SET_TANDEM_PRICES', payload }),
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(BookingInfo),
);
