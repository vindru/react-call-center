import React from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { useDrop } from 'react-dnd';
import { Segment, Header } from 'semantic-ui-react';

import './Drop.scss';
import delIcon from './deleteIcon.png';

const Dustbin = ({ dispatch, green }) => {
  const [{ canDrop, isOver }, drop] = useDrop({
    accept: 'ADDED_PRODUCT',
    drop: item => {
      if (!item.fixed) {
        dispatch({
          type: 'DROPPED_PRODUCT_DELETE_UPDATE_SCREEN',
          payload: item,
        });
      }
      return { name: 'Dustbin' };
    },
    collect: monitor => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  });
  return (
    <div ref={drop} className={classNames('dropBox', 'delete', { canDrop, isOver })}>
      <Segment textAlign="center">
        <Header as="h5" disabled>
          <img alt="del" src={delIcon} />
          Drag here to remove
        </Header>
      </Segment>
    </div>
  );
};
export default connect()(Dustbin);
