import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Checkbox, Form, Input, Header, Button } from 'semantic-ui-react';

import './CustomerInfo.scss';

class CustomerInfo extends Component {
  render() {
    const { dispatch, Bookings, toggleCustomerInfoEditable, isCustomerInfoEditable } = this.props;

    const { updateBooking } = { ...Bookings };

    const handleChange = (e, d) => {
      const payload = {};
      payload[d.name] = d.checked === undefined ? d.value : d.checked;
      dispatch({ type: 'BASICINFO_CHANGE_UPDATE_SCREEN', payload });
    };
    const handleCancel = () => {
      dispatch({ type: 'CANCEL_UPDATE_SCREEN_EDIT_CUSTOMER_INFO' });
      toggleCustomerInfoEditable();
    };

    return (
      <React.Fragment>
        <Header as="h3" className="verticalBoxHeader">
          Customer Info
        </Header>
        {isCustomerInfoEditable ? (
          <Header as="h5" className="verticalBoxHeader" disabled floated="right" onClick={handleCancel}>
            Cancel
          </Header>
        ) : (
          <Header as="h5" className="verticalBoxHeader" disabled floated="right" onClick={toggleCustomerInfoEditable}>
            Edit
          </Header>
        )}
        <Form>
          <Form.Field>
            <label htmlFor="first">FIRST NAME</label>
            {isCustomerInfoEditable ? (
              <Input
                type="text"
                maxLength="35"
                onChange={handleChange}
                id="first"
                name="firstname"
                value={updateBooking ? updateBooking.firstname : ''}
              />
            ) : (
              updateBooking && updateBooking.firstname !== '' && <p>{updateBooking.firstname} </p>
            )}
          </Form.Field>
          <Form.Field>
            <label htmlFor="surname">SURNAME</label>
            {isCustomerInfoEditable ? (
              <Input
                type="text"
                maxLength="35"
                onChange={handleChange}
                id="surname"
                name="surname"
                value={updateBooking ? updateBooking.surname : ''}
              />
            ) : (
              updateBooking && updateBooking.surname !== '' && <p>{updateBooking.surname} </p>
            )}
          </Form.Field>
          <Form.Field>
            <label htmlFor="phonenumber">PHONE NUMBER</label>
            {isCustomerInfoEditable ? (
              <Input
                type="tel"
                maxLength="35"
                onChange={handleChange}
                id="phonenumber"
                name="phone"
                value={updateBooking ? updateBooking.phone : ''}
              />
            ) : (
              updateBooking && updateBooking.phone !== '' && <p>{updateBooking.phone} </p>
            )}
          </Form.Field>
          <Form.Field>
            <a href={updateBooking ? `tel:${updateBooking.phone}` : '#'}>
              <Button basic>Call</Button>
            </a>
            <a href={updateBooking ? `sms://${updateBooking.phone}` : '#'}>
              <Button basic>Message</Button>
            </a>
          </Form.Field>
          <Form.Field>
            <label htmlFor="email">EMAIL</label>
            {isCustomerInfoEditable ? (
              <Input
                type="email"
                maxLength="105"
                onChange={handleChange}
                id="email"
                name="email"
                value={updateBooking ? updateBooking.email : ''}
              />
            ) : (
              updateBooking && updateBooking.email !== '' && <p>{updateBooking.email} </p>
            )}
          </Form.Field>
          <Form.Field>
            <Checkbox
              disabled={!isCustomerInfoEditable}
              onChange={handleChange}
              checked={updateBooking ? updateBooking.consent18 : false}
              name="consent18"
              label="Confirm above 18"
            />
          </Form.Field>
          <Form.Field>
            <Checkbox
              disabled={!isCustomerInfoEditable}
              onChange={handleChange}
              checked={updateBooking ? updateBooking.consent_weight : false}
              name="consent_weight"
              label="230lbs or less"
            />
          </Form.Field>
        </Form>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  ...state,
});
export default connect(mapStateToProps)(CustomerInfo);
