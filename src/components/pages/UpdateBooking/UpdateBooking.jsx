import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Divider, Grid, Header, Modal, Button } from 'semantic-ui-react';
import { DndProvider } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import moment from 'moment';

import './UpdateBooking.scss';
import Notes from '../Notes';
import {
  BookingTimePlacePickerBar,
  CustomerInfo,
  BookingInfo,
  ProductsAndPromotions,
  ChannelInfo,
  SourceInfo,
  PickUpInfo,
  MainViewGroupView,
  ErrorsMessage,
} from './sections';
import {
  suspendBoooking,
  getProducts,
  getBookingFromBookingId,
  createNotesByBookingId,
  getAvailablityByDate,
  updateBookingByInvoiceId,
} from '../../../store/Bookings/Bookings.actions';

const todaysDate = moment();

class UpdateBooking extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notes: [],
      openConfirmModal: false,
      redirect: false,
      message: '',
      isUpdatingBooking: false,
      isSavingForLater: false,
      isUpdateSuccessful: false,

      // booking_id: '',
      // checkin_time: { hours: 0, minutes: 0 },
      // createdAt: '',
      // date: '',
      // email: '',
      // invoiceDetails: [],
      // invoice_id: '',
      // location_id: '',
      // jumping_date: '',
      // line_items: [],
      // name: '',
      // paid: false,
      // phone: '',
      // slots: [],
      // updatedAt: '',
      // __v: 0,
      // _id: '',

      enableMoving: false,
      // insertManualCredit
      insertManualCreditAmount: 0,
      insertManualCreditQuantity: 0,
      openinsertManualCreditModal: false,
      // for suspend modal
      isSuspendConfirmModal: false,
      isSuspended: false,
      // for read only mode
      isReadOnly: false,
      booking_id: '',
      // for editing customer info
      isCustomerInfoEditable: false,
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.updatePayment = this.updatePayment.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.storeNotes = this.storeNotes.bind(this);
    this.CreateNotes = this.CreateNotes.bind(this);
    this.updateBookingItems = this.updateBookingItems.bind(this);
    this.getData = this.getData.bind(this);
    this.insertManualCreditHandler = this.insertManualCreditHandler.bind(this);
  }

  componentWillMount() {
    if (this.props.match.params.id) {
      this.setState({ booking_id: this.props.match.params.id });
      this.getData(this.props.match.params.id);
    } else {
      this.props.history.push('/');
    }
  }

  // on payment form submit handler
  onSubmit() {
    // Validate the data and create the booking.

    const { Bookings } = { ...this.props };
    const { updateBooking } = { ...Bookings };
    console.log('updateBooking', updateBooking);
    try {
      const errors = [];
      if (updateBooking.firstname === '') {
        errors.push({ id: 'firstName', message: 'First Name cannot be empty.' });
      }
      if (updateBooking.surname === '') {
        errors.push({ id: 'surname', message: 'Surname cannot be empty.' });
      }
      if (updateBooking.email === '') {
        errors.push({ id: 'email', message: 'Email cannot be empty.' });
      }
      if (updateBooking.phonenumber === '') {
        errors.push({ id: 'phoneNumber', message: 'Phone number cannot be empty.' });
      }
      if (updateBooking.products.length === 0) {
        errors.push({ id: 'products', message: 'Total Products cannot be zero.' });
      }
      if (!updateBooking.consent18) {
        errors.push({ id: 'consent18', message: 'Person should be above 18.' });
      }
      if (!updateBooking.consent_weight) {
        errors.push({ id: 'consent_weight', message: 'Person should be in given weight limit.' });
      }

      const payload = {};
      payload.errors = errors;
      this.props.updateBookingInfo(payload);

      console.log('Errors', errors);

      if (errors.length === 0) {
        // Create Booking
        this.setState({ isUpdatingBooking: true });
        // Calculate addOns
        let addOnProducts = updateBooking.products.filter(
          p =>
            ['OTHER_PRODUCT', 'PRODUCT'].indexOf(p.type) > -1 &&
            ['CREDIT'].indexOf(p.type) === -1 &&
            p.name !== 'Tandem',
        );
        addOnProducts = addOnProducts.map(product => ({
          code: product.code,
          label: product.name,
          amount: product.price,
          quantity: product.qty,
          forUser: [],
        }));

        const allCredits = [];
        const credits = updateBooking.products.filter(p => p.type === 'CREDIT');
        credits.forEach(credit => {
          for (let i = 0; i < credit.qty; i++) {
            allCredits.push({
              line_id: credit.lineId,
              type: 'credit',
              timestamp: moment().toDate(),
              code: credit.code,
              label: credit.name,
              amount: credit.price,
              retref: credit.retref,
            });
          }
        });
        // Calculate Total Tandem
        const tandemProduct = updateBooking.products.find(x => x.name === 'Tandem' && x.type === 'PRODUCT');
        // const tandemDeposit = updateBooking.products.find(x => x.name === 'Tandem' && x.type === 'DEPOSIT');

        const totalQty = tandemProduct ? tandemProduct.qty : 0;

        const depositPerTandem = tandemProduct.deposit;
        const pricePerTandem = tandemProduct ? tandemProduct.price : 0;

        const requestBody = {
          location_id: Bookings.place,
          name: updateBooking.firstname,
          surname: updateBooking.surname,
          email: updateBooking.email,
          phone: updateBooking.phone,
          number_of_spaces: totalQty,
          space_price: pricePerTandem, // price of one tandem
          space_deposit_price: depositPerTandem * totalQty, // deposit of all tandems
          addons: addOnProducts,
          checkin_time: { hours: updateBooking.checkin_time.hours, minutes: updateBooking.checkin_time.minutes },
          date: updateBooking.jumping_date.toDate(),
          consent18: updateBooking.consent18,
          consent_weight: updateBooking.consent_weight,
          channel: updateBooking.selectedChannel,
          source: updateBooking.selectedSource,
          status: updateBooking.status,
          credits: allCredits,
          updated_by: 'David',
        };

        if (updateBooking.pickUpRequired) {
          requestBody.pickup_time = updateBooking.selectedPickUpTime;
          requestBody.pickup_spot = updateBooking.selectedSpot;
        }

        const invoiceId = updateBooking.invoice_id;

        console.log('Request Body', requestBody, invoiceId);
        updateBookingByInvoiceId(requestBody, invoiceId)
          .then(async resp => {
            console.log('Response Update Booking', resp);
            if (resp.data.invoice_id) {
              this.CreateNotes(resp.booking_id);
              // Show alert and redirect to Home.
              console.log(resp.data);
              this.setState({
                openConfirmModal: true,
                redirect: false,
                message: 'Booking Updated.',
                isUpdatingBooking: false,
                isUpdateSuccessful: true,
              });
              this.props.saveUpdateBooking();
            } else {
              // Show alert and redirect to Home.
              this.setState({
                openConfirmModal: true,
                redirect: false,
                message: 'Error while updating booking',
                isUpdatingBooking: false,
              });
            }
          })
          .catch(err => {
            console.log('Error in Create booking', err);
            this.setState({
              openConfirmModal: true,
              redirect: false,
              message: 'Error while updating booking',
              isUpdatingBooking: false,
            });
          });
      }
    } catch (e) {
      console.log('ERROR: ', e);
    }
  }

  async getData(bookingId) {
    // previous state should have been flushed.
    await this.props.resetBookingInfo();
    try {
      const response = await getBookingFromBookingId(bookingId);
      if (response.data) {
        const bookingDate = moment(response.data.date);
        if (response.data.status === 'SUSPEND') {
          this.setState({
            openConfirmModal: true,
            message: 'This booking has been suspended.',
            isSuspended: true,
          });
        } else if (bookingDate.isBefore(todaysDate, 'days')) {
          this.setState({
            openConfirmModal: true,
            message: 'Its a read-only booking.',
            isReadOnly: true,
          });
        }
        const bookingData = response.data;

        const payload = {
          firstname: bookingData.invoiceDetails[0].name,
          surname: bookingData.invoiceDetails[0].surname,
          email: bookingData.invoiceDetails[0].email,
          phone: bookingData.invoiceDetails[0].phone,
          consent18: bookingData.invoiceDetails[0].consent18,
          consent_weight: bookingData.invoiceDetails[0].consent_weight,
          numberOfSpace: bookingData.number_of_spaces,
          products: [],
          subtotal: 0,
          total: 0,
          totalDiscount: 0,
          experienceTotal: 0,
          deposit: 0,
          depositWithFee: 0,

          selectedChannel: bookingData.invoiceDetails[0].channel,
          selectedSource: bookingData.invoiceDetails[0].channel,
          selectedSpot: bookingData.invoiceDetails[0].pickup_spot,
          selectedPickUpTime: bookingData.invoiceDetails[0].pickup_time,
          pickUpRequired: bookingData.invoiceDetails[0].pickup_spot !== '',
          booking_id: bookingData.booking_id,
          checkin_time: bookingData.checkin_time,
          createdAt: bookingData.createdAt,
          date: moment(bookingData.date),
          invoiceDetails: bookingData.invoiceDetails,
          invoice_id: bookingData.invoice_id,
          location_id: bookingData.location_id,
          jumping_date: moment(bookingData.jumping_date),
          paid: bookingData.paid,
          slots: bookingData.slots,
          updatedAt: bookingData.invoiceDetails[0].updatedAt,
          previousPaymentMethods: bookingData.invoiceDetails[0].previous_payment_methods,
        };
        await this.props.setUpdateBooking(payload);
        await this.updateBookingItems();
        this.props.fetchProducts(bookingData.location_id);
      } else {
        this.setState({
          openConfirmModal: true,
          message: 'Booking not found.Please enter a valid booking-id in url or select a booking from home screen.',
          isReadOnly: true,
          redirect: true,
        });
      }
    } catch (e) {
      this.setState({
        openConfirmModal: true,
        message: 'Booking not found.Please enter a valid booking-id in url or select a booking from home screen.',
        isReadOnly: true,
        redirect: true,
      });
      console.log(e);
    }
  }

  toggleCustomerInfoEditable = () => {
    this.setState(prevState => {
      return {
        isCustomerInfoEditable: !prevState.isCustomerInfoEditable,
      };
    });
  };

  handleMove = () => {
    // const { jumping_date, location_id  } = { ...this.props.Bookings.updateBooking };
    const jumpingDate = this.props.Bookings.updateBooking.jumping_date;
    const locationId = this.props.Bookings.updateBooking.location_id;
    this.props.enableMoving();

    getAvailablityByDate(jumpingDate, locationId)
      .then(rules => {
        this.props.setAvailability(rules);
      })
      .catch(e => {
        console.log('ERROR:', e);
      });
    this.props.fetchProducts(locationId);
  };

  handleSuspend = status => {
    // Suspend function coming from Redux actions
    suspendBoooking({
      booking_id: this.state.booking_id,
      body: {
        status,
      },
    })
      .then(async resp => {
        if (resp.data.status === 'SUSPEND') {
          this.setState({
            openConfirmModal: true,
            redirect: false,
            message: 'Booking Suspended.',
            isSuspended: true,
            isSuspendConfirmModal: false,
          });
          this.props.suspendUpdateBooking();
        } else if (resp.data.status === 'LIVE') {
          this.setState({
            openConfirmModal: true,
            redirect: false,
            message: 'Booking Suspenion Removed.',
            isSuspended: false,
            isSuspendConfirmModal: false,
          });
        } else {
          this.setState({
            openConfirmModal: true,
            redirect: false,
            message: 'Error while suspending booking',
            isSuspended: false,
            isSuspendConfirmModal: false,
          });
        }
      })
      .catch(err => {
        this.setState({
          openConfirmModal: true,
          redirect: true,
          message: `Error while suspending : ${err}`,
          isSuspended: false,
          isSuspendConfirmModal: false,
        });
      });
  };

  insertManualCreditHandler = (e, formData) => {
    console.log(e, formData);
    const { insertManualCreditAmount, insertManualCreditQuantity } = this.state;
    // for green card
    this.props.droppedProduct({
      deposit: parseInt(insertManualCreditAmount, 10),
      id: `insertManualCredit_${Math.random()
        .toString(36)
        .substr(2, 9)}`,
      qty: parseInt(insertManualCreditQuantity, 10),
      name: 'CREDIT',
      max: 12,
      price: parseInt(insertManualCreditAmount, 10),
      type: 'PROMOTION',
      createDeposit: true,
    });
    // close Modal
    this.setState({ openinsertManualCreditModal: false });
  };

  updateBookingItems() {
    const { updateBooking } = { ...this.props.Bookings };
    const credits = [];
    const debits = [];
    try {
      updateBooking.slots.forEach(slotItem => {
        console.log('SLot Items', slotItem);
        slotItem.line_items.forEach(lineItem => {
          if (lineItem.type === 'debit') {
            const i = debits.find(x => x.code === lineItem.code);
            if (i) {
              i.qty += 1;
              i.lineItemsArray.push(lineItem.line_id);
            } else if (lineItem.code === 'tandem') {
              debits.push({
                ...lineItem,
                qty: 1,
                min: 1,
                max: 10,
                debit: true,
                slotId: slotItem.slot_id,
                lineItemsArray: new Array(lineItem.line_id),
              });
            } else {
              debits.push({
                ...lineItem,
                qty: 1,
                min: 0,
                max: 10,
                debit: true,
                slotId: slotItem.slot_id,
                lineItemsArray: new Array(lineItem.line_id),
              });
            }
          } else {
            const i = credits.find(x => x.code === lineItem.code);
            if (i) {
              i.qty += 1;
              i.lineItemsArray.push(lineItem.line_id);
            } else if (lineItem.code === 'tandem') {
              credits.push({
                ...lineItem,
                qty: 1,
                min: 1,
                max: 10,
                credit: true,
                slotId: slotItem.slot_id,
                lineItemsArray: new Array(lineItem.line_id),
              });
            } else {
              credits.push({
                ...lineItem,
                qty: 1,
                min: 0,
                max: 10,
                credit: true,
                slotId: slotItem.slot_id,
                lineItemsArray: new Array(lineItem.line_id),
              });
            }
          }
        });
      });
      // now drop debits with their quantity
      debits.forEach(debit => {
        // check for a product in debit its correpsonding counterpart exist in credit or not
        const credit = credits.find(x => x.code === debit.code);
        const creditQty = credit ? credit.qty : 0;
        const diff = debit.qty - creditQty;
        // for orange card
        this.props.droppedProduct({
          deposit: debit.code === 'tandem' ? debit.slot_deposit_price : debit.amount,
          originalDeposit: debit.code === 'tandem' ? debit.slot_deposit_price : debit.amount,
          id: debit.code,
          slotId: debit.slotId,
          lineItemsArray: debit.lineItemsArray,
          code: debit.code,
          max: debit.max,
          min: debit.min,
          qty: debit.qty,
          name: debit.label,
          price: debit.amount,
          originalPrice: debit.amount,
          fixed: debit.code === 'tandem',
          type: 'PRODUCT',
        });
        if (diff > 0 && debit.code === 'tandem') {
          // for red card
          this.props.droppedProduct({
            deposit: debit.code === 'tandem' ? debit.slot_deposit_price : debit.amount,
            originalDeposit: debit.code === 'tandem' ? debit.slot_deposit_price : debit.amount,
            id: debit.code,
            slotId: debit.slotId,
            code: debit.code,
            max: debit.max,
            min: debit.min,
            qty: creditQty,
            name: debit.label,
            price: debit.amount - (debit.slot_deposit_price || 0),
            originalPrice: debit.amount - (debit.slot_deposit_price || 0),
            type: 'DEBIT',
          });
          this.props.droppedProduct({
            deposit: debit.code === 'tandem' ? debit.slot_deposit_price : debit.amount,
            originalDeposit: debit.code === 'tandem' ? debit.slot_deposit_price : debit.amount,
            id: debit.code,
            slotId: debit.slotId,
            code: debit.code,
            max: debit.max,
            min: debit.min,
            qty: diff,
            name: debit.label,
            price: debit.slot_deposit_price,
            originalPrice: debit.slot_deposit_price,
            type: 'DEPOSIT',
          });
        }
        if (diff === 0 && debit.code === 'tandem') {
          this.props.droppedProduct({
            deposit: debit.code === 'tandem' ? debit.slot_deposit_price : debit.amount,
            originalDeposit: debit.code === 'tandem' ? debit.slot_deposit_price : debit.amount,
            id: debit.code,
            slotId: debit.slotId,
            code: debit.code,
            max: debit.max,
            min: debit.min,
            qty: debit.qty,
            name: debit.label,
            price: debit.amount - (debit.slot_deposit_price || 0),
            originalPrice: debit.amount - (debit.slot_deposit_price || 0),
            type: 'DEBIT',
          });
        }
        if (diff > 0 && debit.code !== 'tandem') {
          // for red card
          this.props.droppedProduct({
            deposit: debit.amount,
            originalDeposit: debit.amount,
            id: debit.code,
            slotId: debit.slotId,
            code: debit.code,
            max: debit.max,
            min: debit.min,
            qty: diff,
            name: debit.label,
            price: debit.amount,
            originalPrice: debit.amount,
            type: 'DEPOSIT',
          });
        }
      });
      // now drop only those credits which hasn't been dropped yet, with their quantity
      credits.forEach(credit => {
        // check for a product in credit its correpsonding counterpart exist in debit or not
        this.props.droppedProduct({
          deposit: credit.code === 'tandem' ? credit.slot_deposit_price : credit.amount,
          originalDeposit: credit.code === 'tandem' ? credit.slot_deposit_price : credit.amount,
          id: credit.code,
          code: credit.code,
          lineItemsArray: credit.lineItemsArray,
          slotId: credit.slotId,
          lineId: credit.line_id,
          max: credit.max,
          min: credit.min,
          qty: credit.qty,
          name: credit.label,
          price: credit.amount,
          originalPrice: credit.amount,
          retref: credit.retref,
          type: 'CREDIT',
        });
      });
    } catch (e) {
      console.log('ERROR:', e);
    }
  }

  storeNotes(note) {
    const { notes } = this.state;
    notes.push(note);
    this.setState({ notes });
    console.log(this.state.notes);
  }

  closeModal() {
    this.setState({ openConfirmModal: false, isSuspendConfirmModal: false });
    if (this.state.redirect) {
      this.props.history.push('/');
    }
  }

  updatePayment(e, d) {
    const payload = {};
    payload[d.name] = d.value;
    console.log('Payment Info payload', payload);
    this.props.updatePaymentInfo(payload);
  }

  CreateNotes(bookingId) {
    this.state.notes.forEach(item => {
      const requestData = {
        booking_id: bookingId,
        executive_id: 'user',
        note: item.note,
        date: item.date,
      };
      createNotesByBookingId(requestData)
        .then(() => {
          this.getNotes();
          console.log('note added');
          this.setState({
            noteInTheMaking: '',
          });
        })
        .catch(err => {
          console.log('catch 2 error', err.message);
          this.setState({ BroadcastError: 'Error in adding note' });
        });
    });
  }

  render() {
    const { history } = { ...this.props };
    const {
      openConfirmModal,
      redirect,
      message,
      isUpdatingBooking,
      isUpdateSuccessful,
      isSuspended,
      isReadOnly,
      isSuspendConfirmModal,
      isCustomerInfoEditable,
    } = {
      ...this.state,
    };

    // for enabling /disabling SAVE button
    let { hasChanged, date } = this.props.Bookings.updateBooking;
    // check if date is moment object or not
    if (!moment.isMoment(date)) {
      date = moment(date);
    }
    return (
      <DndProvider backend={HTML5Backend}>
        <Grid columns="16" celled="internally" id="UpdateBookingPage">
          <Modal
            open={openConfirmModal}
            onClose={() => this.closeModal()}
            closeOnEscape={false}
            closeOnDimmerClick={false}
            size="mini">
            <Modal.Content>
              <Header>{message}</Header>
            </Modal.Content>
            <Modal.Actions>
              {isReadOnly && (
                <React.Fragment>
                  <Button secondary onClick={() => this.closeModal()} inverted>
                    Close
                  </Button>
                </React.Fragment>
              )}
              {isSuspendConfirmModal && (
                <React.Fragment>
                  <Button primary onClick={() => this.handleSuspend('SUSPEND')} inverted>
                    Suspend Booking
                  </Button>
                  <Button secondary onClick={() => this.closeModal()} inverted>
                    Cancel
                  </Button>
                </React.Fragment>
              )}
              {isSuspended && !isUpdateSuccessful && !isReadOnly && (
                <React.Fragment>
                  <Button primary onClick={() => this.handleSuspend('LIVE')} inverted>
                    Remove Suspension
                  </Button>
                  <Button secondary onClick={() => this.closeModal()} inverted>
                    Close
                  </Button>
                </React.Fragment>
              )}
              {isUpdateSuccessful && !isReadOnly && (
                <Button color="green" onClick={() => this.closeModal()} inverted>
                  OK
                </Button>
              )}
              {!isUpdateSuccessful && !isReadOnly && !isSuspended && !isSuspendConfirmModal && (
                <Button color={!isSuspended ? 'green' : 'red'} onClick={() => this.closeModal()} inverted>
                  Close
                </Button>
              )}
            </Modal.Actions>
          </Modal>
          <Grid.Row className="header-bar">
            <Grid.Column width={16}>
              <Header as="h2" className="mt-5 main-title">
                Update Booking
              </Header>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row className="date-picker-bar">
            <Grid.Column width={16}>
              <BookingTimePlacePickerBar />
            </Grid.Column>
            {/* <Grid.Column className="bs-none" width={3}> */}
            {/*   <MainViewGroupView /> */}
            {/* </Grid.Column> */}
          </Grid.Row>
          <Grid.Row stretched className="booking-section">
            <Grid.Column width={3}>
              <CustomerInfo
                isReadOnly={isReadOnly}
                isCustomerInfoEditable={isCustomerInfoEditable}
                toggleCustomerInfoEditable={this.toggleCustomerInfoEditable}
              />
              <Divider />
              <ChannelInfo />
              <Divider />
              <SourceInfo />
              <Divider />
              <PickUpInfo isReadOnly={isReadOnly} />
            </Grid.Column>
            <Grid.Column width={5} id="BookingInfo">
              <BookingInfo getData={this.getData} isReadOnly={isReadOnly} />
            </Grid.Column>
            <Grid.Column width={8}>
              <Grid.Row>
                <ProductsAndPromotions isReadOnly={isReadOnly} />
              </Grid.Row>
              <Divider />
              <Grid.Row>
                <ErrorsMessage />
              </Grid.Row>
              <Grid.Row>
                <Notes Data="" date={date.toDate()} type="panel" id={this.state.booking_id} />
              </Grid.Row>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row
            className="padded-columns"
            id="actions-footer"
            columns={5}
            verticalAlign="middle"
            textAlign="center">
            <Grid.Column>
              {/*   <Modal size="mini" open={openinsertManualCreditModal} trigger={<Button onClick={() => this.setState({ openinsertManualCreditModal: true })} basic content="Insert Manual Credit" />} closeIcon>
                  <Modal.Header>Insert Manual Credit</Modal.Header>
                  <Modal.Content>

                    <Modal.Description>
                      <Form onSubmit={this.insertManualCreditHandler}>

                        <Form.Input fluid onChange={(e) => this.setState({insertManualCreditAmount: e.target.value})} label="ENTER AMOUNT" type="number" placeholder="$" />
                        <Form.Input fluid onChange={(e) => this.setState({insertManualCreditQuantity: e.target.value})} label="QUANTITY" type="number" icon="sort" />

                        <Divider />
                        <Button floated="left" basic>Cancel</Button>
                        <Button type="submit" floated="right" primary>Confirm</Button>

                      </Form>
                    </Modal.Description>
                  </Modal.Content>
                </Modal> */}
            </Grid.Column>
            <Grid.Column>
              <Button onClick={this.handleMove} basic content="Move" />
            </Grid.Column>
            <Grid.Column>
              <Button
                disabled={isReadOnly}
                onClick={e =>
                  isSuspended
                    ? this.setState({
                        openConfirmModal: true,
                        message: 'Are you sure you want to remove suspension?',
                        isSuspended: true,
                        isUpdateSuccessful: false,
                        redirect: false,
                      })
                    : this.setState({
                        openConfirmModal: true,
                        message: 'Are you sure you want to suspend this booking?',
                        isSuspendConfirmModal: true,
                        isUpdateSuccessful: false,
                        redirect: false,
                      })
                }
                basic
                content={isSuspended ? 'Remove Suspension' : 'Suspend'}
              />
            </Grid.Column>
            <Grid.Column>
              <Button
                onClick={() => {
                  this.props.history.push('/');
                }}
                basic
                content="Cancel"
              />
            </Grid.Column>
            <Grid.Column
              id="blue"
              color={
                hasChanged.products ||
                hasChanged.checkin_time ||
                hasChanged.jumping_date ||
                hasChanged.experienceTotal ||
                hasChanged.basic_info
                  ? 'blue'
                  : ''
              }>
              <Button
                loading={isUpdatingBooking}
                disabled={
                  !hasChanged.products &&
                  !hasChanged.checkin_time &&
                  !hasChanged.jumping_date &&
                  !hasChanged.experienceTotal &&
                  !hasChanged.basic_info
                }
                onClick={() => this.onSubmit()}
                basic
                content="Save"
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </DndProvider>
    );
  }
}

const mapStateToProps = state => ({
  ...state,
});

const mapDispatchToProps = dispatch => ({
  updateBookingInfo: payload => dispatch({ type: 'BASICINFO_CHANGE_UPDATE_SCREEN', payload }),
  setPlace: placeId => dispatch({ type: 'PLACE_PICKED', payload: placeId }),
  fetchProducts: placeId => dispatch(getProducts(placeId)),
  setAvailability: payload => dispatch({ type: 'GET_AVAILABILITY', payload }),
  updatePaymentInfo: payload => dispatch({ type: 'PAYMENTINFO_CHANGE', payload }),
  resetBookingInfo: () => dispatch({ type: 'RESET_BOOKINGINFO_UPDATE_SCREEN' }),
  enableMoving: () => dispatch({ type: 'ENABLE_MOVING' }),
  droppedProduct: payload => dispatch({ type: 'DROPPED_PRODUCT_UPDATE_SCREEN_INITIAL', payload }),

  productQuantityChange: payload => dispatch({ type: 'PRODUCT_QTY_CHANGE_UPDATE_SCREEN_INITIAL', payload }),
  setUpdateBooking: payload => dispatch({ type: 'SET_UPDATE_BOOKING', payload }),
  saveUpdateBooking: () => dispatch({ type: 'SAVE_UPDATE_BOOKING' }),
  suspendUpdateBooking: () => dispatch({ type: 'SUSPEND_UPDATE_BOOKING' }),
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(UpdateBooking),
);
