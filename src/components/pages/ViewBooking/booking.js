import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Card, Icon, Button, Label } from 'semantic-ui-react';

import Notes from '../Notes';
import './booking.scss';

export default class Booking extends Component {
  constructor(props) {
    super(props);
    this.state = {
      NotesDisplay: false,
      notes: 0,
    };
    this.printFriendsNumber = this.printFriendsNumber.bind(this);
    this.printOtherProducts = this.printOtherProducts.bind(this);
    this.handleNotes = this.handleNotes.bind(this);
  }

  onNotesChange = notes => this.setState({ notes });

  handleNotes() {
    const { NotesDisplay } = { ...this.state };
    this.setState({
      NotesDisplay: !NotesDisplay,
    });
  }

  printOtherProducts() {
    try {
      if (this.props.booking.slots) {
        let totalProductQty = 0;
        this.props.booking.slots.forEach(slot => {
          slot.line_items.forEach(item => {
            if (item.code !== 'tandem' && item.type === 'debit') {
              totalProductQty++;
            }
          });
        });
        return totalProductQty;
      }
      return '';
    } catch (e) {
      console.log('ERROR', e);
      return '';
    }
    return '';
  }

  printFriendsNumber() {
    try {
      if (this.props.booking.slots && this.props.booking.slots.length) {
        return this.props.booking.slots.length;
      }
      return '';
    } catch (e) {
      console.log('ERROR', e);
      return '';
    }
  }
  printBadge = status => {
    try {
      if (status) {
        if (status === '') {
          return 'Booked';
        } else if (status === 'SUSPEND') {
          return 'Suspended';
        } else {
          return status;
        }
      } else {
        return 'Booked';
      }
    } catch (e) {
      console.log('ERROR', e);
      return '';
    }
  };
  capitalizeFirstLetter = string => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  };
  render() {
    const { status } = this.props.booking;

    let surname, firstname;
    try {
      if (this.props.booking.invoiceDetails && this.props.booking.invoiceDetails[0]) {
        surname = this.capitalizeFirstLetter(this.props.booking.invoiceDetails[0].surname.trim());
      } else {
        surname = '';
      }
      firstname = this.capitalizeFirstLetter(this.props.booking.name.trim());
    } catch (e) {
      surname = '';
      console.log('ERROR', e);
    }

    return (
      <Card className="board-item">
        <Card.Content extra>
          <Card.Header>
            <p className="truncate">
              <Link to={this.props.booking && `/UpdateBooking/${this.props.booking.booking_id}`}>
                {surname}
                {', '}
                {firstname}
              </Link>
            </p>
            <Label.Group className="float-right" circular>
              <Label color={status === 'SUSPEND' ? 'orange' : ''} as="span">
                {this.printBadge(status)}
              </Label>
            </Label.Group>
          </Card.Header>
        </Card.Content>

        <Card.Content className="bookingButtonsRow" extra>
          <Button disabled={true} basic className="bookingButtons">
            <Icon name="group" size="small" />
            {this.printFriendsNumber()}
          </Button>
          <Button disabled={true} basic className="bookingButtons">
            <Icon name="cart" size="small" />
            {this.printOtherProducts()}
          </Button>
          <Button
            basic
            active={this.state.NotesDisplay}
            onClick={this.handleNotes}
            className="float-right bookingButtons">
            <Icon name="chat outline" size="small" />
            <span>{this.state.notes}</span>
          </Button>
        </Card.Content>
        <Card.Content extra></Card.Content>

        <Card.Content hidden={!this.state.NotesDisplay}>
          <Notes
            Data=""
            refreshnotes={this.props.refreshnotes}
            notes={this.props.notes}
            date={this.props.date && this.props.date.toDate()}
            onChange={this.onNotesChange}
            type="Drop"
            id={this.props.booking && this.props.booking.booking_id}
          />
        </Card.Content>
      </Card>
    );
  }
}
