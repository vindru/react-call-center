import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import Muuri from 'muuri';
import {
  Grid,
  Dropdown,
  Header,
  Divider,
  Modal,
  Menu,
  Checkbox,
  Label,
  Button,
  Dimmer,
  Loader,
  Message,
  Icon,
} from 'semantic-ui-react';
// all styles
import './index.css';
// components used in this
import axios from 'axios';
import FilterRow from './filterRow';
import TimeSlot from './timeslot';
import Booking from './booking';
// for create new booking clcik
import { getAvailablityByDate } from '../../../store/Bookings/Bookings.actions';
// fetching from  APIS
const AVAILABILITY_URL = `${process.env.REACT_APP_BOOKING_SERVER_URL}/api/v1`;

const BrodcastTemps = [
  { key: 1, text: 'none ', value: 1 },
  { key: 2, text: 'Templeate 1', value: 2 },
  { key: 3, text: 'Templeate 2', value: 3 },
];

// options for location
const optionsForLocation = [
  { key: 1, text: 'Las Vegas', value: 1 },
  { key: 2, text: 'Seaside', value: 2 },
  { key: 3, text: 'Santa Cruz', value: 3 },
];
const optionsForSort = [
  { key: 0, text: 'Sort1', value: 'Sort1' },
  { key: 1, text: 'Sort2', value: 'Sort2' },
  { key: 2, text: 'Sort3', value: 'Sort3' },
];
const optionsForFilter = [
  { key: 0, text: 'Filter1', value: 'Filter1' },
  { key: 1, text: 'Filter2', value: 'Filter2' },
  { key: 2, text: 'Filter3', value: 'Filter3' },
];
// --------- Modal css------
const inlineStyle = {
  modal: {
    position: 'absolute',
    marginTop: '0px !important',
    zIndex: '9999',
  },
};

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      timeslots: [],
      date: props.Bookings.date,
      location: 1,
      noBookings: true,

      isFetching: true,
      errorInFetch: false,
      BroadcastTextarea: '',
      BroadcastError: '',
      BrodcastModalCheck: 'Text',
      BroadcastModalList: '',
      BroadcastModalSend: true,
      BrodcastModalCheckEmail: true,
      BrodcastModalCheckText: false,
    };
    // change date to following format -  yyyy-mm-dd
    try {
      console.log('date', this.state.date);
      const date = this.state.date.format('YYYY-MM-DD');

      this.getData(date, this.state.location);
      this.getNotes(date);
    } catch (e) {
      console.log('DATE ERROR: ', e);
    }
  }

  getNotes = date => {
    console.log('dsdcsd', date);
    axios({
      method: 'get',
      url: `${process.env.REACT_APP_BOOKING_SERVER_URL}/api/v1/notes/date/${date}`, // ---|Broadcast api link
      config: { headers: { 'Content-Type': 'multipart/form-data' } },
    })
      .then(data => {
        console.log(data);
        this.setState({ notes: data.data });
        // this would pass it to parent component for display of number of notes
        // if(this.props.onChange){
        //  this.props.onChange(data.data.length);
        console.log(this.state.notes);
      })
      .catch(err => {
        console.log('catch 2 error', err.message);
      });
  };

  async getData(date, location) {
    const timeslots = [];
    try {
      const response = await axios.get(`${AVAILABILITY_URL}/checkAvailability/${date}/${location}`);
      console.log(response);
      if (Array.isArray(response.data)) {
        let noBookings = true;
        response.data.forEach(timeslot => {
          timeslots.push(timeslot);
          if (noBookings && timeslot.number_of_spaces_booked !== 0) {
            noBookings = false;
          }
        });
        this.setState({
          noBookings,
        });
      }

      this.setState({
        timeslots,
        errorInFetch: false,
        isFetching: false,
      });
      // Uncomment below for drag drop to work
      // this.dragDrop()
    } catch (e) {
      this.setState({
        errorInFetch: true,
        isFetching: false,
      });
      console.log(e);
    }
  }

  UseTemplate = value => {};

  // ---|Broadcast Api Call ------------------------------------------
  BroadCastCallApi = (timeSlot, ReciverList) => {
    this.setState({ BroadcastModalSend: false });

    const text = document.getElementById(`${timeSlot}BM_Text`).textContent;
    const Textlist = [];
    const Emaillist = [];

    ReciverList.map(person => Textlist.push(person.phone));

    ReciverList.map(person => Emaillist.push(person.email));

    if (Emaillist.length > 0 && Textlist.length > 0) {
      if (text !== '') {
        const Payload = {
          body: text,
          SendByMethod:
            this.state.BrodcastModalCheckEmail && this.state.BrodcastModalCheckText
              ? 'All'
              : this.state.BrodcastModalCheckEmail
              ? 'Email'
              : 'Text',
          Emaillist,
          TextList: Textlist,
        };
        axios({
          method: 'post',
          url: `${AVAILABILITY_URL}/broadcast`, // ---|Broadcast api link
          // url:"http://localhost:8081/api/v1/broadcast",
          data: Payload,
          config: { headers: { 'Content-Type': 'multipart/form-data' } },
        })
          .then(data => {
            data.data.broacastStats === 'succses'
              ? this.setState({ BroadcastModalClose: false, BroadcastModalSend: true, BroadcastTextarea: '' })
              : this.setState({ BroadcastError: 'Error in BrodCasting' });
          })
          .catch(err => {
            console.log('catch 2 error', err.message);
            this.setState({ BroadcastError: 'Error in BrodCasting', BroadcastModalSend: true });
          });
      } else {
        this.setState({ BroadcastError: 'Please write a message.', BroadcastModalSend: true });
      }
    } else {
      this.setState({ BroadcastError: 'No Recipents', BroadcastModalSend: true });
    }
  };

  // ---|Broadcast Modal CheckBox Toggle ------------------------------------------
  BrodcastModalCheckToggle = (e, { value }) => {
    const { BrodcastModalCheckText, BrodcastModalCheckEmail } = this.state;
    if (value === 'Text') {
      this.setState({
        BrodcastModalCheckText: !BrodcastModalCheckText,
      });
    } else if (value === 'EMail') {
      this.setState({
        BrodcastModalCheckEmail: !BrodcastModalCheckEmail,
      });
    }
  };

  // ---|Broadcast Modal Launch Function ------------------------------------------
  LaunchBroadcast = timeSlot => {
    const list = this.state.timeslots.filter(item => {
      return item.checkin_time === timeSlot;
    });
    this.setState({
      BroadcastModalClose: true,
      BroadcastModalSend: true,
      TimeSlot: timeSlot,
      BroadcastModalList: list,
    });
  };

  // ---|Broadcast Modal  ------------------------------------------
  BroadcastModal = timeSlot => {
    const ReciverList = [];
    if (typeof this.state.BroadcastModalList[0] !== 'undefined') {
      if (this.state.BroadcastModalList[0].bookings.length > 0) {
        this.state.BroadcastModalList[0].bookings.map(person => {
          ReciverList.push(person);
        });
      }
    }
    return (
      <Modal
        size="tiny"
        closeIcon
        onClose={() =>
          this.setState({
            BroadcastModalClose: false,
            BroadcastTextarea: '',
            BroadcastError: ' ',
            BroadcastModalSend: true,
          })
        }
        open={this.state.BroadcastModalClose}
        onOpen={() => this.setState({ BroadcastTextarea: '', BroadcastError: ' ', BroadcastModalSend: true })}
        style={inlineStyle.modal}>
        <Modal.Header>Broadcast</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            {typeof this.state.BroadcastModalList[0] !== 'undefined' ? (
              this.state.BroadcastModalList[0].bookings.length > 0 ? (
                <div className="Reciver">
                  {ReciverList.map(person => {
                    return <div className="ui label">{person.name}</div>;
                  })}
                </div>
              ) : (
                <div className="Reciver">NO Bookings yet</div>
              )
            ) : null}

            <div className="ui form">
              <div className="field">
                <label>Text</label>
                <textarea
                  id={`${timeSlot}BM_Text`}
                  value={this.state.BroadcastTextarea}
                  onChange={e => this.setState({ BroadcastTextarea: e.target.value })}
                />
              </div>
              <br />
              <div className="field">
                <Checkbox
                  label="Text Message"
                  name="checkboxRadioGroup"
                  value="Text"
                  checked={this.state.BrodcastModalCheckText}
                  onChange={this.BrodcastModalCheckToggle}
                />
                &nbsp;&nbsp;&nbsp;
                <Checkbox
                  label="E-Mail"
                  name="checkboxRadioGroup"
                  value="EMail"
                  checked={this.state.BrodcastModalCheckEmail}
                  onChange={this.BrodcastModalCheckToggle}
                />
              </div>
            </div>
          </Modal.Description>
          <div id="BroadcastError">{this.state.BroadcastError}</div>
        </Modal.Content>
        <Modal.Actions>
          <Button
            onClick={() => this.setState({ BroadcastModalClose: false, BroadcastTextarea: '', BroadcastError: ' ' })}>
            Cancel
          </Button>
          {this.state.BroadcastModalSend !== false ? (
            <Button primary content="Sent" onClick={() => this.BroadCastCallApi(timeSlot, ReciverList)} />
          ) : (
            <Button loading primary>
              Loading
            </Button>
          )}
        </Modal.Actions>
      </Modal>
    );
  };

  render() {
    return (
      <Grid celled="internally">
        <Grid.Row>
          <Grid.Column className="" width={6}>
            <Header as="h2" className="main-title mt-5">
              Booking Management
            </Header>
          </Grid.Column>
          <Grid.Column className="bs-none" floated="right" width={4}>
            {/* <Dropdown text="Board View" icon="list" basic floating labeled button className="icon float-right">
              <Dropdown.Menu>
                <Dropdown.Header icon="list" content="Change the View" />
                <Dropdown.Divider />
                <Dropdown.Item text="View 1" />
                <Dropdown.Item text="View 2" />
                <Dropdown.Item text="View 3" />
              </Dropdown.Menu>
            </Dropdown> */}
          </Grid.Column>
        </Grid.Row>
        <FilterRow
          date={this.state.date}
          setFetch={this.setFetch}
          onDateChange={this.onDateChange}
          onLocationChange={this.onLocationChange}
          onSortChange={this.onSortChange}
          onFilterChange={this.oonFilterChange}
          optionsForLocation={optionsForLocation}
          optionsForFilter={optionsForFilter}
          optionsForSort={optionsForSort}
        />

        <Grid.Row className="doubleScroll1">
          <div id="BroadCastModal">{this.BroadcastModal(this.state.TimeSlot)}</div>

          <div className="board">
            {!this.state.isFetching &&
              !this.state.errorInFetch &&
              this.state.timeslots.map(item => {
                return (
                  <div key={item._id} className="board-column">
                    <div className="board-column-header">
                      <TimeSlot
                        refreshnotes={this.getNotes}
                        notes={this.state.notes}
                        date={this.state.date}
                        item={item}
                        onCreateBooking={this.onCreateBooking}
                        LaunchBroadcast={this.LaunchBroadcast.bind(this)}
                      />
                    </div>
                    <div className="board-column-content">
                      {item.bookings.length !== 0 &&
                        item.bookings.map(booking => {
                          return (
                            <div key={booking._id} className="board-item">
                              <div className="board-item-content">
                                <Booking
                                  refreshnotes={this.getNotes}
                                  notes={this.state.notes}
                                  date={this.state.date}
                                  booking={booking}
                                />
                              </div>
                            </div>
                          );
                        })}
                    </div>
                  </div>
                );
              })}
            {!this.state.isFetching &&
              !this.state.errorInFetch &&
              this.state.timeslots.length !== 0 &&
              this.state.noBookings && (
                <p className="noBookings">
                  <strong>No Bookings yet</strong>
                  <br />
                  Click the plus button next to a time slot to create a booking.
                </p>
              )}
            {!this.state.isFetching && !this.state.errorInFetch && this.state.timeslots.length === 0 && (
              <p className="noBookings">
                <strong>No Timeslots present.</strong>
                <br />
                Kindly contact the admin if you want them to be added.
              </p>
            )}
          </div>
          {this.state.isFetching && (
            <Dimmer inverted active>
              <Loader size="large">Fetching</Loader>
            </Dimmer>
          )}
          {!this.state.isFetching && this.state.errorInFetch && (
            <Dimmer inverted active>
              <Message negative>
                <Message.Header>Error in Fetching Bookings</Message.Header>
              </Message>
            </Dimmer>
          )}
        </Grid.Row>
      </Grid>
    );
  }

  // dragDrop
  dragDrop = () => {
    const itemContainers = [].slice.call(document.querySelectorAll('.board-column-content'));
    const columnGrids = [];
    let boardGrid;

    // Define the column grids so we can drag those
    // items around.
    itemContainers.forEach(function(container) {
      // Instantiate column grid.
      const grid = new Muuri(container, {
        items: '.board-item',
        layoutDuration: 400,
        layoutEasing: 'ease',
        dragEnabled: true,
        dragSort() {
          return columnGrids;
        },
        dragSortInterval: 0,
        dragContainer: document.body,
        dragReleaseDuration: 400,
        dragReleaseEasing: 'ease',
      })
        .on('dragStart', function(item) {
          // Let's set fixed widht/height to the dragged item
          // so that it does not stretch unwillingly when
          // it's appended to the document body for the
          // duration of the drag.
          item.getElement().style.width = `${item.getWidth()}px`;
          item.getElement().style.height = `${item.getHeight()}px`;
        })
        .on('dragReleaseEnd', function(item) {
          // Let's remove the fixed width/height from the
          // dragged item now that it is back in a grid
          // column and can freely adjust to it's
          // surroundings.
          item.getElement().style.width = '';
          item.getElement().style.height = '';
          // Just in case, let's refresh the dimensions of all items
          // in case dragging the item caused some other items to
          // be different size.
          columnGrids.forEach(function(grid) {
            grid.refreshItems();
          });
        })
        .on('layoutStart', function() {
          // Let's keep the board grid up to date with the
          // dimensions changes of column grids.
          boardGrid.refreshItems().layout();
        });

      // Add the column grid reference to the column grids
      // array, so we can access it later on.
      columnGrids.push(grid);
    });

    // Instantiate the board grid so we can drag those
    // columns around.
    boardGrid = new Muuri('.board', {
      layout: {
        horizontal: true,
      },
      layoutDuration: 400,
      layoutEasing: 'ease',
      dragEnabled: true,
      dragSortInterval: 0,
      dragStartPredicate: {
        handle: '.board-column-content',
      },
      dragReleaseDuration: 400,
      dragReleaseEasing: 'ease',
    });
  };

  // change the cards being  displayed ON...
  // change the cards being  displayed ON...
  onDateChange = e => {
    try {
      if (e) {
        const pickedDate = moment(e);
        console.log('picked datee', e, 'pickedDate object', pickedDate);
        if (!pickedDate.isSame(this.state.date, 'day')) {
          this.props.dispatch({
            type: 'DATE_PICKED',
            payload: pickedDate,
          });
          this.setState({
            date: pickedDate,
            isFetching: true,
          });
          this.getData(pickedDate.format('YYYY-MM-DD'), this.state.location);

          this.getNotes(pickedDate.format('YYYY-MM-DD'));
        }
      }
    } catch (e) {
      console.log('ERROR', e);
    }
  };

  onLocationChange = (e, data) => {
    try {
      if (data) {
        this.setState({
          location: data.value,
          errorInFetch: false,
          isFetching: true,
        });
        this.getData(this.state.date.format('YYYY-MM-DD'), data.value);
      }
    } catch (e) {
      console.log('ERROR', e);
    }
  };

  onSortChange = e => {
    if (e) {
    }
  };

  onFilterChange = e => {
    if (e) {
      const date = `${e.getFullYear()}/${e.getMonth()}/${e.getDate()}`;
    }
  };

  // todo - take to next and dispatch action
  onCreateBooking = (e, data) => {
    try {
      console.log(this.state.date, this.state.location, data);
      this.props.dispatch({ type: 'RESET_BOOKINGINFO' });
      this.props.dispatch({ type: 'DATE_PICKED', payload: this.state.date });
      this.props.dispatch({ type: 'PLACE_PICKED', payload: this.state.location });
      getAvailablityByDate(this.state.date, this.state.location).then(rules => {
        this.props.dispatch({ type: 'GET_AVAILABILITY', payload: rules });
      });
      this.props.dispatch({
        type: 'SELECT_SLOT',
        payload: {
          checkinTime: {
            hours: parseInt(data.checkin_time.hours, 10),
            minutes: parseInt(data.checkin_time.minutes, 10),
          },
        },
      });
    } catch (e) {
      console.log(e);
    }
  };
}
const mapStateToProps = state => ({
  ...state,
});
export default connect(mapStateToProps)(Home);
