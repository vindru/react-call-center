import React, { Component } from 'react';
import { Icon, Card, Button } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import Notes from '../Notes';
import moment from 'moment';

class TimeSlot extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notes: 0,
      disableCreateBooking: false,
      lockColor: 'green',
    };
  }

  componentDidMount() {
    this.lockColorAndDisableCreateBooking();
  }

  onNotesChange = notes => this.setState({ notes });

  handleNotesClick = () => this.setState(prevState => ({ notesInFocus: !prevState.notesInFocus }));

  lockColorAndDisableCreateBooking = () => {
    try {
      const { item } = this.props;

      // Creating a new Moment object to avoid referencing to Date object in state (coming via props)
      // which is common to all Timeslots on that date
      const date = moment(this.props.date);
      date.hour(item.checkin_time.hours);
      date.minutes(item.checkin_time.minutes);
      date.seconds(0);
      // ----- Following will ENABLE/DISABLE create booking based on : Date of booking
      // ----- And lock color based on : lock_status from API
      if (item.lock_status) {
        this.setState({
          lockColor: item.lock_status,
          disableCreateBooking: date.isBefore(moment()),
        });
      } else {
        // ----- Fallback TO
        // ----- Following will ENABLE/DISABLE create booking based on : Date of booking
        // ----- And lock color based on : Slots left
        if (item.number_of_spaces - item.number_of_spaces_booked > 0) {
          this.setState({
            lockColor: 'green',
            disableCreateBooking: date.isBefore(moment()),
          });
        } else {
          this.setState({
            lockColor: 'red',
            disableCreateBooking: date.isBefore(moment()),
          });
        }
      }
    } catch (e) {
      console.log('ERROR:', e);
    }
  };

  render() {
    return (
      <React.Fragment>
        <Card>
          <Card.Content extra style={{ zIndex: '1' }}>
            {!this.state.disableCreateBooking && (
              <Link to="/CreateBooking">
                <Button
                  onClick={e => this.props.onCreateBooking(e, this.props.item)}
                  id={this.props.item && this.props.item._id}
                  className="float-right bg-white"
                  icon="plus"
                  color="blue"
                  basic
                />
              </Link>
            )}
            {this.state.disableCreateBooking && (
              <Button disabled className="float-right bg-white" icon="plus" color="blue" basic />
            )}

            <Icon className="float-right mr-15 mt-5" size="large" name="lock" color={this.state.lockColor} />
            <Card.Header>
              {// for print time in 2-digit format
              this.props.item.checkin_time && this.props.item.checkin_time.hours < 10
                ? `0${this.props.item.checkin_time.hours}`
                : this.props.item.checkin_time.hours}
              :
              {// for print time in 2-digit format
              this.props.item.checkin_time && this.props.item.checkin_time.minutes < 10
                ? `0${this.props.item.checkin_time.minutes}`
                : this.props.item.checkin_time.minutes}
            </Card.Header>
            <Card.Meta style={{ color: 'black' }}>
              {this.props.item &&
                `${this.props.item.number_of_spaces_booked}/${this.props.item.number_of_spaces} - $${this.props.item.invoice_price}`}
            </Card.Meta>
          </Card.Content>
          <Card.Content
            extra
            style={{
              height: '60px',
              padding: '0',
              position: 'absolute',
              backgroundColor: '#E2EBFA',
              width:
                this.props.item &&
                `${(this.props.item.number_of_spaces_booked / this.props.item.number_of_spaces) * 100}%`,
            }}></Card.Content>

          <Card.Content className="timeslotButtonsRow" extra>
            <Button
              className="timeslotButtons"
              basic
              onClick={() => {
                if (this.props.item) {
                  this.props.LaunchBroadcast(this.props.item.checkin_time);
                }
              }}>
              <Icon name="bullhorn" />
              Broadcast
            </Button>
            <Button
              active={this.state.notesInFocus}
              basic
              onClick={this.handleNotesClick}
              className="float-right timeslotButtons">
              <Icon name="chat outline" />
              Notes{' '}
              <span style={{ borderRadius: '2px', color: '#216CDA', backgroundColor: '#E7EFFB', padding: '2px 4px' }}>
                {this.state.notes}
              </span>
            </Button>
          </Card.Content>
          <Card.Content extra hidden={!this.state.notesInFocus}>
            <Notes
              Data=""
              refreshnotes={this.props.refreshnotes}
              notes={this.props.notes}
              date={this.props.date && this.props.date.toISOString().split('T')[0]}
              onChange={this.onNotesChange}
              type="Drop"
              id={this.props.item && this.props.item._id}
            />
          </Card.Content>
        </Card>
      </React.Fragment>
    );
  }
}
export default TimeSlot;
