import React, { Component } from 'react';
import { Grid, Input, Button, Dropdown, Icon } from 'semantic-ui-react';
import moment from 'moment';
// date picker
import enLocale from 'react-semantic-ui-datepickers/dist/locales/en-US';
import SemanticDatepicker from 'react-semantic-ui-datepickers';
import 'react-semantic-ui-datepickers/dist/react-semantic-ui-datepickers.css';
// this components styles
import './filterRow.scss';

export default class FilterRow extends Component {
  onDateMoveOneLeft = (e, data) => {
    // make a copy of Date from props BY VALUE, so that the props ( thereby state ) is not changed
    // when the copy is changed
    const date = moment(this.props.date);
    date.subtract(1, 'day');
    this.props.onDateChange(date);
  };

  onDateMoveOneRight = (e, data) => {
    // make a copy of Date from props BY VALUE, so that the props ( thereby state ) is not changed
    // when the copy is changed
    const date = moment(this.props.date);
    date.add(1, 'day');
    this.props.onDateChange(date);
  };

  render() {
    return (
      <Grid.Row className="FilterRow">
        <Grid.Column width={8} className="DateAndPlacePicker">
          <div className="buttonWithDatePicker">
            <Button basic icon="left chevron" onClick={this.onDateMoveOneLeft} />
            <SemanticDatepicker
              locale={enLocale}
              clearOnSameDateClick={false}
              selected={this.props.date && this.props.date.toDate()}
              clearable={false}
              format="D  MMMM"
              onDateChange={this.props.onDateChange}
            />
            <Button basic icon="right chevron" onClick={this.onDateMoveOneRight} />
          </div>

          <Dropdown
            onChange={this.props.onLocationChange}
            defaultValue={this.props.optionsForLocation[0] && this.props.optionsForLocation[0].value}
            options={this.props.optionsForLocation}
            selection
            basic
            labeled
            button
            className="icon ml-30"
            icon={<Icon name="sort" className="right" />}
          />
        </Grid.Column>
        <Grid.Column className="bs-none" floated="right" textAlign="right" width={8}>
          {/* <Dropdown
            placeholder="Sort"
            options={this.props.optionsForSort}
            clearable="true"
            selection
            basic
            labeled
            button
            className="icon ml-15"
            icon={
              <React.Fragment>
                <Icon name="sliders" />
                <Icon name="sort" className="right" />
              </React.Fragment>
            }
          />
          <Dropdown
            placeholder="Filter"
            options={this.props.optionsForFilter}
            clearable="true"
            selection
            basic
            labeled
            button
            className="icon ml-15"
            icon={
              <React.Fragment>
                <Icon name="filter" />
                <Icon name="sort" className="right" />
              </React.Fragment>
            }
          />
          <Input icon={{ name: 'search', circular: true, link: true }} placeholder="Search..." /> */}
        </Grid.Column>
      </Grid.Row>
    );
  }
}
