import React, { Component } from 'react';
import { Icon } from 'semantic-ui-react';

export default class TextIcon extends Component {
  render() {
    return (
      <div
        style={{
          whiteSpace: 'nowrap',
          display: 'inline-flex',
        }}>
        <Icon size="large" color={this.props.color} name={this.props.name} className={this.props.className} />
        <div style={this.style} hidden={this.props.hideText}>
          {this.props.children}
        </div>
      </div>
    );
  }
}
