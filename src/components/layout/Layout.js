import React from 'react';
import SideMenu from './SideMenu';
import TopMenu from './TopMenu';
import './Layout.scss';

export default props => (
  <div className="shell">
    <TopMenu />
    <SideMenu />
    <div className="main-content">{props.children}</div>
  </div>
);
