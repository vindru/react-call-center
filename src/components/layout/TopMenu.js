import React from 'react';
import { Image, Label, Menu, Dropdown, Breadcrumb } from 'semantic-ui-react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import Notification from './Notification';
import './TopMenu.scss';

export default connect(state => state.SideMenu)(({ smallMenu }) => {
  const breadCrumb = [{ key: 'Home', content: 'Home', link: true }];
  return (
    <Menu fixed="top" className={classNames({ smallMenu })}>
      <Menu.Menu position="left">
        <Menu.Item className="no-border">
          <Breadcrumb icon="right angle" sections={breadCrumb} />
        </Menu.Item>
      </Menu.Menu>
      <Menu.Menu position="right">
        <Menu.Item className="no-border" position="right">
          <Notification />
          <Label className="label-on-corner place-over-bell" color="blue" size="mini" circular>
            22
          </Label>
        </Menu.Item>
        <Menu.Item className="no-border" position="right">
          <div className="display-inline">
            <Image circular size="mini" src="https://react.semantic-ui.com/images/avatar/large/jenny.jpg" />
            <Dropdown text="Albiona" icon="sort" className="title">
              <Dropdown.Menu>
                <Dropdown.Item text="Change Settings" />
                <Dropdown.Item icon="file" text="Account Info" />
                <Dropdown.Divider />
                <Dropdown.Item text="Logout" />
              </Dropdown.Menu>
            </Dropdown>
          </div>
        </Menu.Item>
      </Menu.Menu>
    </Menu>
  );
});
