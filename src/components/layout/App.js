﻿import React from 'react';
import { Route, Switch } from 'react-router';
import Layout from './Layout';
import Home from '../pages/ViewBooking';
import UpdateBooking from '../pages/UpdateBooking';
import UserManagement from '../pages/UserManagement';
import CardForm from '../pages/Card';

import CreateBooking from '../pages/CreateBooking';

export default () => (
  <Layout>
    <Switch>
      <Route exact path="/userManagement" component={UserManagement} />
      <Route exact path="/card" component={CardForm} />
      <Route path="/UpdateBooking/:id" component={UpdateBooking} />
      <Route exact path="/UpdateBooking" component={UpdateBooking} />
      <Route exact path="/CreateBooking" component={CreateBooking} />
      <Route path="/" component={Home} />
    </Switch>
  </Layout>
);
