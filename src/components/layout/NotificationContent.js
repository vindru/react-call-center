import React, { Component } from 'react';

import { Button, Grid, Header, Segment } from 'semantic-ui-react';

export default class NotificationContent extends Component {
  state = {};
  render() {
    const patients = [
      {
        name: 'jatin s',
        bio: 'Lorem ipsum dolor sit amet,',
        button: 'More Info',
      },
      {
        name: 'jatin s 2',
        bio: 'Lorem ipsum dolor sit amet,',
        button: 'More Info',
      },
      {
        name: 'jatin s 3',
        bio: 'Lorem ipsum dolor sit amet,',
        button: 'More Info',
      },
    ];

    return (
      <Grid.Column textAlign="center">
        {patients.map(patient => (
          <Segment>
            <Header as="h4">Basic Plan</Header>
            <p>{patient.bio}</p>
            <Button>{patient.button}</Button>
          </Segment>
        ))}
      </Grid.Column>
    );
  }
}
