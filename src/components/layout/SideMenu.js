import React from 'react';
import { Menu } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { actionCreators } from '../../store/SideMenu';

import TextIcon from '../extension/TextIcon';

import 'font-awesome/css/font-awesome.min.css';
import './SideMenu.scss';

export default connect(state => state.SideMenu)(({ smallMenu, activeItem, dispatch }) => {
  const handleToggleClick = () => {
    dispatch(actionCreators.toggleSideMenu());
  };
  const handleItemClick = (e, { name }) => {
    dispatch(actionCreators.setActiveItem(name));
  };
  return (
    <Menu fixed="left" borderless className={classNames('sidemenu', { smallMenu })} vertical>
      <Menu.Item className="logo-space-menu-item" onClick={handleToggleClick}>
        <div className="logo-space">
          <i className="fa fa-arrows-alt" />
        </div>
      </Menu.Item>
      <Menu.Item as={Link} to="/" name="Booking Management" active={true} onClick={handleItemClick}>
        <TextIcon hideText={smallMenu} className="sideMenu-div" name="home">
          Booking Management
        </TextIcon>
      </Menu.Item>
    </Menu>
  );
});

// render() {
//   return (
//     // <div className="parent">
//     //   <div className={"smallMenu-side side"}>

//     //     {getMenu()}
//     //   </div>

//     //   <div
//     //     className={(smallMenu ? "smallMenu-content " : "") + "content"}
//     //   >

//     //     <h2 style={{ display: 'inline-block', marginBottom: '0' }} className="main-title"> Booking Management </h2>

//     <Dropdown style={{ float: 'right' }} text='Board View' icon='list' basic floating labeled button className='icon'>
//       <Dropdown.Menu>
//         <Dropdown.Header icon='list' content='Change the View' />
//         <Dropdown.Divider />
//         <Dropdown.Item text='View 1' />
//         <Dropdown.Item text='View 2' />
//         <Dropdown.Item text='View 3' />

//       </Dropdown.Menu>
//     </Dropdown>

//     // <Divider />
//     //     {props.children}
//     //   </div>
//     // </div>
//   );
// }
// }

// export default connect(
//   state => state.sideMenu,
//   dispatch => bindActionCreators(actionCreators, dispatch)
// )(SideMenu);
