const path = require('path');
const express = require('express');
const app = express();
const port = process.env.SERVER_PORT || 3000;
const bodyParser = require('body-parser');
const morgan = require('morgan')

app.use(morgan(':method :url :status :res[content-length] - :response-time ms'))
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, '../build')));

// PATH CONFIGURATION TO RESPOND TO A REQUEST TO STATIC ROUTE REQUEST BY SERVING index.html
app.get('*', function (req, res) {
  res.sendFile(path.join(__dirname, '../', 'build', 'index.html'));
});

// console.log that your server is up and running
app.listen(port, () => console.log(`Listening on port ${port}`));

// create a GET rout
